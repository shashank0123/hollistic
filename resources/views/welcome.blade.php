<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->


<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
	<title>Hollistic: A Global Life Improvement Ecosystem | </title>

	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="assets/images/favicon.ico">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png">
  <link rel="manifest" href="assets/images/site.webmanifest">
  <link rel="mask-icon" href="assets/images/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
	<!-- <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicons/favicon.png"> -->
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7CLora:400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/animations.css">
	<link rel="stylesheet" href="assets/css/fonts.css">
	<!-- <link rel="stylesheet" href="assets/css/style.css"> -->
	<link rel="stylesheet" href="assets/css/main.css" class="color-switcher-link">
	<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
	<link rel="stylesheet" href="assets/css/shop.css" class="color-switcher-link">
	<script src="assets/js/vendor/modernizr-2.6.2.min.js"></script>
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css"/>
	<link href="assets/css/lightbox.css" rel="stylesheet">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>

<!-- <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css"/> -->
	<!--[if lt IE 9]>
		<script src="assets/js/vendor/html5shiv.min.js"></script>
		<script src="assets/js/vendor/respond.min.js"></script>
		<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->
	<style media="screen">
		.d-none{
			display: none;
		}
		.text-red{
			color: #ff0000;
		}
	</style>
</head>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
	<div class="preloader">
		<div class="preloader_image"></div>
	</div>
	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">
			<i class="rt-icon2-cross2"></i>
		</span>
	</button>
		<div class="widget widget_search">
			<form method="get" class="searchform search-form form-inline" action="http://webdesign-finder.com/html/diversify/">
				<div class="form-group bottommargin_0"> <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input"> </div> <button type="submit" class="theme_button no_bg_button">Search</button> </form>
		</div>
	</div>
	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls with_padding">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->
		</div>
	</div>
	<!-- eof .modal -->
	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">
			<!-- template sections -->
			<section style="background-color: #000;" class="page_topline ds table_section table_section_lg section_padding_top_15 section_padding_bottom_15 columns_margin_0">
				<div class="container-fluid responsive-header-section">
					<div class="row">
						<div class="col-lg-6 text-center text-lg-left hidden-xs">
							<div class="inline-content big-spacing">
								<div class="page_social">
								    <a class="social-icon socicon-youtube" target="_blank" href="https://www.youtube.com/channel/UCJzsP1_r3YoNVNdf06n4kXw" title="Youtube"></a>
								    <a class="social-icon socicon-blogger" target="_blank" href="https://www.blogger.com/u/2/blogger.g#welcome" title="blogger"></a>
								    <a class="social-icon socicon-vimeo" target="_blank" href="https://vimeo.com/hollistic" title="vimeo"></a>
								    <a class="social-icon socicon-instagram" target="_blank" href="https://www.instagram.com/hollisticmedia/" title="instagram"></a>
								    <a class="social-icon socicon-facebook" target="_blank" href="https://www.facebook.com/hollisticmedia" title="Facebook"></a>
								    <a class="social-icon socicon-pinterest" target="_blank" href="https://in.pinterest.com/hollisticmedia/" title="pinterest"></a>
								    <a class="social-icon socicon-twitter" target="_blank" href="https://twitter.com/hollisticmedia" title="Twitter"></a>
								    <a class="social-icon socicon-tumblr" target="_blank" href="https://www.tumblr.com/blog/hollisticmedia" title="tumblr"></a>
								    <a class="social-icon socicon-snapchat" target="_blank" href="https://www.snapchat.com/add/hollisticmedia" title="snapchat"></a>
								    <a class="social-icon socicon-reddit" target="_blank" href="https://www.reddit.com/user/hollisticmedia" title="reddit"></a>
								    <a class="social-icon socicon-quora" target="_blank" href="https://www.quora.com/profile/Hollistic-Media" title="quora"></a>
								    <!--<a class="social-icon socicon-buzzfeed" href="#" title="buzzfeed"></a>-->
								    <a class="social-icon socicon-flickr" href="https://www.flickr.com/photos/170872704@N04/" title="flickr"></a>



								</div>
								<!-- <span class="xs-block">
									<i class="fa fa-phone" aria-hidden="true"></i>
									+91-95-7711-9900
								</span> -->
					</div>
						</div>
						<div class="col-lg-6 text-center text-lg-right">
							<div id="topline-animation-wrap">
								<div id="" class="inline-content big-spacing" style="text-align: right;"> <span class="hidden-xs">
							<!-- <i class="fa fa-map-marker highlight3 rightpadding_5" aria-hidden="true"></i>
							73 Harvey Forest Suite, Singapore -->
						</span>
						<!-- <span class="greylinks xs-block">
							<i class="fa fa-phone highlight3 rightpadding_5" aria-hidden="true"></i>
							<a href="tel:+919577119900">+91-95-7711-9900</a>
						</span> -->
						<!-- <span class="greylinks xs-block">
							<i class="fa fa-pencil highlight3 rightpadding_5" aria-hidden="true"></i>
							<a href="mailto:H-Love@Hollistic.org">H-Love@Hollistic.org</a>
						</span> -->
						<div class="xs-block">
							<ul class="inline-list menu greylinks">
								<!-- <li class="dropdown">
									<a id="search-show" href="index.php">
										<i class="fa fa-search" style="font-size: 18px;"></i>
									</a>
								</li> -->
								<li>
									<?php if(isset($_SESSION['loggedin'])) : ?>
										<a href="account.php" style="text-color: #fff;font-weight: bold;font-size: 14px;">
											<i class="fa fa-user" aria-hidden="true"  style="font-size: 16px;"></i> ACCOUNT
										</a>
									<?php else : ?>
										<a href="login.php" style="text-color: #fff;font-weight: bold;font-size: 14px;">
										<i class="fa fa-user" aria-hidden="true"  style="font-size: 16px;"></i> LOGIN
									</a>
									<?php endif; ?>
								</li>
								<!-- <li>
									<a href="shop-cart.php">
										<i class="fa fa-shopping-cart" aria-hidden="true"  style="font-size: 18px;"></i>
									</a>
								</li> -->
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<header class="page_header header_white toggler_xs_right columns_margin_0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 display_table">
				<div class="header_left_logo display_table_cell">
					<a href="index.php" class="logo logo_with_text">
						<img src="assets/images/logo1.jpg" alt="">
						<span class="logo_text">
						<span>Hollistic<sup style="font-size: 11px;top: -1.5em;">®</sup></span>
							<small style="color: #8B0000;
    border-color: #8B0000; font-weight: bold; font-weight:700; margin-left:-6%;">Life improvement</small>
						</span>
					</a>
				</div>
				<div class="header_mainmenu display_table_cell text-center">
					<!-- main nav start -->
					<nav class="mainmenu_wrapper">
						<ul class="mainmenu nav sf-menu">

							<li class="active">
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="about.php">About Us</a>
							</li>
							<!-- eof pages -->
							<li>
								<a href="programs.php">Coaching</a>
								<ul>
									<li>
										<a href="client-programs.php">Client Programs</a>
									</li>
									<li>
										<a href="partner-program.php">Partner Programs</a>
									</li>

								</ul>
							</li>
							<li>
										<a href="ventures.php">Ventures</a>
									</li>
									<li>
								<a href="capital.php">Capital</a>
							</li>

							<li>
								<a href="content.php">Content</a>
							</li>
							<li>
								<a href="schedule.php">Schedule</a>
							</li>

							<li>
								<a href="merchandise.php">Merchandise</a>
							</li>
							<li>
								<a href="faq.php">Help</a>
							</li>
							<li>
								<a href="contact.php">Contact Us</a>
							</li>
							<!-- <li style="margin-bottom: 15px;margin-top: 7px;">
								<a href="#" class="theme_button color2 margin_0 connect-withus-btn" data-toggle="modal" data-target="#registerModal">Connect With Us</a>
							</li> -->
							<li class="responsive-social-icons">
								<a class="social-icon socicon-youtube" style="display: inline-block;padding: 0px 0;" href="https://www.youtube.com/channel/UCJzsP1_r3YoNVNdf06n4kXw" title="youtube" target="blank"></a>
								<a class="social-icon socicon-blogger" style="display: inline-block;padding: 0px 0;" href="https://www.blogger.com/u/2/blogger.g#welcome" target="blank"></a>
								<a class="social-icon socicon-vimeo" style="display: inline-block;padding: 0px 0;" href="https://vimeo.com/hollistic" title="Google Plus" target="blank"></a>
								<a class="social-icon socicon-instagram" style="display: inline-block;padding: 0px 0;" href="https://www.instagram.com/hollisticmedia/" title="Linkedin" target="blank"></a>
								<a class="social-icon socicon-facebook" style="display: inline-block;padding: 0px 0;" href="https://www.facebook.com/hollisticmedia" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-pinterest" style="display: inline-block;padding: 0px 0;" href="https://in.pinterest.com/hollisticmedia/" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-twitter" style="display: inline-block;padding: 0px 0;" href="https://twitter.com/hollisticmedia" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-tumblr" style="display: inline-block;padding: 0px 0;" href="https://www.tumblr.com/blog/hollisticmedia" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-snapchat" style="display: inline-block;padding: 0px 0;" href="https://www.snapchat.com/add/hollisticmedia" title="snapchat" target="blank"></a>
								<a class="social-icon socicon-reddit" style="display: inline-block;padding: 0px 0;" href="https://www.reddit.com/user/hollisticmedia" title="reddit" target="blank"></a>
								<a class="social-icon socicon-quora" style="display: inline-block;padding: 0px 0;" href="https://www.quora.com/profile/Hollistic-Media" title="quora" target="blank"></a>

							</li>
						</ul>
					</nav>

					<span class="toggle_menu"><span>
					</span>
				</span>
			</div>
			<!-- <div class="header_right_buttons display_table_cell text-right hidden-xs">
				<a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#contentModal">Connect With Us</a>
			</div> -->
		</div>
	</div>
</div>
</header>
<div class="serach-bar-responsive">
	<div id="topline-show" class="widget widget_search">
		<form method="get" class="searchform" action="http://webdesign-finder.com/html/diversify/">
			<div class="form-group-wrap">
				<div class="form-group serach-input-responsive">
					<label class="sr-only" for="topline-widget-search">Search for:</label>
					<input id="topline-widget-search" type="text" value="" name="search" class="form-control" placeholder="Enter Keyword...">
					<a href="index.php" id="search-close">
						<i class="fa fa-close close-icon-btn"></i>
					</a>
				</div>
				<button type="submit" class="theme_button no_bg_button color1">ok</button>
			</div>
		</form>
	</div>
</div>
<!-- body conetn -->
<section class="intro_section page_mainslider ds all-scr-cover bottom-overlap-teasers" >
<div class="flexslider" data-dots="true" data-nav="true">
  <ul class="slides">
    <li>
      <div class="slide-image-wrap image-container" style="
            background-position: top;">
        <img src="assets/images/banner/banner1.jpg" alt="" />
      </div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <div class="slide_description_wrapper">
              <div class="slide_description">
                <div class="intro-layer slider-subtitle" data-animation="fadeInRight">Hollistic:</div>
                <div class="intro-layer" data-animation="fadeInLeft">
                  <h2>
                    <span class="highlight3">A Global Life </span>
                    <br> Improvement Ecosystem
                    </h2>
                  </div>
                  <div class="intro-layer" data-animation="fadeInRight">
                    <p class="thin">
                      <em></em>
                    </p>
                  </div>
                  <div class="intro-layer" data-animation="fadeInUp">
                    <div class="slide_buttons">
                      <a href="about.php" class="theme_button color1 min_width_button">Learn More</a>
                    </div>
                  </div>
                </div>
                <!-- eof .slide_description -->
              </div>
              <!-- eof .slide_description_wrapper -->
            </div>
            <!-- eof .col-* -->
          </div>
          <!-- eof .row -->
        </div>
        <!-- eof .container -->
      </li>
      <li>
        <div class="slide-image-wrap image-container">
          <img src="assets/images/banner/banner2.jpg" alt="" />
          <div class="after"></div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center">
              <div class="slide_description_wrapper">
                <div class="slide_description">
                  <div class="intro-layer slider-subtitle" data-animation="fadeInRight"> Multi-Dimensional Coaching:</div>
                  <div class="intro-layer" data-animation="fadeInLeft">
                    <h2>
                      <span class="highlight3"> Change Your Self...</span>
                      <br>  Change Your Life!
                      </h2>
                    </div>
                    <div class="intro-layer" data-animation="fadeInRight">
                      <p class="thin">
                        <em></em>
                      </p>
                    </div>
                    <div class="intro-layer" data-animation="fadeInUp">
                      <div class="slide_buttons">
                        <a href="programs.php" class="theme_button color1 min_width_button">Learn More</a>
                      </div>
                    </div>
                  </div>
                  <!-- eof .slide_description -->
                </div>
                <!-- eof .slide_description_wrapper -->
              </div>
              <!-- eof .col-* -->
            </div>
            <!-- eof .row -->
          </div>
          <!-- eof .container -->
        </li>
        <li>
          <div class="slide-image-wrap image-container">
            <img src="assets/images/banner/banner12.jpg" alt="" />
            <div class="after"></div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-sm-12 text-center">
                <div class="slide_description_wrapper">
                  <div class="slide_description">
                    <div class="intro-layer slider-subtitle" data-animation="fadeInRight">Humanity-Oriented Ventures:</div>
                    <div class="intro-layer" data-animation="fadeInLeft">
                      <h2>
                        <span class="highlight3">Human Wellness, </span>
                        <br> Life Technology, Social Capital
                        </h2>
                      </div>
                      <div class="intro-layer" data-animation="fadeInRight">
                        <p class="thin">
                          <em></em>
                        </p>
                      </div>
                      <div class="intro-layer" data-animation="fadeInUp">
                        <div class="slide_buttons">
                          <a href="ventures.php" class="theme_button color1 min_width_button">Learn More</a>
                        </div>
                      </div>
                    </div>
                    <!-- eof .slide_description -->
                  </div>
                  <!-- eof .slide_description_wrapper -->
                </div>
                <!-- eof .col-* -->
              </div>
              <!-- eof .row -->
            </div>
            <!-- eof .container -->
          </li>
          <li>
            <div class="slide-image-wrap image-container">
              <img src="assets/images/banner/banner4.jpg" alt="" />
              <div class="after"></div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <div class="slide_description_wrapper">
                    <div class="slide_description">
                      <div class="intro-layer slider-subtitle" data-animation="fadeInRight">Conscious Capital:</div>
                      <div class="intro-layer" data-animation="fadeInLeft">
                        <h2>
                          <span class="highlight3"> Affecting Lives Through</span>
                          <br>  Positive Impact
                          </h2>
                        </div>
                        <div class="intro-layer" data-animation="fadeInRight">
                          <p class="thin">
                            <em></em>
                          </p>
                        </div>
                        <div class="intro-layer" data-animation="fadeInUp">
                          <div class="slide_buttons">
                            <a href="capital.php" class="theme_button color1 min_width_button">Learn More</a>
                          </div>
                        </div>
                      </div>
                      <!-- eof .slide_description -->
                    </div>
                    <!-- eof .slide_description_wrapper -->
                  </div>
                  <!-- eof .col-* -->
                </div>
                <!-- eof .row -->
              </div>
              <!-- eof .container -->
            </li>
          </ul>
        </div>
        <!-- eof flexslider -->
      </section>
      <section id="services" class="ls section_intro_overlap columns_padding_0 columns_margin_0 container_padding_0">
        <div class="container-fluid">
          <!-- <div class="row flex-wrap v-center-content"><div class="col-lg-4 col-sm-6 col-xs-12 to_animate" data-animation="fadeInUp"><div class="teaser main_bg_color transp with_padding big-padding margin_0"><div class="media xxs-media-left"><div class="media-left media-middle"><div class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper"><i class="fa fa-twitter highlight" aria-hidden="true"></i></div></div><div class="media-body media-middle"><h4><a href="client-programs.php">Client Programs</a></h4><p>It’s easier now than ever before to seek guidance
in any area of life.</p></div></div></div></div><div class="col-lg-4 col-sm-6 col-xs-12 to_animate" data-animation="fadeInUp"><div class="teaser main_bg_color2 transp with_padding big-padding margin_0"><div class="media xxs-media-left"><div class="media-left media-middle"><div class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper"><i class="fa fa-rocket highlight2" aria-hidden="true"></i></div></div><div class="media-body media-middle"><h4><a href="partner-program.php">Partner Programs</a></h4><p>We believe that the life-altering impact the written word can create is unparalled.</p></div></div></div></div><div class="col-lg-4 col-sm-6 col-xs-12 to_animate" data-animation="fadeInUp"><div class="teaser main_bg_color3 transp with_padding big-padding margin_0"><div class="media xxs-media-left"><div class="media-left media-middle"><div class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper"><i class="fa fa-users highlight3" aria-hidden="true"></i></div></div><div class="media-body media-middle"><h4><a href="venture-program.php">Venture Programs</a></h4><p>Entrepreneurs, technologists, and investors focused on the Life Improvement sector may reach us to
discuss our venture programs.</p></div></div></div></div></div> -->
        </div>
      </section>
      <section id="about" class="ls section_padding_top_110 section_padding_bottom_65 columns_padding_30">
        <div class="container">
          <div class="row">
            <div class="col-md-12" data-animation="fadeInUp" data-delay="600">
              <!-- <div class="embed-responsive embed-responsive-3by2"><a href="https://www.youtube.com/embed/xKxrkht7CpY" class="embed-placeholder"><img src="assets/images/updated/about hollistic/1.jpg" alt=""></div> -->
              <h2 class="section_header color4" style="margin-top: 23px;"> Welcome to Hollistic! </h2>
              <!-- <p class="section-excerpt grey">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore.</p> -->
              <p class="hollistic-content" >You are unique! And yet, it isn’t always easy to realise how special you are
in the midst of the billions of people that inhabit this planet. But that is
exactly what you are, in your very distinct way. And Hollistic is here to help
you express that very uniqueness in full shine, while at the same time,
regain focus on and understand what you truly want in life and ensure you
have the strategies and support you need to make progress towards those
goals.
</p>
              <p class="hollistic-content" >Hollistic offers an integrated way of understanding people’s minds, with one
primary goal: the improvement of life. Through the exploration of oneself,
Hollistic strives to help people achieve growth in multiple dimensions, while
attaining a state of balance in all major areas of life.</p>
              <!-- <p class="hollistic-content" style=" font-weight: 700;">Hollistic® is a registered trademark in multiple countries and classes.</p><br> -->
            </div>
          </div>
        </div>
      </section>
      <!-- <section id="about" class="ls section_padding_top_30 columns_padding_30"><div class="container"><div class="row">
      <!-- <div class="col-md-6"><img src="assets/images/updated/about hollistic/2.jpg" alt=""></div> -
             <div class="col-md-12"><p class="hollistic-content" style="color: #464545;">Hollistic offers an integrated way of understanding people’s minds, with one
primary goal: the improvement of life. Through the exploration of oneself,
Hollistic strives to help people achieve multi-dimensional growth, while
attaining a state of balance in all areas of life.</p><br><p class="hollistic-content" style="color: #464545;"></p></div></div></div></section> -->
      <!-- <section id="about" class="ls section_padding_top_30 columns_padding_30"><div class="container"><div class="row"><div class="col-md-12"><p class="hollistic-content" style="color: #464545;">Hollistic’s approach to Life Improvement extends from our core work in
Multi-Dimensional Coaching to our projects and ventures in Human Wellness,
Life Technology, and Social Capital. Using the medium of “H-Love”, i.e. our
unconditional availability for all beings, Hollistic abides by an ideal of serving
as the “World’s Backup”. Our highest common goal in our work is to enable
ourselves as well as others to see, think, feel, and live with our hearts at the
center of all that we do.
</p><p class="hollistic-content" style="color: #464545; font-weight: 700;">Hollistic® is a registered trademark in multiple countries and classes.</p><br></div>
      <!-- <div class="col-md-6"><img src="assets/images/updated/about hollistic/4.jpg" alt=""></div> -

          </div></div></section> -->
      <!--  <section class="ls page_portfolio section_padding_top_100 section_padding_bottom_0 columns_padding_0 container_padding_0"><div class="container-fluid"><div class="row"><div class="col-sm-12"><div class="row masonry-layout columns_margin_0" data-filters=".isotope_filters"><div class="col-md-3"><div class="custom-gallery-item"><div class="item-content"><a href="about.php"><h2><b>Hollistic’s Vision</b></h2><p style="color: #fff;"> To help people understand and witness the magnificence and beauty that they truly are

</p></a></div></div></div><div class="col-md-3"><div class="custom-gallery-item1"><div class="item-content"><a href="content.php"><h2><b>Hollisticity</b></h2><p style="color: #fff;">A state of being where all positive qualities are combined together multi-dimensionally into a single integrated state</p></a></div></div></div><div class="col-md-3"><div class="custom-gallery-item2"><div class="item-content"><a href="programs.php"><h2 ><b>H-Love</b></h2><p style="color: #fff;">The unconditional availability to all beings, with a vision of serving as the “World’s Backup”</p></a></div></div></div><div class="col-md-3"><div class="custom-gallery-item3"><div class="item-content"><a href="contact.php"><h2><b>HCLE</b></h2><p style="color: #fff;">Hollistic Core Life Education Program with five modules: Self, Perspective, People, Communication, World</p></a></div></div></div></div></div></div></div></section> -->
      <div class="row flex-wrap v-center-content">
        <div class="col-lg-4 col-sm-4 col-xs-12 to_animate hprograms" data-animation="fadeInUp">
          <div class="teaser main_bg_color transp with_padding big-padding margin_0">
            <div class="media xxs-media-left">
              <div class="media-center media-middle hpicon">
                <a href="programs.php" class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
                  <i class="fa fa-users highlight" aria-hidden="true"></i>
                </a>
              </div>
              <div class="media-body media-middle text-center">
                <h4>
                  <a href="programs.php" class="home-programs">Coaching</a>
                </h4>
                <br>
                  <p>Client Programs</p>
                  <p>Partner Programs</p>
                  <p>
                    <br/>
                  </p>
                  <!-- <p>Content Publications</p> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-sm-4 col-xs-12 to_animate hprograms" data-animation="fadeInUp">
            <div class="teaser main_bg_color2 transp with_padding big-padding margin_0">
              <div class="media xxs-media-left">
                <div class="media-center media-middle hpicon">
                  <a href="ventures.php" class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
                    <i class="fa fa-rocket highlight2" aria-hidden="true"></i>
                  </a>
                </div>
                <div class="media-body media-middle text-center">
                  <h4>
                    <a href="ventures.php" class="home-programs">Ventures</a>
                  </h4>
                  <br>
                    <p>Human Wellness</p>
                    <p>Life Technology</p>
                    <p>Social Capital</p>
                    <!-- <p>Referral Sharing</p> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-xs-12 to_animate hprograms" data-animation="fadeInUp">
              <div class="teaser main_bg_color3 transp with_padding big-padding margin_0">
                <div class="media xxs-media-left">
                  <div class="media-center media-middle hpicon">
                    <a href="capital.php" class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
                      <i class="fa fa-money highlight3" aria-hidden="true" ></i>
                    </a>
                  </div>
                  <div class="media-body media-middle text-center">
                    <h4>
                      <a href="capital.php" class="home-programs">Capital</a>
                    </h4>
                    <br>
                      <!-- 	<p>Project</p><p>Team</p><p>Regions</p><p>Sectors</p><p>Company Type</p><p>Invest</p> -->
                      <p>Conscious Investing</p>
                      <p>
                        <br/>
                      </p>
                      <p>
                        <br/>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="page-section" style="padding-top: 70px;"><div class="container wide"><h2 class="section_header color4 text-center"> Our Programs</h2>
            <!-- <h2 class="section-title align-center">Our Programs</h2> ><section><div class="container"><div class="row"><div class="col-md-4"><div class="gallery-item"><div class="program-text text-left"><h5 style="color: #fff;"><b>Client Programs</b></h5><ul><li><p>Individual Coaching</p></li><li><p>Group or Organisational Coaching</p></li><li><p>Certified Coach Training</p></li></ul><a href="client-programs.php" class="theme_button color2 margin_0">Read More</a>
            <!-- <a href="#" class="btn btn-style-5">Read More</a> ></div></div></div><div class="col-md-4"><div class="gallery-item1"><div class="program-text text-left"><h5 style="color: #fff;"><b>Partner Programs</b></h5><ul><li><p>Group Coaching for Global Corporate/Social Initiatives</p></li><li><p>Thematic Seminars/Workshops</p></li><li><p>Publications</p></li><li><p>Referral Sharing</p></li></ul><a href="partner-program.php" class="theme_button color2 margin_0">Read More</a>
            <!-- <a href="#" class="btn btn-style-5">Read More</a> ></div></div></div><div class="col-md-4"><div class="gallery-item2"><div class="program-text text-left"><h5 style="color: #fff;"><b>Venture Programs</b></h5><ul><li><p>Life Technology Ventures</p></li><li><p>Social Capital Ventures</p></li></ul><a href="venture1.php" class="theme_button color2 margin_0">Read More</a>
            <!-- <a href="#" class="btn btn-style-5">Read More</a> ></div></div></div></div></div></section></div></div> -->
            <section class="testimonial-section2" style="background-image: url(&quot;assets/images/1920x900_bg.jpg&quot;); ">
              <div class="row text-center">
                <div class="col-12">
                  <!-- <div class="h2">Testimonial</div> -->
                </div>
              </div>
              <div id="testim" class="testim">
                <!--         <div class="testim-cover"> -->
                <div class="wrap">
                  <span id="right-arrow" class="arrow right fa fa-chevron-right"></span>
                  <span id="left-arrow" class="arrow left fa fa-chevron-left "></span>
                  <ul id="testim-dots" class="dots">
                    <li class="dot active"></li>
                    <li class="dot"></li>
                    <li class="dot"></li>
                    <li class="dot"></li>
                  </ul>
                  <div id="testim-content" class="cont">
                    <div class="active">
                      <div class="h4" style="color: #000;font-weight: bold;">Hollistic’s Vision</div>
                      <p>
                        <i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i> To help people understand, witness, and grow towards the magnificence and beauty that they truly are.

                        <i class="fa fa-quote-right" style="font-size: 35px;color: #99152c;" aria-hidden="true"></i>
                      </p>
                    </div>
                    <div>
                      <div class="h4" style="color: #000;font-weight: bold;">Hollisticity</div>
                      <p>
                        <i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>  A state of being where all positive qualities are combined together multi-dimensionally into a single integrated state.

                        <i class="fa fa-quote-right" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>
                      </p>
                    </div>
                    <div>
                      <div class="h4" style="color: #000;font-weight: bold;">H-Love</div>
                      <p>
                        <i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i> The unconditional availability to all beings, with a vision of serving as the
                        <br>“World’s Backup”

                          <i class="fa fa-quote-right" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>
                        </p>
                      </div>
                      <div>
                        <div class="h4" style="color: #000;font-weight: bold;">HCLE</div>
                        <p>
                          <i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i> Hollistic Core Life Education Program with five themes: Self, Perspective, People, Communication, World.


                          <i class="fa fa-quote-right" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <!--         </div> -->
              </section>
              <section class="cs1 section_padding_0 columns_padding_0 columns_margin_0" style="">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-md-12 text-center ">
                      <h3 class="sub-heading " style="padding-top: 50px; color: #fff; padding-bottom: 20px;">
                        <b>Coaching Metrics</b>
                      </h3>
                    </div>
                    <div class="col-md-2 col-sm-6 after_cover color_bg_3">
                      <div class="teaser counter-background-teaser text-center">
                        <span class="counter counter-background counted" data-from="0" data-to="21" data-speed="2100"></span>
                        <h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="9" data-speed="2100">9</h3>
                        <p class="small-text custom-small-text">Years in Coaching (Since 2011)</p>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-6 after_cover color_bg_3">
                      <div class="teaser counter-background-teaser text-center">
                        <span class="counter counter-background counted" data-from="0" data-to="743" data-speed="2100"></span>
                        <h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="58" data-speed="2100">9x</h3>
                        <p class="small-text custom-small-text">Coaching Methodologies</p>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-6 after_cover color_bg_3">
                      <div class="teaser counter-background-teaser text-center">
                        <span class="counter counter-background counted" data-from="0" data-to="99" data-speed="2100"></span>
                        <h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="36" data-speed="2100">	90+
</h3>
                        <p class="small-text custom-small-text">Coaching Tools</p>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 after_cover color_bg_3">
                      <div class="teaser counter-background-teaser text-center">
                        <span class="counter counter-background counted" data-from="0" data-to="15" data-speed="2100"></span>
                        <h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="100" data-speed="2100">90+</h3>
                        <p class="custom-small-text">Coaching Media Developed</p>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 after_cover color_bg_3">
                      <div class="teaser counter-background-teaser text-center">
                        <span class="counter counter-background counted" data-from="0" data-to="15" data-speed="2100"></span>
                        <h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="100" data-speed="2100">9,000+</h3>
                        <p class="custom-small-text">Coaching Concepts Elaborated</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <!-- <section id="contact" class="ls section_padding_top_100"><div class="container"><div class="row"><div class="col-xs-12 bottommargin_0 to_animate" data-animation="fadeInUp"><div class="ds bg_teaser with_padding big-padding"><img src="assets/images/help-form.jpg" alt=""><div class="row columns_padding_30"><div class="col-xs-12 col-sm-9 col-md-7 col-lg-6 col-sm-offset-3 col-md-offset-5 col-lg-offset-6"><h2 class="section_header color3">Do You Need Help?</h2><p class="bottommargin_0">Contact us and we help you to solve any of your problem.</p><form class="contact-form row columns_padding_10" method="post" action="http://webdesign-finder.com/html/diversify/"><div class="col-sm-6"><div class="form-group bottommargin_0"><label for="name">Full Name <span class="required">*</span></label><input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name*"></div></div><div class="col-sm-6"><div class="form-group bottommargin_0"><label for="email">Email address<span class="required">*</span></label><input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email Address*"></div></div><div class="col-sm-12"><div class="form-group bottommargin_0"><label for="message">Message</label><textarea aria-required="true" rows="4" cols="45" name="message" id="message" class="form-control" placeholder="Your Message..."></textarea></div></div><div class="col-sm-12 bottommargin_0"><div class="contact-form-submit topmargin_10"><button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color3 min_width_button margin_0">Send now</button></div></div></form></div></div></div></div></div></div></section> -->
              <!-- <section id="events" class="ls section_padding_top_110"><div class="container"><div class="row">
              <!-- <h2 class="section_header color4"> Our Curriculum</h2> ><div class="col-xs-12 bottommargin_0 to_animate" data-animation="fadeInUp"><div class="row masonry-feed columns_padding_2"><div class="col-xs-12 col-md-4"><div class="row"><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="assets/images/430x420_img1.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Career Guidance</b></a></h3><p class="curriculum-content">Ages 12 - 24</p></div></div></div><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="assets/images/430x420_img2.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Lifestyle</b></a></h3><p class="curriculum-content">For Everyone</p></div></div></div></div></div>
              <!-- <div class="col-xs-12 col-md-6"><div class="vertical-item closest-event content-absolute text-center ds"><div class="item-media"><img src="assets/images/gallery/04.jpg" alt=""></div><div class="item-content darken_gradient"><div id="comingsoon-countdown" data-format="DHMS"></div><h3 class="entry-title"><a href="gallery-single.html">Tuesday All-Day Happy Hour</a></h3><div class="entry-meta inline-content"><span><i class="fa fa-map-marker highlight4 rightpadding_5" aria-hidden="true"></i>
                  229 Verna Viaduct, new york
                </span><span><i class="fa fa-clock-o highlight4 rightpadding_5" aria-hidden="true"></i>
                  2:00 pm to 2:00 am
                </span></div></div></div></div> -><div class="col-xs-12 col-md-4"><div class="row"><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="assets/images/430x420_img3.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Productivity</b></a></h3><p class="curriculum-content">For everyone</p></div></div></div><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="assets/images/430x420_img6.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Mind & Spirit</b></a></h3><p class="curriculum-content">Adults & Millenials</p></div></div></div></div></div><div class="col-xs-12 col-md-4"><div class="row"><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="assets/images/430x420_img5.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Health & Fitness</b></a></h3><p class="curriculum-content">For Everyone</p></div></div></div><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="assets/images/430x420_img4.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Love & Relationships</b></a></h3><p class="curriculum-content">Find Your Fit</p></div></div></div></div></div></div></div></div></div></section> -->
              <!-- <section class="ls page_portfolio section_padding_top_0 section_padding_bottom_0 columns_padding_0 container_padding_0"><div class="container-fluid"><div class="row"><div class="col-sm-12"><div class="row masonry-layout columns_margin_0" data-filters=".isotope_filters"><div class="col-md-6"><div class="custom-gallery-items"><div class="items-content"><h2 class="custom-gallery-content"><b>Do you need life improvement?</b></h2><p class="custom-gallery-text">No matter what you’re facing, we’d love to guide you with wisdom!</p>
              <!-- <a class="theme_button min_width_button color2 inverse" href="#" style="color: #fff;">Read More</a> --
                    <a href="contact.php" class="btn btn-style-7 btn-big">Connect With Us</a></div></div></div><div class="col-md-6"><div class="custom-gallery-items1"><div class="items-content"><h2 class="custom-gallery-content"><b>Want to help others improve?</b></h2><p class="custom-gallery-text">Your financial support is very important for our global projects.</p>
              <!-- <a class="theme_button min_width_button color2 inverse" href="#" style="color: #fff;">Read More</a> --
                    <a href="programs.php" class="btn btn-style-7 btn-big">Join Program</a></div></div></div></div></div></div></div></section> -->
              <div class="modal fade" id="ModalScrollable" tabindex="-1" role="dialog" aria-labelledby="ModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title sub-heading" id="ModalScrollableTitle">About Hollistic</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p style="color: #464545">Hollistic has embraced all the formal learnings of its founder, Gorhav Kanal, as well as his own invented ideas, to offer an integrated way of understanding people’s minds with only one goal: life improvement and growth towards “hollisticity”, a state of being, where all positive qualities are combined together multi-dimensionally into a single integrated state.
</p>
                      <p style="color: #464545;">Hollistic’s approach to Life Improvement is threefold: through our projects and ventures in Multi-Dimensional Coaching, Life Technology, and Social Capital. With the help of “H-Love”, i.e. our unconditional availability for others, with an ideal of serving as the “World’s Backup”, we work through a combination of consulting, coaching, and investing in Life Improvement ideas. Our highest common goal in our work is to enable ourselves as well as others to see, think, feel and live with our hearts at the center of all that we do.
</p>
                      <p style="color: #464545;">
                        <b> “Hollistic” is a registered trademark in the US (pending), India (pending), and Singapore (completed).
</b>
                      </p>
                    </div>
                    <!-- <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary">Save changes</button></div> -->
                  </div>
                </div>
              </div>
              <div class="modal fade" id="contentModal" tabindex="-1" role="dialog" aria-labelledby="contentModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Staytune</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p style="color: #464545;">We are adding content yet.</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary modal-close-btn" data-dismiss="modal">Close</button>
                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- <section id="team" class="ls section_padding_top_90" style="padding-bottom: 120px;"><div class="container"><div class="row"><div class="col-md-12"><h2 class="section_header color our-coaches-heading">Our Coaches</h2></div></div><div class="row"><div class="col-md-3"><a href="#" class="member-photo"><img src="assets/images/315x336_photo1.jpg" alt=""></a><div class="team-desc"><div class="member-info"><h5 class="member-name"><a href="#">Robert Smith</a></h5><div class="member-position">Lead Pastor</div></div></div></div><div class="col-md-3"><a href="#" class="member-photo"><img src="assets/images/315x336_photo2.jpg" alt=""></a><div class="team-desc"><div class="member-info"><h5 class="member-name"><a href="#">Bradley Grosh</a></h5><div class="member-position">Campus Pastor</div></div></div></div><div class="col-md-3"><a href="#" class="member-photo"><img src="assets/images/315x336_photo3.jpg" alt=""></a><div class="team-desc"><div class="member-info"><h5 class="member-name"><a href="#">Vicky Johnson</a></h5><div class="member-position">Director of Children’s Ministry</div></div></div></div><div class="col-md-3"><a href="#" class="member-photo"><img src="assets/images/315x336_photo4.jpg" alt=""></a><div class="team-desc"><div class="member-info"><h5 class="member-name"><a href="#">Adam Johnson</a></h5><div class="member-position">Director of Student’s Ministry</div></div></div></div></div></div></section> -->
              <script>

// vars
'use strict'
var	testim = document.getElementById("testim"),
  testimDots = Array.prototype.slice.call(document.getElementById("testim-dots").children),
  testimContent = Array.prototype.slice.call(document.getElementById("testim-content").children),
  testimLeftArrow = document.getElementById("left-arrow"),
  testimRightArrow = document.getElementById("right-arrow"),
  testimSpeed = 7500,
  currentSlide = 0,
  currentActive = 0,
  testimTimer,
  touchStartPos,
  touchEndPos,
  touchPosDiff,
  ignoreTouch = 30;
;

window.onload = function() {

  // Testim Script
  function playSlide(slide) {
      for (var k = 0; k < testimDots.length; k++) {
          testimContent[k].classList.remove("active");
          testimContent[k].classList.remove("inactive");
          testimDots[k].classList.remove("active");
      }

      if (slide < 0) {
          slide = currentSlide = testimContent.length-1;
      }

      if (slide > testimContent.length - 1) {
          slide = currentSlide = 0;
      }

      if (currentActive != currentSlide) {
          testimContent[currentActive].classList.add("inactive");
      }
      testimContent[slide].classList.add("active");
      testimDots[slide].classList.add("active");

      currentActive = currentSlide;

      clearTimeout(testimTimer);
      testimTimer = setTimeout(function() {
          playSlide(currentSlide += 1);
      }, testimSpeed)
  }

  testimLeftArrow.addEventListener("click", function() {
      playSlide(currentSlide -= 1);
  })

  testimRightArrow.addEventListener("click", function() {
      playSlide(currentSlide += 1);
  })

  for (var l = 0; l < testimDots.length; l++) {
      testimDots[l].addEventListener("click", function() {
          playSlide(currentSlide = testimDots.indexOf(this));
      })
  }

  playSlide(currentSlide);

  // keyboard shortcuts
  document.addEventListener("keyup", function(e) {
      switch (e.keyCode) {
          case 37:
              testimLeftArrow.click();
              break;

          case 39:
              testimRightArrow.click();
              break;

          case 39:
              testimRightArrow.click();
              break;

          default:
              break;
      }
  })

  testim.addEventListener("touchstart", function(e) {
      touchStartPos = e.changedTouches[0].clientX;
  })

  testim.addEventListener("touchend", function(e) {
      touchEndPos = e.changedTouches[0].clientX;

      touchPosDiff = touchStartPos - touchEndPos;

      console.log(touchPosDiff);
      console.log(touchStartPos);
      console.log(touchEndPos);


      if (touchPosDiff > 0 + ignoreTouch) {
          testimLeftArrow.click();
      } else if (touchPosDiff < 0 - ignoreTouch) {
          testimRightArrow.click();
      } else {
        return;
      }

  })
}



              </script>
<!-- end body content -->
<footer class="page_footer template_footer ds ms parallax overlay_color section_padding_top_110 section_padding_bottom_100 columns_padding_25">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div id="subscribe" class="widget widget_mailchimp with_padding cs main_color2 parallax overlay_color topmargin_0">
					<h3 class="widget-title">Our Newsletter</h3>
					<form class="signup" action="subscribe.php" method="get">
						<div class="form-group-wrap">
							<div class="form-group">
								<label for="mailchimp" class="sr-only">Enter your email here</label>
								<input name="email" type="email" autocomplete="off" id="mailchimp" class="mailchimp_email form-control text-center" placeholder="Your Email Address">
								</div>
								<button type="submit" class="theme_button color2">Subscribe now</button>
							</div>
							<div class="response"></div>
						</form>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-center">
					<div class="widget widget_text widget_about">
						<a href="index.php" class="logo logo_with_text bottommargin_10">
							<img src="assets/images/logo5.png" alt="">
								<span class="logo_text">
                            Hollistic<sup style="font-size:11px;top: -1.5em;">&reg;</sup>
									<small style="color: #6993cd;border-color: #6993cd; font-weight: 700;margin-left: -7%;">LIFE IMPROVEMENT</small>
								</span>
							</a>
							<p>Any and all information displayed here is copyright of Hollistic<sup style="font-size: 11px;">®</sup> unless another source or copyright-holder is cited. If any information has been addressed or sent to any person or organisation, it may be of a confidential nature and intended solely for the addressee(s).
								<a href="#" style="color: #acadb8;" data-toggle="modal" data-target="#hollisticModal"> Read More...</a>
							</p>
							<p class="topmargin_25">
								<a class="social-icon border-icon rounded-icon socicon-youtube" target="_blank" href="https://www.youtube.com/channel/UCJzsP1_r3YoNVNdf06n4kXw" title="Youtube"></a>
								<a class="social-icon border-icon rounded-icon socicon-blogger" target="_blank" href="https://www.blogger.com/u/2/blogger.g#welcome" title="Twitter"></a>
								<a class="social-icon border-icon rounded-icon socicon-vimeo" target="_blank" href="https://vimeo.com/hollistic" title="vimeo" title="vimeo"></a>
								<a class="social-icon border-icon rounded-icon socicon-instagram" target="_blank" href="https://www.facebook.com/hollisticmedia" title="instagram"></a>
								<a class="social-icon border-icon rounded-icon socicon-facebook" target="_blank" href="https://www.facebook.com/hollisticmedia" title="Linkedin"></a>
								<a class="social-icon border-icon rounded-icon socicon-pinterest" target="_blank" href="https://in.pinterest.com/hollisticmedia/" title="Youtube"></a>
								<a class="social-icon border-icon rounded-icon socicon-tumblr" target="_blank" href="https://www.tumblr.com/blog/hollisticmedia" title="Tumblr"></a>
								<a class="social-icon border-icon rounded-icon socicon-snapchat" target="_blank" href="https://www.snapchat.com/add/hollisticmedia" title="snapchat"></a>
								<a class="social-icon border-icon rounded-icon socicon-reddit" target="_blank" href="https://www.reddit.com/user/hollisticmedia" title="reddit"></a>
								<a class="social-icon border-icon rounded-icon socicon-quora" target="_blank" href="https://www.quora.com/profile/Hollistic-Media" title="quorq"></a>
								<a class="social-icon border-icon rounded-icon socicon-flickr" target="_blank" href="https://www.flickr.com/photos/170872704@N04/" title="flickr"></a>
							</p>
						</div>
					</div>
					<!-- <div class="col-md-3 col-sm-6 col-xs-12 text-center"><div class="widget widget_text"><h3 class="widget-title">New Here?</h3><ul class="list-unstyled greylinks"><li class="media"><a href="partner-program.php"> Partner Programs </a></li><li class="media"><a href="client-programs.php"> Client Programs </a></li><li class="media"><a href="venture-program.php">  Venture Programs </a></li></ul></div></div> -->
					<div class="col-md-6 col-sm-6 col-xs-12 text-center">
						<div class="widget widget_text">
							<h3 class="widget-title">Quick Links</h3>
							<ul class="list-unstyled greylinks">
								<div class="row">
									<div class="col-xs-6">
										<li class="media">
											<a href="index.php"> Home </a>
										</li>
										<li class="media">
											<a href="about.php"> About Us </a>
										</li>
										<li class="media">
											<a href="programs.php">Coaching</a>
										</li>
										<li class="media">
											<a href="ventures.php"> Ventures </a>
										</li>
										<li class="media">
											<a href="capital.php"> Capital </a>
										</li>
									</div>
									<div class="col-xs-6">
										<li class="media">
											<a href="content.php"> Content </a>
										</li>
										<li class="media">
											<a href="schedule.php"> Schedule </a>
										</li>
										<li class="media">
											<a href="merchandise.php"> Merchandise </a>
										</li>
										<li class="media">
											<a href="faq.php"> Help</a>
										</li>
										<li class="media">
											<a href="contact.php"> Contact Us </a>
										</li>
									</div>
								</div>
							</ul>
						</div>
					</div>
					<!-- <div class="col-md-4 col-sm-6 col-xs-12 text-center"><div class="widget widget_text"><h3 class="widget-title">Our Contacts</h3><ul class="list-unstyled greylinks"><li class="media"><i class="fa fa-map-marker highlight rightpadding_5" aria-hidden="true"></i>  73 Harvey Forest Suite, Singapore </li><li class="media"><a href="tel:+919577119900"><i class="fa fa-phone highlight rightpadding_5" aria-hidden="true"></i>
										+91-95-7711-9900 </a></li><li class="media"><i class="fa fa-pencil highlight rightpadding_5" aria-hidden="true"></i><a href="mailto:H-Love@Hollistic.org" > H-Love@Hollistic.org</a></li><li class="media"><i class="fa fa-clock-o highlight rightpadding_5" aria-hidden="true"></i> Mon-Fri: 9:00-19:00, Sat: 10:00-17:00 </li></ul></div></div> -->
					<!-- <div class="col-md-3 col-sm-6 col-xs-12 hidden-xs hidden-sm text-center"><div class="widget widget_recent_posts"><h3 class="widget-title">Recent Blogs</h3><ul class="list-unstyled greylinks"><li><p><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</a></p><div class="entry-meta inline-content greylinks"><span><i class="fa fa-calendar highlight3 rightpadding_5" aria-hidden="true"></i><a href="#"><time datetime="2017-10-03T08:50:40+00:00">
										17 jan, 2018</time></a></span><span><i class="fa fa-user highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">Admin</a></span><span class="categories-links"><i class="fa fa-tags highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">lgbt</a></span></div></li><li><p><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></p><div class="entry-meta inline-content greylinks"><span><i class="fa fa-calendar highlight3 rightpadding_5" aria-hidden="true"></i><a href="#"><time datetime="2017-10-03T08:50:40+00:00">
										17 jan, 2018</time></a></span><span><i class="fa fa-user highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">Admin</a></span><span class="categories-links"><i class="fa fa-tags highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">Serivces</a></span></div></li></ul></div></div> -->
				</div>
			</div>
		</footer>
		<section class="ds ms parallax page_copyright overlay_color section_padding_top_30 section_padding_bottom_30">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p style="color: #fff;font-size: 15px;"> Copyright &copy; 2015-2019
							<a href="http://www.hollistic.org/" style="color: #fff" target="_blank">Hollistic.org</a>. All Rights Reserved.
						</p>
					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- eof #box_wrapper -->
</div>
<div class="modal fade" id="hollisticModal" tabindex="-1" role="dialog" aria-labelledby="hollisticModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Hollistic</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Any and all information displayed here is copyright of Hollistic® unless another source or copyright-holder is cited. If any information has been addressed or sent to any person or organisation, it may be of a confidential nature and intended solely for the addressee(s). Any disclosure, copying, or distribution of the contents of the information in any form is strictly prohibited and is unlawful. The information provided herein by Hollistic does not constitute legal, professional or commercial advice, whatsoever. While every care has been taken to ensure that the content is useful and accurate, Hollistic gives no warranty, guarantee, undertaking in this respect, and does not accept any legal liability or responsibility for the content or the accuracy of the information so provided, or, for any loss or damage caused arising directly or indirectly in connection with reliance on the use of such information.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary modal-close-btn" data-dismiss="modal">Close</button>
				<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
			</div>
		</div>
	</div>
</div>
<!-- eof #canvas -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="assets/js/compressed.js"></script>
<script src="assets/js/selectize.min.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/slick.min.js"></script>
<script src="assets/js/lightbox.js"></script>
<script src="assets/js/hammer.js"></script>
<script src="assets/js/jquery.hammer.js"></script>
<script type="text/javascript">

    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })

		$('#lightbox').hammer().on("swipe", function (event) {
                if (event.gesture.direction === 4) {
                	console.log('swipe');
                    $('#lightbox a.lb-prev').trigger('click');
                } else if (event.gesture.direction === 2) {
                	console.log('swipe');
                    $('#lightbox a.lb-next').trigger('click');
                }
            });
		$(function(){
		    var cururl = window.location.pathname;
			var curpage = cururl.substr(cururl.lastIndexOf('/') + 1);
			var hash = window.location.hash.substr(1);
			if((curpage == "" || curpage == "/" || curpage == "admin") && hash=="")
			{
			//$("nav .navbar-nav > li:first-child").addClass("active");
			}
			else
			{
			$(".mainmenu li").each(function()
			{
			    $(this).removeClass("active");
			});
			if(hash != "")
			$(".mainmenu li a[href*='"+hash+"']").parents("li").addClass("active");
			else
			$(".mainmenu li a[href*='"+curpage+"']").parents("li").addClass("active");
			}
		});
		const items = document.querySelectorAll(".accordion a");

			function toggleAccordion(){
			  this.classList.toggle('active');
			  this.nextElementSibling.classList.toggle('active');
			}

			items.forEach(item => item.addEventListener('click', toggleAccordion));
		$('#view-more').click(function(event)
		{
			event.preventDefault();
			var pin = '1234' // Will be fetched from backend.
			var upin = $('#gpin').val();

			if(pin == upin)
			{
				$('#gallery-accessor').hide();
				$('#private-gallery').show();

			} else {
				$('#gpin').css('border-color', 'red');
				$('#gpin-success').hide();
				$('#gpin-message').show();
			}

		});
</script>
<script>
		 // $('.slick-slider').slick();
		 // $(".slick-slider").not('.slick-initialized').slick()
	</script>
<!-- <script src="assets/js/switcher.js"></script> --></body><!-- Mirrored from webdesign-finder.com/html/diversify/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 01 Dec 2018 12:19:26 GMT --></html>
<!-- footer -->

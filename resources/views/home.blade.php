@extends('layouts.app')

@section('page', 'My Account')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>My Account</h2>
                    <ol class="breadcrumb greylinks">
                        <li> <a href="{{url('/')}}">
                            Home
                        </a> </li>

                        <li class="active"><a href="{{url('/home')}}" >My Account</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>


    <section class="section_padding_top_65 section_padding_bottom_65 individual-coaching-section">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                	<div class="account-panel">
                		<ul class="account-sidebar">
                			<li><a href="{{url('/my-programs')}}">My Programs</a></li>
                			<li><a href="{{url('/settings')}}">Account Settings</a></li>
                			<li><a href="{{url('program-documents')}}">Program Documents</a></li>
                			<li>
                        <a href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                      </li>
                		</ul>
                	</div>
                </div>
                 <div class="col-md-9">
                 		<h2>My Programs</h2>
                 		<hr/>
                </div>

            </div>
        </div>
    </section>
@endsection

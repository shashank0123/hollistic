@extends('admin.app')

@section('content')



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Category
      <small>Add New Category</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Category</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">


        <div class="box">

          @if($errors->any())
          <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </div>
          @endif

          @if($message = Session::get('message'))
          <div class="btn btn-primary" style="width: 100%">
            <p>{{ $message }}</p>
          </div>
          @endif
          <br><br>
          <a href="{{url('/')}}/admin/categories-list" style="padding: 2%"><button type="submit" class="btn btn-primary" style="padding: 0.5% 3%">Back</button> </a><br><br>



          <div class="box-header">
            <h3 class="box-title">Enter details Here
              {{-- <small>Simple and fast</small> --}}
            </h3>
            <!-- /. tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body pad">
            <form method="POST" action="{{url('/')}}/admin/add-category-success" enctype="multipart/form-data">
              @csrf
              <div class="box-body">

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="category_name">Category Name</label>
                      <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter Page Name Here" required>
                    </div>                    
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="base">Category Type</label>
                      <select class="form-control" name="base" id="base">     
                        <option value="">Select Option</option>
                        <option value="Product">Product</option>
                        <option value="Program">Program</option>
                      </select> 
                    </div>                    
                  </div>
                </div>

                <div class="form-group">
                  <label for="status">Status</label>
                  <select class="form-control" name="status" id="status">     
                    <option value="">Select Option</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>             
                </div>                
                
              </div>
              <!-- /.box-body -->              

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('script')

<script>
  $('#state').on('change',function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var state = $('#state').val();
    
    $.ajax({
      type: 'POST',
      url: '/admin/ajaxData',
      data: {_token: CSRF_TOKEN, state_id : state },
      success:function(data){
        $('#city').html(data);
      }
    });
  });

</script>


@endsection
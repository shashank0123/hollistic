@extends('admin.app')

@section('content')



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Marchantise Product
			<small>Edit Marchantise Product</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">Marchantise Product</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">			

				<div class="box">

					@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</div>
					@endif

					@if($message = Session::get('message'))
					<div class="btn btn-primary" style="width: 100%">
						<p>{{ $message }}</p>
					</div>
					@endif
					<br><br>
					<a href="{{url('/')}}/admin/products-list" style="padding: 2%"><button type="submit" class="btn btn-primary" style="padding: 0.5% 3%">Back</button> </a><br><br>

					<div class="box-header">
						<h3 class="box-title">Edit details Here
							{{-- <small>Simple and fast</small> --}}
						</h3>
						<!-- /. tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body pad">
						<form method="POST" action="{{url('/')}}/admin/edit-product/{{$product->id}}" enctype="multipart/form-data">
							@csrf
							<div class="box-body">
								<div class="row">
									<div class="col-sm-8">	

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="product_name">Name</label>
													<input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter Product Name Here" value="{{$product->product_name}}">
												</div>					
											</div>
										</div>

										<div class="form-group">
											<label for="image_url">Image</label>
											<input type="file" class="form-control" name="image_url" id="image_url">        
										</div>

										{{-- <div class="form-group">
											<label for="video_url">Video URL</label>
											<input type="text" class="form-control" name="video_url" id="video_url" value="{{$product->video_url}}">        
										</div> --}}

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="price">Price</label>
													<input type="text" class="form-control" id="price" name="price" placeholder="Enter Product Name Here" value="{{$product->price}}">
												</div>					
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="short_description">Short Description</label>
													<input type="text" class="form-control" id="short_description" name="short_description" placeholder="Enter Short Description Here" value="{{$product->short_description}}">
												</div>						
											</div>
										</div>

										<div class="col-sm-4" style="text-align: center;">
											<div style="padding: 5%">
												<img src="{{url('/')}}/products-img/{{$product->image_url}}" style="width: 250px; height: 150px" />
											</div>

											{{-- @if(!empty($product->video_url))
											<div style="padding: 5%">
												<video width="300" height="200" controls>
													<source src="{{$product->video_url}}" type="video" />
													</video>
												</div>
												@endif --}}
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="description">Full Description</label>
													<textarea class="textarea" name="description" placeholder="Place description here"
													id="description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"> {{$product->description}}</textarea>
												</div>			
											</div>
										</div>					

										<div class="form-group">
											<label for="status">Status</label>
											<select class="form-control" name="status" id="status">     
												<option value="">Select Option</option>
												<option value="1" @if($product->status == '1') {{'selected'}} @endif>Active</option>
												<option value="0" @if($product->status == '0') {{'selected'}} @endif>Deactive</option>
											</select>             
										</div>

									</div>
									<!-- /.box-body -->              

									<div class="box-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /.col-->
				</div>
				<!-- ./row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->


		@endsection
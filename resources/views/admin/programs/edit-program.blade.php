@extends('admin.app')

@section('content')



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Program
			<small>Edit New Program</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">Program</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">			

				<div class="box">

					@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</div>
					@endif

					@if($message = Session::get('message'))
					<div class="btn btn-primary" style="width: 100%">
						<p>{{ $message }}</p>
					</div>
					@endif
					<br><br>
					<a href="{{url('/')}}/admin/programs-list" style="padding: 2%"><button type="submit" class="btn btn-primary" style="padding: 0.5% 3%">Back</button> </a><br><br>

					<div class="box-header">
						<h3 class="box-title">Edit details Here
							{{-- <small>Simple and fast</small> --}}
						</h3>
						<!-- /. tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body pad">
						<form method="POST" action="{{url('/')}}/admin/edit-program/{{$program->id}}" enctype="multipart/form-data">
							@csrf
							<div class="box-body">
								<div class="row">
									<div class="col-sm-8">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="title">Title (Heading)</label>
													<input type="text" class="form-control" id="title" name="title" placeholder="Enter Title Here" value="{{$program->title}}">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="price_per_person">Price (Per Person)</label>
													<input type="text" class="form-control" id="price_per_person" name="price_per_person" placeholder="Author Name" value="{{$program->price_per_person}}">
												</div>						
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="duration">Duration</label>
													<input type="text" class="form-control" id="duration" name="duration" placeholder="Author Name" value="{{$program->duration}}">
												</div>						
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label for="short_description">Short Description</label>
													<input type="text" class="form-control" id="short_description" name="short_description" placeholder="Enter Short Description Here" value="{{$program->short_description}}">
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-4">
										<div style="padding: 5%">
											<img src="{{url('/')}}/programs-img/{{$program->image_url}}" style="width: 200px; height: 200px" />
										</div>
									</div>

								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label for="description">Full Description</label>
											<textarea class="textarea" name="description" placeholder="Place some text here"
											id="description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"> {{$program->description}}</textarea>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label for="image_url">Image</label>
									<input type="file" class="form-control" name="image_url" id="image_url">        
								</div>

								<div class="form-group">
									<label for="status">Status</label>
									<select class="form-control" name="status" id="status">     
										<option value="">Select Option</option>
										<option value="1" @if($program->status == '1') {{'selected'}} @endif>Active</option>
										<option value="0" @if($program->status == '0') {{'selected'}} @endif>Inactive</option>
									</select>             
								</div>

							</div>
							<!-- /.box-body -->              

							<div class="box-footer">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.col-->
		</div>
		<!-- ./row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


@endsection
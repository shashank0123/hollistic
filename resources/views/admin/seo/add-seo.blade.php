@extends('admin.app')

@section('content')



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add SEO
      <small>Add New SEO</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">SEO</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">


        <div class="box">

          @if($errors->any())
          <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </div>
          @endif

          @if($message = Session::get('message'))
          <div class="btn btn-primary" style="width: 100%">
            <p>{{ $message }}</p>
          </div>
          @endif
          <br><br>
          <a href="{{url('/')}}/admin/seos-list" style="padding: 2%"><button type="submit" class="btn btn-primary" style="padding: 0.5% 3%">Back</button> </a><br><br>



          <div class="box-header">
            <h3 class="box-title">Enter details Here
              {{-- <small>Simple and fast</small> --}}
            </h3>
            <!-- /. tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body pad">
            <form method="POST" action="{{url('/')}}/admin/add-seo-success" enctype="multipart/form-data">
              @csrf
              <div class="box-body">

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="page_name">Page Name</label>
                      <input type="text" class="form-control" id="page_name" name="page_name" placeholder="Enter Page Name Here" required>
                    </div>                    
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="title">Title</label>
                      <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title Here" required>
                    </div>                    
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="keywords">Keywords</label>
                      <input type="text" class="form-control" id="keywords" name="keywords" placeholder="Enter Keywords Here" required>
                    </div>                    
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="tags">Tags</label>
                      <input type="text" class="form-control" id="tags" name="tags" placeholder="Enter Tags Here" required>
                    </div>                    
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-12">
                      <label for="meta_desc">Meta Description</label>
                      <textarea class="textarea" name="meta_desc" placeholder="Place some text here"
                      id="meta_desc" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>
                    </div>
                  </div>
                </div>                
                
              </div>
              <!-- /.box-body -->              

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('script')

<script>
  $('#state').on('change',function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var state = $('#state').val();
    
    $.ajax({
      type: 'POST',
      url: '/admin/ajaxData',
      data: {_token: CSRF_TOKEN, state_id : state },
      success:function(data){
        $('#city').html(data);
      }
    });
  });

</script>


@endsection
@extends('layouts.app')

@section('page', 'Merchandise')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>Merchandise</h2>
				<ol class="breadcrumb greylinks">
					<li>
						<a href="{{url('/')}}">
							Home
						</a>
					</li>
					<!-- <li><a href="#">Sale</a></li> -->
					<li class="active">
						<a href="{{url('/merchandise')}}"> Merchandise </a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_100 columns_padding_30">
	<div class="container">
		<div class="row">
			<p class="sub-heading" style="color: #000; font-size: 15px;">Support our work and vision by shopping for your favourite Hollistic-branded gear here!</p>
			<br>
				<div class="col-sm-7 col-md-8 col-lg-8 col-sm-push-5 col-md-push-4 col-lg-push-4">

				</div>
			</div>
		</div>
	</section>
@endsection

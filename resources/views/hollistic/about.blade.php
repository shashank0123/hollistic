@extends('layouts.app')

@section('page', 'About Us')

@section('content')

<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2>About Us</h2>
        <ol class="breadcrumb greylinks">
          <li>
            <a href="{{url('/')}}">
              Home
            </a>
          </li>
          <!-- <li><a href="#">Pages</a></li> -->
          <li class="active">
            <a href="{{url('/about')}}" style=" "> About Us</a>
          </li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- <section id="about" class="ls section_padding_top_110 columns_padding_30" style="padding-bottom: 100px;"><div class="container"><div class="row"><div class="col-md-6 col-md-push-6" data-animation="fadeInUp" data-delay="600"><div class="embed-responsive embed-responsive-3by2"><a href="https://www.youtube.com/embed/xKxrkht7CpY" class="embed-placeholder"><img src="images/gallery/01.jpg" alt=""></a></div></div><div class="col-md-6 col-md-pull-6" data-animation="fadeInRight" data-delay="300"><h2 class="section_header color4"> What is Diversify </h2><p class="section-excerpt grey">The Center is committed to a policy of non-discrimination in employment and in the provision of all services.</p><p>The mission of The Diversify LGBT Community Center is to enhance and sustain the health and well-being of the lesbian, gay, bisexual, transgender and HIV communities by providing activities, programs and services that create community.Empower community
              members; provide essential resources; advocate for civil and human rights; and embrace, promote and support our cultural diversity.</p></div></div></div></section>
            -->
            <section class="section_padding_top_65 individual-coaching-section">
              <div class="container">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h3 class="sub-heading">
                      <b class="about-title">About Hollistic</b>
                    </h3>
                    <br>
                    <center>
                      <img class="about-top-img about-img" src="images/a2.jpg">
                    </center>
                    <br>
                    <p class="para1">Hollistic is an organisation specialised in the area of Life Improvement, with three primary approaches: Multi-Dimensional Coaching for individuals and organisations, building out Humanity-Oriented Ventures that can make a difference to society, and the investing of Conscious Capital in projects that can impact a great number of lives.</p>
                    <p class="para1">Using the medium of “H-Love”, i.e. our unconditional availability for all
                      beings, Hollistic abides by the ideal of serving as the “World’s Backup”. Our
                      highest common goal in our work is to enable ourselves as well as others to
                    see, think, feel, and live with our hearts at the centre of all that we do.</p>
                    <h4 class="about-h4" id="response-about">Hollistic<sup style="font-size: 11px;">&reg;</sup> is a registered trademark in multiple countries and classes.
                    </h4>
                    
                  </div>
                </div>
              </div>
              <div class="container div2">
                <hr>
                <br>
                <div class="row">
                  <div class="col-md-4 gk-img">
                    <img class="img-left" src="images/gk.jpg" alt="">
                  </div>
                  <div class="col-md-8">
                    <!--<div class="pre-title color2" style="color: #464545; font-size: 17px;font-family: Verdana,Geneva,sans-serif;"><b><sup style="font-size: 11px;">&reg;</sup></b></div>-->
                    <h2 class="about-name">About Gorhav Kanal
                      <br/>
                      <span class="about-subhead">Founder of Hollistic<b><sup class="div2-sup">&reg;</sup>
                      </b>
                    </span>
                  </h2>
                  <h4 class="div2-h4">Gorhav Kanal founded Hollistic<sup class="div2-sup">&reg;</sup> in 2015 in his hometown, Mumbai, India.
                  </h4>
                  <p class="div2-p">As he grew more and more to understand the nuances of life through his
                    work, explorations, and connections with other people, Gorhav says that
                    there sparked in his heart a fire that was born out of witnessing events of
                    sheer injustice. These experiences affected him deeply and made him want
                    to alter his direction and choices in life and put everything into his
                    understanding of people and the world, eventually leading to the creation
                    of Hollistic, with a broad vision of being unconditionally available to all
                    people in order to help them grow and transform their lives. Gorhav’s driving
                    motive and the purpose behind his work through his creation of Hollistic is to
                    help people understand, witness, and grow towards the magnificence and
                  beauty that they truly are.</p>
                  <img class="gk-resp-img img-div2" src="images/gk.jpg" alt="">
                  <p class="div2-p">Prior to Hollistic, Gorhav founded and ran Kanal, Inc., a technology company
                    that operated an e-commerce product and service arbitrage business based in New York,
                    from 2005 to 2015. Gorhav has also been a private investor in the global
                  financial markets for over 10 years.</p>

                </div>
              </div>
              <div class="row">
                <div class="col-md-7">
                  <!-- /<h4>Gorhav Kanal founded Hollistic in 2015 in his hometown, Mumbai, India.</h4> -->
                  <P class="div2-p">Gorhav graduated from Cornell University’s College of Engineering (Ithaca,
                    NY, USA) with a B.S. in Computer Science and a specialisation in Artificial
                    Intelligence, in 2002. He continued being a student even after graduating,
                    taking continuing/executive education courses from a number of universities
                  in the US and Europe.</P>
                  <img class="gk-resp-img-2 img-div2" src="images/about/ceo.jpg" alt="">
                  <P class="div2-p">After over 5 years of exploratory personal development work, to jumpstart
                    himself into the broader journey of learning about human development and
                    the human mind, Gorhav designed and completed his own Master’s
                    programme equivalent in Integrated Multi-Disciplinary Studies, with a
                    specialisation in Mind Coaching, from 2016 to 2018. The programme was
                    completed through a combination of online and in-person courses across the
                    Asia-Pacific region, with Singapore serving as the base. The curriculum comprised of over a hundred modules from the fields of
                    philosophy, psychology, spirituality, body-mind therapies, neuro-linguistic
                    programming (NLP), individual and organisational coaching, peace and
                  conflict resolution, mediation and facilitation, amongst other mind and life-oriented trainings.</P>
                </div>
                <div class="col-md-5 gk-img-2">
                  <img src="images/about/ceo.jpg" alt="" class="img-div3">
                </div>
              </div>
            </div>
            <div class="container myth-container">
              <hr class="myth-border">
              <!--<br>-->
              <div class="row">
                <div class="col-md-12 text-left">
                  <center>
                    <h3 class="sub-heading">
                      <b class="about-title">The Myth of Mr. H</b>
                    </h3>
                  </center>
                  <br>
                  <center>
                    <img class="about-top-img about-img" src="images/a3.jpg" >
                  </center>
                  <br>
                  <!-- <div class="pre-title color2" style="color: #464545; font-size: 17px;font-family: Verdana,Geneva,sans-serif;"><b>Founder of Hollistic<sup style="font-size: 11px;">&reg;</sup></b></div> -->
                  <!--  <h2 style="margin-bottom: 20px;text-align:left;"></h2> -->
                  <p class="div3-p>While there is no clear evidence for how Mr. H came into being, there is
                  known to be a myth that is spoken of, and the story goes as follows:</p>
                  <p class="div3-p" style="font-size: 15px; color: #000; text-align:left; font-family: Verdana,Geneva,sans-serif !important;">There was once a Little Prince, growing up in a big city in the Land of
                    Elephants, and a Beautiful Nurse from a mystical world far, far away, was
                    brought in to help bring him up over a long period of time. One fine day,
                    while the Little Prince&#39;s eyes were closed, the Beautiful Nurse decided to cast
                    a spell on him that would send him into a deep abyss, and just before
                    disappearing, she openly revealed to the Little Prince that she was proof that
                    the world was not always what it seemed. The Little Prince was stunned that
                    the Beautiful Nurse could not see the beauty that was within herself and
                    chose the path that she did. Shocked and dismayed, the Little Prince then
                    found himself inside of a trap that seemed to have no clear exit. The more

                    he thought about what the Beautiful Nurse had done, the deeper he got
                  sucked in to the abyss.</p>
                  <p class="div3-p">However, not willing to give up, the Little Prince seeked to learn the magical
                    laws of coaching, in order to save himself from the seemingly never-ending
                    downward spiral that he had found himself in. As he grew more and more to
                    learn the art and craft of coaching and bring himself out of the magnetic
                    chasm that the Beautiful Nurse had created, the Little Prince began to see
                    that there were others that had fallen into similar traps and situations that
                    their own minds would not allow them to be free from. Deeply empathizing
                    with these people, having been in the same place before, the Little Prince
                    knew that this was to be his calling, his raison d&#39;etre, to help people not only
                    get through challenging situations, where they may find themselves lost and
                    helpless, but to help them realize their greatest growth and truest, most
                  magnificent selves, emerge out of these very events.</p>
                  <p class="div3-p">The Little Prince had then decided that he would look to discover a state
                    where he could feel internally complete, despite the world presenting him
                    with all kinds of challenging situations and events. He called this the quality
                    of being
                    <b>Hollistic</b>. When he looked deeply to understand what this state was
                    all about, he began to see that it was far more complex than he had initially
                    imagined, for it contained within it all of nature&#39;s beauty and wonder, which
                    presented itself in a multi-dimensional form. Further, despite having gone
                    through many unexpectedly challenging events, the Little Prince continued
                    to have faith that there is a way to achieve and hold a state of unity with
                    oneself and the universe, where all positive qualities of nature could be
                    witnessed together, and referred to this as a state of
                    <b>Hollisticity</b>.
                  </p>
                  <p class="div3-p">As the Little Prince grew to understand the world better, he began to want to
                    share all of his learnings with other people. In fact, he even wanted these
                    valuable lessons of life to reach the Beautiful Nurse someday. However, he
                    knew that in order to do so, he would need to truly exhibit the highest
                    possible quality, which he called
                    <b>H-Love</b>, a feeling of unconditional love and
                    connection with all beings. Only if he truly manifested unconditionality in his
                    thoughts, emotions, and actions, could the Little Prince connect with any
                    human being on the planet, in order to help them see the magnificence and
                    beauty that truly lives within them.
                  </p>
                  <p class="div3-p">Finally, as he took an oath that this would become the work of his life, the
                    Little Prince transformed into the character known as
                    <b>Mr. H</b>, and began his

                    work to be unconditionally available to all people that may be stuck,
                    helpless, or lost, in order to help them find their true selves, live their true
                    paths, and create their true destinies.
                  </p>
                  <!-- <h4>Hollistic<sup style="font-size: 11px;">&reg;</sup>is a registered trademark in multiple countries and classes.</h4> -->
                  <br><br>
                </div>
              </div>
            </div>
          </section>
          <!-- <section class="ls section_padding_top_100 section_padding_bottom_50 coaching-content"><div class="container coaching-section"><div class="row"><div class="col-sm-12"><h2 class="section_header">Coaching Methodologies</h2></div></div><div class="row coaching-methodologie-section"><div class="col-sm-4"><div class="teaser text-left"><div class="teaser_icon main_bg_color size_normal round"><i class="fa fa-comments-o"></i></div><h4 class="text-uppercase coaching-methodologies"> Personal Coaching</h4><p style="color: #464545;" class="text-justify">It’s easier now than ever before to seek guidance in any area of life. Using carefully selected elements from our core five-module program, a professional coach would work with you to help transform the way you think about yourself and your life.</p></div></div><div class="col-sm-4"><div class="teaser text-left"><div class="teaser_icon main_bg_color size_normal round"><i class="fa fa-star-o"></i></div><h4 class="text-uppercase coaching-methodologies"> Organisational Coaching </h4><p style="color: #464545;" class="text-justify">A coaching session isn’t always a solitary exercise. With group coaching, we take everyone for a ride. We use our core five-module program and a customised plan, to help address all of the group’s needs.</p></div></div><div class="col-sm-4"><div class="teaser text-left"><div class="teaser_icon main_bg_color size_normal round"><i class="fa fa-thumbs-o-up"></i></div><h4 class="text-uppercase coaching-methodologies"> Combinational Methods </h4><p style="color: #464545;" class="text-justify">Would you like to learn the magic behind the craft of coaching? Are you someone who gets energised just by the thought of helping take your own life and the lives of others to the next level? If this sounds like you, the Certified Hollistic here to help you. </p></div></div></div></div></section> -->
                                <!-- <section id="blog" class="ls section_padding_top_90 work-history-section"><div class="container"><div class="row flex-wrap v-center"><div class="col-sm-8 to_animate" data-animation="fadeInRight"><h2 class="section_header">Work History & Future Plans</h2></div><div class="col-sm-4 text-right hidden-xs to_animate" data-animation="fadeInRight"><a href="#" class="view-more">View More</a></div><div class="col-sm-12 topmargin_30"><div class="loop-colors"><article class="post format-small-image to_animate" data-animation="fadeInRight"><div class="side-item side-md content-padding big-padding with_border bottom_color_border left"><div class="row"><div class="col-md-5 col-lg-4"><div class="item-media entry-thumbnail"><img src="images/gallery/07.jpg" alt=""></div></div><div class="col-md-7 col-lg-8"><div class="item-content"><header class="entry-header"><h3 class="entry-title small"><a href="#" rel="bookmark">Lorem ipsum dolor sit amet consectetur adipisicing</a></h3><div class="entry-meta inline-content greylinks"><span><i class="fa fa-calendar highlight rightpadding_5" aria-hidden="true"></i><a href="#"><time datetime="2017-10-03T08:50:40+00:00">
                                17 jan, 2018</time></a></span><span><i class="fa fa-user highlight rightpadding_5" aria-hidden="true"></i><a href="#">Admin</a></span><span class="categories-links"><i class="fa fa-tags highlight rightpadding_5" aria-hidden="true"></i><a href="#">Community</a></span></div></header><div class="entry-content md-content-3lines-ellipsis"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat.</p></div></div></div></div></div></article><article class="post format-small-image topmargin_30 to_animate" data-animation="fadeInLeft"><div class="side-item side-md content-padding big-padding with_border bottom_color_border right"><div class="row"><div class="col-md-5 col-lg-4"><div class="item-media entry-thumbnail"><img src="images/gallery/08.jpg" alt=""></div></div><div class="col-md-7 col-lg-8"><div class="item-content"><header class="entry-header"><h3 class="entry-title small"><a href="#" rel="bookmark">Lorem ipsum dolor sit amet consectetur adipisicing.</a></h3><div class="entry-meta inline-content greylinks"><span><i class="fa fa-calendar highlight rightpadding_5" aria-hidden="true"></i><a href="#"><time datetime="2017-10-03T08:50:40+00:00">
                                17 jan, 2018</time></a></span><span><i class="fa fa-user highlight rightpadding_5" aria-hidden="true"></i><a href="#">Admin</a></span><span class="categories-links"><i class="fa fa-tags highlight rightpadding_5" aria-hidden="true"></i><a href="#">Services</a></span></div></header><div class="entry-content md-content-3lines-ellipsis"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat.</p></div></div></div></div></div></article><article class="post format-small-image topmargin_30 to_animate" data-animation="fadeInRight"><div class="side-item side-md content-padding big-padding with_border bottom_color_border left"><div class="row"><div class="col-md-5 col-lg-4"><div class="item-media entry-thumbnail"><img src="images/gallery/09.jpg" alt=""></div></div><div class="col-md-7 col-lg-8"><div class="item-content"><header class="entry-header"><h3 class="entry-title small"><a href="#" rel="bookmark">Lorem ipsum dolor sit amet consectetur adipisicing.</a></h3><div class="entry-meta inline-content greylinks"><span><i class="fa fa-calendar highlight rightpadding_5" aria-hidden="true"></i><a href="#"><time datetime="2017-10-03T08:50:40+00:00">
                          17 jan, 2018</time></a></span><span><i class="fa fa-user highlight rightpadding_5" aria-hidden="true"></i><a href="#">Admin</a></span><span class="categories-links"><i class="fa fa-tags highlight rightpadding_5" aria-hidden="true"></i><a href="#">Transgender</a></span></div></header><div class="entry-content md-content-3lines-ellipsis"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.</p></div></div></div></div></div></article></div></div></div></div></section> -->
                        <!--section id="about" class="ls columns_padding_30 completetion-year-section"><div class="container completetion-year"><div class="row"><div class="col-md-6 to_animate" data-animation="fadeInUp" data-delay="600"><div class=""><a href="#" class="text-center"><img src="images/Graph.png" alt=""></a></div></div><div class="col-md-6 to_animate" data-animation="fadeInRight" data-delay="300"><h2 class="section_header color4" style="margin-top: 15px;"> Completing Nine Years! </h2><p class="hollistic-content" style="color: #464545;">Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor. Mauris fermentum dictum magna. Sed laoreet aliquam leo.</p><p class="hollistic-content" style="color: #464545;">Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor. Mauris fermentum dictum magna.</p><p class="hollistic-content" style="color: #464545;">Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor. Mauris fermentum dictum magna.</p></div></div></div></section-->
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">Log In</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form>
                                  <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label form-label-manual">Email</label>
                                    <div class="col-sm-10">
                                      <input type="text"  class="form-control" id="inputemail" placeholder="Email Id">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label form-label-manual">Password</label>
                                    <div class="col-sm-10">
                                      <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <a href="#" class="theme_button color2 margin_0 confirm-identity">Confirm Identity</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalScrollableTitle">About Gorhav Kanal</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <p class="modal-p">Gorhav graduated from Cornell University’s College of Engineering (Ithaca,
                                  NY, USA) with a B.S. in Computer Science and a specialisation in Artificial
                                  Intelligence, in 2002. He continued being a student even after graduating,
                                  taking continuing/executive education courses from a number of universities
                                in the US and Europe.</p>
                                <p class="modal-p">After over 5 years of exploratory personal development work, to jumpstart
                                  himself into the broader journey of learning about human development and
                                  the human mind, Gorhav designed and completed his own Master’s
                                  programme equivalent in Integrated Multi-Disciplinary Studies, with a
                                  specialisation in Mind Coaching, from 2016 to 2018. The programme
                                  comprised of over a hundred modules from the fields of philosophy,
                                  psychology, spirituality, body-mind therapies, neuro-linguistic programming
                                  (NLP), individual and organisational coaching, peace and conflict resolution
                                  through mediation and facilitation, amongst other mind and life-oriented
                                  trainings. The programme was completed through a combination of online and in-person courses across the Asia-Pacific region, with Singapore serving
                                as the base.</p>
                              </div>
                              <!-- <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary">Save changes</button></div> -->
                            </div>
                          </div>
                        </div>

                        @endsection

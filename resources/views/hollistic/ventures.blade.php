@extends('layouts.app')

@section('page', 'Ventures')

@section('content')

<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>Ventures</h2>
                    <ol class="breadcrumb greylinks">
                        <li> <a href="{{url('/')}}">
                            Home
                        </a> </li>

                        <li class="active">
                            <a href="{{url('ventures')}}">Ventures</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="vsecb1" style="padding-bottom: 30px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                <div class="page-section venture-program-section  ">
                    <h3 class="sub-heading text-center" style="color: #000;"> <b>Humanity-Oriented Ventures</b></h3>
                     <p class="sub-heading-para venture-p" style="display: inline-block; color: #000; font-size: 15px">Hollistic strives to help better the world we live in by focusing on Humanity-Oriented Ventures in the Life Improvement space that can make a difference
to society. Our projects are concentrated within the broader areas of Human
Wellness, Life Technology, and Social Capital. We work through the stages of
project initiation, ideation, planning, prototyping, and testing, until we find
the most suitable team to bring the project to market, working closely with
them through the process.
Entrepreneurs and investors interested in these areas may reach us to
discuss synergistic partnerships. Following are the ventures we are presently
working on:</p>
                </div>
                </div>
            </div>
        </div>
    </section>

    {{-- /*<section class="vsecb1" style="background: #c2dcbd6e !important;">*/ --}}
    <section class="vsecb1" style="background: #dfecdc6e !important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                <div class="page-section venture-program-section ">
                    <h3 class="sub-heading" style="color: #000;font-size: 21px;"> <b>Human Wellness Ventures</b></h3>
                     <p class="sub-heading-para venture-p" style="display: inline-block;color: #000; font-size: 15px">Would you like to make healthier and
more wholeful choices in the real world, but are overwhelmed by the
number of options out there, all competing for your time and money?
Our Human Wellness Ventures makes this easy for you by bringing
together the best of the best in whole body-mind-soul care, carefully
chosen, packaged, and presented together, from our globally sourced
selection of products and services. Our present Human Wellness
Projects are as follows:</p>
                </div>
                </div>
              <div class="">
                <div class="col-md-4">

                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                           <img src="images/icons/vegetables.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">Wellness Foods</h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">Have all of the world&#39;s best organically grown
superfoods, carefully selected in their right quantities, along with
information describing their benefits and delectable recipes, all
delivered right to your doorstep. Make it both easy and fun to
live a healthy and wholeful lifestyle.</p>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                            <img src="images/icons/nursing.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">Wellness Care</h6>
                        <br>
                        <p class="venture-p" style="color: #000; font-size: 15px">How would you like to have access to a diverse
range of health regimes, routines, exercises, remedies,
treatments, and therapies, all integrated together and offered
under a single roof? Let our team of health experts help you
rehabilitate and rejuvenate, bringing your wellness to completely
new levels.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                             <img src="images/icons/lotus-position.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">Wellness Spaces</h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">Wouldn&#39;t you like a pure, peaceful window of
calm, right in the midst of a busy or even chaotic day? Well, now
you can have just that, with our private, personalised
sanctuaries, sprouting up in the next bustling neighbourhood
near you.</p>
                    </div>
                </div>
               </div>

               <div class="col-md-12 text-left">
                   <!--  <h3 class="sub-heading" style="font-size: 22px;"> <b>Life Technology Ventures</b></h3> -->
                    <br> <p class="sub-heading-para venture-p" style="display: inline-block; color: #000; font-size: 15px; padding-bottom:60px;">Our experience and advisory service offerings in Consumer Retailing
Services expand beyond Human Wellness to broader areas within the
industry. Please <a href="/contact" style="color: #e85242">contact us</a> to discuss any related projects.
</p>
          </div>
            </div>
        </div>
    </section>
     {{-- <section class="vsecb" style="background: #6aa4ff3d !important;"> --}}
     <section class="vsecb" style="background: #f7efee !important;">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-left">
                <div class="page-section venture-program-section ">
                    <h3 class="sub-heading" style="color: #000;font-size: 21px;"> <b>Life Technology Ventures</b></h3>
                     <p class="sub-heading-para venture-p" style="display: inline-block;color: #000; font-size: 15px">Through our Life Technology Ventures,
we aim to create a way for you to track all major aspects of your life
on a single platform, so that you can work on taking your life to totally
different levels. Our present Life Technology Projects are as follows:</p>
                </div>
                </div>
              <div class="">
                <div class="col-md-4">

                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                            <img src="images/icons/friendship.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px"> Life Management System (LMS)</h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">Our <b>Life Management System (LMS)</b> is an automated system
that helps you manage all aspects of your health and lifestyle on a
single platform, helping you achieve harmony between your body,
mind, and heart. By putting a focus on balance and multi-
dimensional growth, the system helps you attain your personal best
in all areas of your life.</p>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                            <img src="images/icons/lifenetwork.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">Life Networks System (LNS)</h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">With our <b>Life Networks System (LNS)</b>, you will be able to easily
navigate, manage, and grow your communication and relationship
networks, in both your personal and professional life, thus enabling
you to be as well-connected as you would like, while taking your
relationships to higher levels.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                            <img src="images/icons/life-rating.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">Life Ratings System (LRS) </h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">With our <b>Life Ratings System (LRS)</b>, you will be able to go about
your day knowing how good your experiences are expected to be or
have been with other people, organisations, places, and so on. In
short, a system that provides you with qualitative information about
all aspects of your environment, enabling you to make wiser day-
to-day choices with greater ease.</p>
                    </div>
                </div>
               </div>
               <div class="col-md-12 text-left">
                   <!--  <h3 class="sub-heading" style="font-size: 22px;"> <b>Life Technology Ventures</b></h3> -->
                    <br> <p class="sub-heading-para venture-p" style="display: inline-block; color: #000; font-size: 15px; padding-bottom:60px;">Our experience and advisory service offerings in Technology Product
Development expand beyond Life Technology to broader areas within the
industry. Please <a href="/contact" style="color: #e85242; font-size: 15px;">contact us</a> to discuss any related projects.</p>
          </div>
            </div>
        </div>
    </section>

     {{-- <section class="vsecb1" style="background-color: #cbadeb75;"> --}}
     <section class="vsecb1" style="background-color: #dfecdc6e;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                <div class="page-section venture-program-section ">
                    <h3 class="sub-heading" style="color: #000;font-size: 21px;"> <b>Social Capital Ventures
</b></h3>
                     <p class="sub-heading-para venture-p" style="display: inline-block;color: #000; font-size: 15px;">Few things fulfil us as much as helping
someone else in need. Our Social Capital Ventures aim to do just that
– to lend a hand to those who need it, using innovative methods, and
in the process, help improve your life and theirs. Our present Social
Capital Projects are as follows:</p>
                </div>
                </div>
              <div class="">
                <div class="col-md-4">

                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                           <img src="images/icons/hconnect.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">  H-CONNECT</h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">Through our H-Connect service, your contribution is
used to support individual caregivers, including providers of
healthcare-related services, assisting both people and animals that
are homeless and are living on the street, primarily in developing
regions of the world. Further, connections are made by these
caregivers between the homeless and the nearest and most suitable
NGOs (non-profits) for them, in order to have them consider joining
these organisations. Reporting of such caregiving and connection
activities is provided, making the impact on these lives visibly felt
by the contributor.</p>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                             <img src="images/icons/family.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">  H-SHELTER</h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">Our H-Shelter service enables you to contribute in-kind
product bundles to human and animal NGOs (non-profits) in
developing regions of the world, that specifically have a need for
those very products. Additionally, make a human connection by
communicating with the beneficiaries of your contributions, and
watch as your gift impacts the lives of others.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="teaser text-left">
                        <div class="teaser_icon size_normal round">
                            <!-- <i class="fa fa-comments-o"></i>  -->
                           <img src="images/icons/truck.png" alt="icons">
                        </div>
                        <h6 class="text-uppercase coaching-methodologies" style="color: #000; font-size: 15px">  H-TRUCK</h6><br>
                        <p class="venture-p" style="color: #000; font-size: 15px">Through our physical H-Truck delivery service, your
contribution is converted to in-kind product gift bags, consisting of
light housing and sheltering material, bedding, toiletries, food
packets, medical supplies, and other bare necessities for
underprivileged homeless people as well as stray animals, that are
not covered by NGOs (non-profits), and are distributed in
developing regions of the world. A little help from you can have a

life-changing impact on another. Here, we encourage you to include
with your contribution, a small message of hope.</p>
                    </div>
                </div>
               </div>
                <div class="col-md-12 text-left" style="margin-top: 30px;">
                    <p style="color: #000;font-weight:bold;"> <b> Image Gallery:
</b></p>
                    <div style="display: flex; flex-wrap: wrap;">
                        <?php for ($i=1; $i < 13; $i++) : ?>
                        <a href="images/ventures/<?= $i ?>.jpg" data-lightbox="venture-images" data-title="Image Gallery" style="margin-right: 7px;margin-top: 7px;">
                            <img src="images/ventures/<?= $i ?>.jpg" style="width: 90px;height: 70px;border: 2px #fdb81394 solid;">
                        </a>
                    <?php endfor; ?>
                    </div>
               </div>
               <div class="col-md-12 text-left">
                   <!--  <h3 class="sub-heading" style="font-size: 22px;"> <b>Life Technology Ventures</b></h3> -->
                    <br> <p class="sub-heading-para venture-p" style="display: inline-block; color: #000; font-size: 15px;">Our experience and advisory service offerings in Investment Capital Management expand beyond Social Capital to broader areas within the industry. Please <a href="/contact" style="color: #e85242"> contact us</a> to discuss any related projects.
</p>

<br> <p class="sub-heading-para" style="display: inline-block; color: #000; font-size: 15px;padding-bottom:60px;">
</p>
          </div>

            </div>
        </div>
    </section>


<section style="background-color: #f1f0f0;padding-top: 25px;padding-bottom: 25px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>To further discuss any of our ventures, including Beta testing and
sample/demo use, reach us through our <a href="{{url('/contact')}}" style="color: #e85242;font-weight: bold;">Contact page</a>.</p>
                </div>
            </div>
        </div>
    </section>



		<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document" style=" width: 100%;height: 100%;margin-top: 0px;">
				<div class="modal-content" style="height: auto;min-height: 100%;border-radius: 0;">
					<div class="modal-header" style="border-bottom: unset;padding-top: 10px;padding-bottom: 0px;">
					    <!-- <h5 class="modal-title" id="exampleModalLabel">Hollistic</h5> -->
					    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					        <span aria-hidden="true">&times;</span>
					    </button>
				    </div>
				    <div class="modal-body" style="padding-top: 0px !important;">
				    	<div class="container-fluid">
				    		<div class="row">
				    			<div class="col-md-12 text-center" style="margin-bottom: 30px;">
				    				<h1 style="margin-bottom: 10px;">Get Your Winning Edge</h1>
				    				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    				tempor incididunt ut labore et dolore magna aliqua.</p>
				    			</div>
				    		</div>
				    	<form class="row">
							<div class="form-group col-md-6">
							    <!-- <label for="formGroupExampleInput">Example label</label> -->
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="First Name" required>
							</div>

						    <div class="form-group col-md-6">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Last Name" required>
						    </div>

						    <div class="form-group col-md-6">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Phone Number" required>
							</div>

						    <div class="form-group col-md-6">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Email" required>
						    </div>

						    <div class="form-group col-md-12">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Country" required>
							</div>

						    <div class="form-group col-md-12">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Occupation" required>
						    </div>

						    <div class="col-md-12 text-center">
								<a href="#" class="theme_button color2 margin_0" style="width: 235px;margin-top: 20px !important">Submit</a>
						    </div>

						    <div class="col-md-12 text-center">
						    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						    	consequat.</p>
						    </div>
						</form>
						</div>
				    </div>
				   <!--  <div class="modal-footer" style="border-top: unset;">
					    <button type="button" class="btn btn-secondary modal-close-btn" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary">Save changes</button>
				    </div> -->
				</div>
			</div>
		</div>

@endsection

@extends('layouts.app')

@section('page', 'Program')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>Products</h2>
				<ol class="breadcrumb greylinks">
					<li>
						<a href="{{url('/')}}">
							Home
						</a>
					</li>
					<!-- <li><a href="#">Pages</a></li> -->
					<li class="active">
						<a href="{{url('/products')}}"> Products </a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<br><br>
	<h1 style="text-align: center;"> No data available </h1>
<br><br>

@endsection

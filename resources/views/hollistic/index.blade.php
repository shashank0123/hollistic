@extends('layouts.app')

@section('page', 'Home')

@section('content')

<section class="intro_section page_mainslider ds all-scr-cover bottom-overlap-teasers" >
	<div class="flexslider" data-dots="true" data-nav="true">
		<ul class="slides">
			<li>
				<div class="slide-image-wrap image-container" style="
							background-position: top;">
					<img src="images/banner/banner1.jpg" alt="" />
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<div class="slide_description_wrapper">
								<div class="slide_description">
									<div class="intro-layer slider-subtitle" data-animation="fadeInRight">Hollistic:</div>
									<div class="intro-layer" data-animation="fadeInLeft">
										<h2>
											<span class="highlight3">A Global Life </span>
											<br> Improvement Ecosystem
											</h2>
										</div>
										<div class="intro-layer" data-animation="fadeInRight">
											<p class="thin">
												<em></em>
											</p>
										</div>
										<div class="intro-layer" data-animation="fadeInUp">
											<div class="slide_buttons">
												<a href="{{url('about')}}" class="theme_button color1 min_width_button">Learn More</a>
											</div>
										</div>
									</div>
									<!-- eof .slide_description -->
								</div>
								<!-- eof .slide_description_wrapper -->
							</div>
							<!-- eof .col-* -->
						</div>
						<!-- eof .row -->
					</div>
					<!-- eof .container -->
				</li>
				<li>
					<div class="slide-image-wrap image-container">
						<img src="images/banner/banner2.jpg" alt="" />
						<div class="after"></div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-sm-12 text-center">
								<div class="slide_description_wrapper">
									<div class="slide_description">
										<div class="intro-layer slider-subtitle" data-animation="fadeInRight"> Multi-Dimensional Coaching:</div>
										<div class="intro-layer" data-animation="fadeInLeft">
											<h2>
												<span class="highlight3"> Change Your Self...</span>
												<br>  Change Your Life!
												</h2>
											</div>
											<div class="intro-layer" data-animation="fadeInRight">
												<p class="thin">
													<em></em>
												</p>
											</div>
											<div class="intro-layer" data-animation="fadeInUp">
												<div class="slide_buttons">
													<a href="{{url('/programs')}}" class="theme_button color1 min_width_button">Learn More</a>
												</div>
											</div>
										</div>
										<!-- eof .slide_description -->
									</div>
									<!-- eof .slide_description_wrapper -->
								</div>
								<!-- eof .col-* -->
							</div>
							<!-- eof .row -->
						</div>
						<!-- eof .container -->
					</li>
					<li>
						<div class="slide-image-wrap image-container">
							<img src="images/banner/banner12.jpg" alt="" />
							<div class="after"></div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-sm-12 text-center">
									<div class="slide_description_wrapper">
										<div class="slide_description">
											<div class="intro-layer slider-subtitle" data-animation="fadeInRight">Humanity-Oriented Ventures:</div>
											<div class="intro-layer" data-animation="fadeInLeft">
												<h2>
													<span class="highlight3">Human Wellness, </span>
													<br> Life Technology, Social Capital
													</h2>
												</div>
												<div class="intro-layer" data-animation="fadeInRight">
													<p class="thin">
														<em></em>
													</p>
												</div>
												<div class="intro-layer" data-animation="fadeInUp">
													<div class="slide_buttons">
														<a href="{{url('/ventures')}}" class="theme_button color1 min_width_button">Learn More</a>
													</div>
												</div>
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>
						<li>
							<div class="slide-image-wrap image-container">
								<img src="images/banner/banner4.jpg" alt="" />
								<div class="after"></div>
							</div>
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer slider-subtitle" data-animation="fadeInRight">Conscious Capital:</div>
												<div class="intro-layer" data-animation="fadeInLeft">
													<h2>
														<span class="highlight3"> Affecting Lives Through</span>
														<br>  Positive Impact
														</h2>
													</div>
													<div class="intro-layer" data-animation="fadeInRight">
														<p class="thin">
															<em></em>
														</p>
													</div>
													<div class="intro-layer" data-animation="fadeInUp">
														<div class="slide_buttons">
															<a href="{{url('capital')}}" class="theme_button color1 min_width_button">Learn More</a>
														</div>
													</div>
												</div>
												<!-- eof .slide_description -->
											</div>
											<!-- eof .slide_description_wrapper -->
										</div>
										<!-- eof .col-* -->
									</div>
									<!-- eof .row -->
								</div>
								<!-- eof .container -->
							</li>
						</ul>
					</div>
					<!-- eof flexslider -->
				</section>
				<section id="services" class="ls section_intro_overlap columns_padding_0 columns_margin_0 container_padding_0">
					<div class="container-fluid">

					</div>
				</section>
				<section id="about" class="ls section_padding_top_110 section_padding_bottom_65 columns_padding_30">
					<div class="container">
						<div class="row">
							<div class="col-md-12" data-animation="fadeInUp" data-delay="600">
								<!-- <div class="embed-responsive embed-responsive-3by2"><a href="https://www.youtube.com/embed/xKxrkht7CpY" class="embed-placeholder"><img src="images/updated/about hollistic/1.jpg" alt=""></div> -->
								<h2 class="section_header color4" style="margin-top: 23px;"> Welcome to Hollistic! </h2>
								<!-- <p class="section-excerpt grey">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore.</p> -->
								<p class="hollistic-content" >You are unique! And yet, it isn’t always easy to realise how special you are
in the midst of the billions of people that inhabit this planet. But that is
exactly what you are, in your very distinct way. And Hollistic is here to help
you express that very uniqueness in full shine, while at the same time,
regain focus on and understand what you truly want in life and ensure you
have the strategies and support you need to make progress towards those
goals.
</p>
								<p class="hollistic-content" >Hollistic offers an integrated way of understanding people’s minds, with one
primary goal: the improvement of life. Through the exploration of oneself,
Hollistic strives to help people achieve growth in multiple dimensions, while
attaining a state of balance in all major areas of life.</p>
								<!-- <p class="hollistic-content" style=" font-weight: 700;">Hollistic® is a registered trademark in multiple countries and classes.</p><br> -->
							</div>
						</div>
					</div>
				</section>

				<div class="row flex-wrap v-center-content">
					<div class="col-lg-4 col-sm-4 col-xs-12 to_animate hprograms" data-animation="fadeInUp">
						<div class="teaser main_bg_color transp with_padding big-padding margin_0">
							<div class="media xxs-media-left">
								<div class="media-center media-middle hpicon">
									<a href="{{url('services')}}" class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
										<i class="fa fa-users highlight" aria-hidden="true"></i>
									</a>
								</div>
								<div class="media-body media-middle text-center">
									<h4>
										<a href="{{url('services')}}" class="home-programs">Services</a>
									</h4>
									<br>
										<p>Client Programs</p>
										<p>Partner Programs</p>
										<p>
											<br/>
										</p>
										<!-- <p>Content Publications</p> -->
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4 col-xs-12 to_animate hprograms" data-animation="fadeInUp">
							<div class="teaser main_bg_color2 transp with_padding big-padding margin_0">
								<div class="media xxs-media-left">
									<div class="media-center media-middle hpicon">
										<a href="{{url('ventures')}}" class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
											<i class="fa fa-rocket highlight2" aria-hidden="true"></i>
										</a>
									</div>
									<div class="media-body media-middle text-center">
										<h4>
											<a href="{{url('ventures')}}" class="home-programs">Ventures</a>
										</h4>
										<br>
											<p>Human Wellness</p>
											<p>Life Technology</p>
											<p>Social Capital</p>
											<!-- <p>Referral Sharing</p> -->
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-4 col-xs-12 to_animate hprograms" data-animation="fadeInUp">
								<div class="teaser main_bg_color3 transp with_padding big-padding margin_0">
									<div class="media xxs-media-left">
										<div class="media-center media-middle hpicon">
											<a href="{{url('capital')}}" class="teaser_icon size_small round light_bg_color thick_border_icon big_wrapper">
												<i class="fa fa-money highlight3" aria-hidden="true" ></i>
											</a>
										</div>
										<div class="media-body media-middle text-center">
											<h4>
												<a href="{{url('capital')}}" class="home-programs">Capital</a>
											</h4>
											<br>
												<!-- 	<p>Project</p><p>Team</p><p>Regions</p><p>Sectors</p><p>Company Type</p><p>Invest</p> -->
												<p>Conscious Investing</p>
												<p>
													<br/>
												</p>
												<p>
													<br/>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<section class="testimonial-section2" style="background-image: url(&quot;images/1920x900_bg.jpg&quot;); ">
								<div class="row text-center">
									<div class="col-12">
										<!-- <div class="h2">Testimonial</div> -->
									</div>
								</div>
								<div id="testim" class="testim">
									<!--         <div class="testim-cover"> -->
									<div class="wrap">
										<span id="right-arrow" class="arrow right fa fa-chevron-right"></span>
										<span id="left-arrow" class="arrow left fa fa-chevron-left "></span>
										<ul id="testim-dots" class="dots">
											<li class="dot active"></li>
											<li class="dot"></li>
											<li class="dot"></li>
											<li class="dot"></li>
										</ul>
										<div id="testim-content" class="cont">
											<div class="active">
												<div class="h4" style="color: #000;font-weight: bold;">Hollistic’s Vision</div>
												<p>
													<i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i> To help people understand, witness, and grow towards the magnificence and beauty that they truly are.

													<i class="fa fa-quote-right" style="font-size: 35px;color: #99152c;" aria-hidden="true"></i>
												</p>
											</div>
											<div>
												<div class="h4" style="color: #000;font-weight: bold;">Hollisticity</div>
												<p>
													<i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>  A state of being where all positive qualities are combined together multi-dimensionally into a single integrated state.

													<i class="fa fa-quote-right" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>
												</p>
											</div>
											<div>
												<div class="h4" style="color: #000;font-weight: bold;">H-Love</div>
												<p>
													<i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i> The unconditional availability to all beings, with a vision of serving as the
													<br>“World’s Backup”

														<i class="fa fa-quote-right" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>
													</p>
												</div>
												<div>
													<div class="h4" style="color: #000;font-weight: bold;">HCLE</div>
													<p>
														<i class="fa fa-quote-left" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i> Hollistic Core Life Education Program with five themes: Self, Perspective, People, Communication, World.


														<i class="fa fa-quote-right" aria-hidden="true" style="font-size: 35px;color: #99152c;"></i>
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--         </div> -->
								</section>
								<section class="cs1 section_padding_0 columns_padding_0 columns_margin_0" style="">
									<div class="container-fluid">
										<div class="row">
											<div class="col-md-12 text-center ">
												<h3 class="sub-heading " style="padding-top: 50px; color: #fff; padding-bottom: 20px;">
													<b>Coaching Metrics</b>
												</h3>
											</div>
											<div class="col-md-2 col-sm-6 after_cover color_bg_3">
												<div class="teaser counter-background-teaser text-center">
													<span class="counter counter-background counted" data-from="0" data-to="21" data-speed="2100"></span>
													<h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="10" data-speed="2100">10+</h3>
													<p class="small-text custom-small-text">Years in Coaching {{-- (Since 2011) --}}</p>
												</div>
											</div>
											<div class="col-md-2 col-sm-6 after_cover color_bg_3">
												<div class="teaser counter-background-teaser text-center">
													<span class="counter counter-background counted" data-from="0" data-to="743" data-speed="2100"></span>
													<h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="58" data-speed="2100">10x</h3>
													<p class="small-text custom-small-text">Coaching Methodologies</p>
												</div>
											</div>
											<div class="col-md-2 col-sm-6 after_cover color_bg_3">
												<div class="teaser counter-background-teaser text-center">
													<span class="counter counter-background counted" data-from="0" data-to="99" data-speed="2100"></span>
													<h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="36" data-speed="2100">	100+
</h3>
													<p class="small-text custom-small-text">Coaching Tools</p>
												</div>
											</div>
											<div class="col-md-3 col-sm-6 after_cover color_bg_3">
												<div class="teaser counter-background-teaser text-center">
													<span class="counter counter-background counted" data-from="0" data-to="15" data-speed="2100"></span>
													<h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="100" data-speed="2100">100+</h3>
													<p class="custom-small-text">Coaching Media Developed</p>
												</div>
											</div>
											<div class="col-md-3 col-sm-6 after_cover color_bg_3">
												<div class="teaser counter-background-teaser text-center">
													<span class="counter counter-background counted" data-from="0" data-to="15" data-speed="2100"></span>
													<h3 class="counter highlight counted custom-counter-content" data-from="0" data-to="100" data-speed="2100">10,000+</h3>
													<p class="custom-small-text">Coaching Concepts Elaborated</p>
												</div>
											</div>
										</div>
									</div>
								</section>

								<div class="modal fade" id="ModalScrollable" tabindex="-1" role="dialog" aria-labelledby="ModalScrollableTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-scrollable" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title sub-heading" id="ModalScrollableTitle">About Hollistic</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<p style="color: #464545">Hollistic has embraced all the formal learnings of its founder, Gorhav Kanal, as well as his own invented ideas, to offer an integrated way of understanding people’s minds with only one goal: life improvement and growth towards “hollisticity”, a state of being, where all positive qualities are combined together multi-dimensionally into a single integrated state.
</p>
												<p style="color: #464545;">Hollistic’s approach to Life Improvement is threefold: through our projects and ventures in Multi-Dimensional Coaching, Life Technology, and Social Capital. With the help of “H-Love”, i.e. our unconditional availability for others, with an ideal of serving as the “World’s Backup”, we work through a combination of consulting, coaching, and investing in Life Improvement ideas. Our highest common goal in our work is to enable ourselves as well as others to see, think, feel and live with our hearts at the center of all that we do.
</p>
												<p style="color: #464545;">
													<b> “Hollistic” is a registered trademark in the US (pending), India (pending), and Singapore (completed).
</b>
												</p>
											</div>
											<!-- <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary">Save changes</button></div> -->
										</div>
									</div>
								</div>
								<div class="modal fade" id="contentModal" tabindex="-1" role="dialog" aria-labelledby="contentModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Staytune</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<p style="color: #464545;">We are adding content yet.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary modal-close-btn" data-dismiss="modal">Close</button>
												<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
											</div>
										</div>
									</div>
								</div>

								<script>

// vars
'use strict'
var	testim = document.getElementById("testim"),
		testimDots = Array.prototype.slice.call(document.getElementById("testim-dots").children),
    testimContent = Array.prototype.slice.call(document.getElementById("testim-content").children),
    testimLeftArrow = document.getElementById("left-arrow"),
    testimRightArrow = document.getElementById("right-arrow"),
    testimSpeed = 7500,
    currentSlide = 0,
    currentActive = 0,
    testimTimer,
		touchStartPos,
		touchEndPos,
		touchPosDiff,
		ignoreTouch = 30;
;

window.onload = function() {

    // Testim Script
    function playSlide(slide) {
        for (var k = 0; k < testimDots.length; k++) {
            testimContent[k].classList.remove("active");
            testimContent[k].classList.remove("inactive");
            testimDots[k].classList.remove("active");
        }

        if (slide < 0) {
            slide = currentSlide = testimContent.length-1;
        }

        if (slide > testimContent.length - 1) {
            slide = currentSlide = 0;
        }

        if (currentActive != currentSlide) {
            testimContent[currentActive].classList.add("inactive");
        }
        testimContent[slide].classList.add("active");
        testimDots[slide].classList.add("active");

        currentActive = currentSlide;

        clearTimeout(testimTimer);
        testimTimer = setTimeout(function() {
            playSlide(currentSlide += 1);
        }, testimSpeed)
    }

    testimLeftArrow.addEventListener("click", function() {
        playSlide(currentSlide -= 1);
    })

    testimRightArrow.addEventListener("click", function() {
        playSlide(currentSlide += 1);
    })

    for (var l = 0; l < testimDots.length; l++) {
        testimDots[l].addEventListener("click", function() {
            playSlide(currentSlide = testimDots.indexOf(this));
        })
    }

    playSlide(currentSlide);

    // keyboard shortcuts
    document.addEventListener("keyup", function(e) {
        switch (e.keyCode) {
            case 37:
                testimLeftArrow.click();
                break;

            case 39:
                testimRightArrow.click();
                break;

            case 39:
                testimRightArrow.click();
                break;

            default:
                break;
        }
    })

		testim.addEventListener("touchstart", function(e) {
				touchStartPos = e.changedTouches[0].clientX;
		})

		testim.addEventListener("touchend", function(e) {
				touchEndPos = e.changedTouches[0].clientX;

				touchPosDiff = touchStartPos - touchEndPos;

				console.log(touchPosDiff);
				console.log(touchStartPos);
				console.log(touchEndPos);


				if (touchPosDiff > 0 + ignoreTouch) {
						testimLeftArrow.click();
				} else if (touchPosDiff < 0 - ignoreTouch) {
						testimRightArrow.click();
				} else {
					return;
				}

		})
}



								</script>

@endsection

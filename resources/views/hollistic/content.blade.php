@extends('layouts.app')

@section('page', 'Content')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>Content</h2>
				<ol class="breadcrumb greylinks">
					<li>
						<a href="{{url('/')}}">
						Home
					</a>
					</li>
					<!-- <li><a href="#">Pages</a></li> -->
					<li class="active">
						<a href="{{url('content')}}"> Content </a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<div class="section_padding_top_65 section_padding_bottom_65 high-level-collection-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="sub-heading-para  text-center" style="color: #000; font-size: 15px;">Changing your life for the better has never been so easy. Browse through
hundreds of topics in the areas of personal and human development, self-growth, and spirituality, and make today the first day of the rest of your
transformed life!</p>
				<br>
					<br>
						<section>
							<div class="container">
								<hr style="height:2px;background-color: #9c9c9c;">
									<br>
										<center>
											<img src="images/twl.png" height="100%" width="300px">
											</center>
											<br>
												<br>
													<script src="https://www.powr.io/powr.js?platform=html"></script>
													<div class="powr-social-feed" id="7dc3302c_1556176841"></div>
													<br>
														<hr style="height:2px;background-color: #9c9c9c;">
															<br>
																<center>
																	<img src="images/facebook.png" height="100%" width="300px">
																	</center>
																	<br>
																		<br>
																			<script src="https://www.powr.io/powr.js?platform=html"></script>
																			<div class="powr-social-feed" id="a59e3e37_1556794089"></div>
																			<!--<script src="https://www.powr.io/powr.js?platform=html"></script><div class="powr-social-feed" id="44d064b3_1556191594"></div>-->
																			<br>
																				<hr style="height:2px;background-color: #9c9c9c;">
																					<br>
																						<center>
																							<img src="images/insta.png" height="100%" width="300px">
																							</center>
																							<br>
																								<br>
																									<script src="https://www.powr.io/powr.js?platform=html"></script>
																									<div class="powr-social-feed" id="09174790_1556176719"></div>
																									<br>
																										<hr style="height:2px;background-color: #9c9c9c;">
																											<br>
																												<center>
																													<img src="images/printrest.png" height="100%" width="300px">
																													</center>
																													<br>
																														<br>
																															<script src="https://www.powr.io/powr.js?platform=html"></script>
																															<div class="powr-social-feed" id="0415eb78_1555734932"></div>
																															<br>
																																<hr style="height:2px;background-color: #9c9c9c;">
																																	<br>
																																		<center>
																																			<img src="images/flk.png" height="100%" width="300px">
																																			</center>
																																			<br>
																																				<br>
																																					<script src="https://www.powr.io/powr.js?platform=html"></script>
																																					<div class="powr-social-feed" id="41a9a5a5_1556175859"></div>
																																					<br>
																																						<hr style="height:2px;background-color: #9c9c9c;">
																																							<br>
																																								<center>
																																									<img src="images/yt.png" height="100%" width="300px">
																																									</center>
																																									<br>
																																										<br>
																																											<script src="https://www.powr.io/powr.js?platform=html"></script>
																																											<div class="powr-social-feed" id="27cb7832_1556176620"></div>
																																											<br>
																																												<hr style="height:2px;background-color: #9c9c9c;">
																																													<br>
																																														<center>
																																															<img src="images/vimeo.png" height="100%" width="300px">
																																															</center>
																																															<br>
																																																<br>
																																																	<script src="https://www.powr.io/powr.js?platform=html"></script>
																																																	<div class="powr-social-feed" id="9ffdcc3a_1556174275"></div>
																																																</div>
																																															</section>
																																														</div>
																																													</div>
																																												</div>
																																											</div>
																																											<section class="ls page_portfolio section_padding_bottom_50 ">
																																												<div class="container">
																																													<hr style="height:2px;background-color: #9c9c9c;">
																																														<div class="row">
																																															<!--    <div class="col-md-12 kpx_login"><div class="text-center"><h2 class="section_header color4"> Private Gallery </h2></div><h3 class=""><a href="#"></a></h3><div class="row kpx_socialButtons" style="width: 540px;margin: 0 auto;"><div id="gallery-accessor"><div class="col-xs-7 col-sm-7" ><input style="height: 51px;
    margin-top: 7px;
    margin-left: 49px;" type="number" aria-required="true" size="30" value="" name="gpin" id="gpin" class="form-control" placeholder="Enter Gallery Pin"><p id="gpin-message" style="margin-left: 49px;font-size: 14px;color: red;display: none;">You entered a wrong pin.</p><p id="gpin-success" style="margin-left: 49px;font-size: 14px;color: green;display: none;">Gallery access granted.</p></div><div class="col-xs-5 col-sm-5"><div class="apply" style="margin-left: 31px;"><a href="#" id="view-more" class="theme_button color2 margin_0" style="color: #fff; float: right;">View More</a></div></div></div></div><br></div> -->
																																														</div>
																																													</div>
																																												</section>
																																												<section style="display: none;" class="ls page_portfolio section_padding_bottom_50 " id="private-gallery">
																																													<div class="container">
																																														<div class="row">
																																															<div class="col-md-4">
																																																<a href="images/about/ceo.jpg" data-lightbox="image-1" data-title="ImageGallery">
																																																	<img src="images/about/ceo.jpg" style="width: 100%;height: 350px;">
																																																	</a>
																																																</div>
																																																<div class="col-md-4">
																																																	<a href="images/about/ceo.jpg" data-lightbox="image-1" data-title="ImageGallery">
																																																		<img src="images/about/ceo.jpg" style="width: 100%;height: 350px;">
																																																		</a>
																																																	</div>
																																																	<div class="col-md-4">
																																																		<a href="images/about/ceo01.jpg" data-lightbox="image-1" data-title="ImageGallery">
																																																			<img src="images/about/ceo01.jpg" style="width: 100%;height: 350px;">
																																																			</a>
																																																		</div>
																																																		<div class="col-md-4">
																																																			<a href="images/about/ceo.jpg" data-lightbox="image-1" data-title="ImageGallery">
																																																				<img src="images/about/ceo.jpg" style="width: 100%;height: 350px;">
																																																				</a>
																																																			</div>
																																																			<div class="col-md-4">
																																																				<a href="images/about/ceo.jpg" data-lightbox="image-1" data-title="ImageGallery">
																																																					<img src="images/about/ceo.jpg" style="width: 100%;height: 350px;">
																																																					</a>
																																																				</div>
																																																				<div class="col-md-4">
																																																					<a href="images/about/ceo.jpg" data-lightbox="image-1" data-title="ImageGallery">
																																																						<img src="images/about/ceo.jpg" style="width: 100%;height: 350px;">
																																																						</a>
																																																					</div>
																																																				</div>
																																																			</div>
																																																		</section>
																																																	</div>
																																																	<script type="text/javascript">
	function createSlick(){

		$(".slick-slider").not('.slick-initialized').slick({
			autoplay: true,
			dots: true,
			responsive: [{
				breakpoint: 500,
				settings: {
					dots: false,
					arrows: false,
					infinite: false,
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}]
		});

	}

	createSlick();

//Now it will not throw error, even if called multiple times.
$(window).on( 'resize', createSlick );

</script>
@endsection

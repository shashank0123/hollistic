@extends('layouts.app')

@section('page', 'Help')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>HELP</h2>
				<ol class="breadcrumb greylinks">
					<li>
						<a href="{{url('/')}}">
							Home
						</a>
					</li>
					<!-- <li><a href="#">Pages</a></li> -->
					<li class="active">
						<a href="{{url('/faq')}}"> HELP </a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>

<section class="ls section_padding_top_30 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

  <!-- <h2>Frequently Asked Questions</h2> -->

  <div class="accordion">
    @if(!empty($faqs))
    @foreach($faqs as $faq)
    <div class="accordion-item">
      <a>{{$faq->questions}}</a>
      <div class="content">
        <p><?php echo $faq->answers;   ?></p>
      </div>
    </div>
    @endforeach
    @endif    
  </div>

</section>

@endsection

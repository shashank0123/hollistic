@extends('layouts.app')

@section('page', 'Program')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>Coaching</h2>
				<ol class="breadcrumb greylinks">
					<li>
						<a href="{{url('/')}}">
							Home
						</a>
					</li>
					<!-- <li><a href="#">Pages</a></li> -->
					<li class="active">
						<a href="{{url('/programs')}}"> Coaching </a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<!-- <section><div class="page-section" style="padding-top: 70px;padding-bottom: 50px;"><div class="container"><div class="row"><h2 class="section_header color4 text-center"> Themes</h2><div class="col-xs-12 bottommargin_0 to_animate animated fadeInUp" data-animation="fadeInUp"><div class="row masonry-feed columns_padding_2"><div class="col-xs-12 col-md-4"><div class="row"><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="images/430x420_img1.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Self</b></a></h3><p class="curriculum-content" style="color: #464545;">Most ideas of life improvement are unlikely to be complete if they are not based on improvement of the self, i.e. personal development. This is why it is imperative to pay attention to your body, mind, heart, and soul. Doing so takes care of your physical, mental, emotional, and spiritual health.</p></div></div></div><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="images/430x420_img2.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Perspective</b></a></h3><p class="curriculum-content" style="color: #464545;">How do you look at events and situations that happen to you? Do you take them in your stride or fear that they will change your life for the worse? Learning how to handle them better will mark a shift in your perspective about the things that may not be in your control, while enabling you to re-focus and better manage the things that are.
</p></div></div></div></div></div><div class="col-xs-12 col-md-4"><div class="row"><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="images/430x420_img3.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>People</b></a></h3><p class="curriculum-content" style="color: #464545;">We are happiest when we have happy and fulfilling relationships with the people in our lives. We need to know how to deal with people and navigate through both easy and difficult relationships in our lives better, in order to take our experiences with other people to higher levels.</p></div></div></div><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="images/430x420_img6.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Communication</b></a></h3><p class="curriculum-content" style="color: #464545;">The key to better relationships is improved communication within them. Learning to understand what to say to whom, when, and in which context, can be a key differentiator in the way the other person perceives and reacts to that information, and can help you enable greater control over the quality of your interactions.</p></div></div></div></div></div><div class="col-xs-12 col-md-4"><div class="row"><div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="images/430x420_img5.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>World</b></a></h3><p class="curriculum-content" style="color: #464545;">Understanding how the world works through signs, vibes, and other information from your surroundings can be very important in decision-making and moving through your day and life with greater ease and success.
</p></div></div></div>
<!-- <div class="col-xs-12 col-sm-6 col-md-12"><div class="vertical-item content-absolute text-center ds"><div class="item-media"><img src="images/430x420_img4.jpg" alt=""></div><div class="item-content darken_gradient"><h3 class="entry-title curriculum-entry-title"><a href="#"><b>Love &amp; Relationships</b></a></h3><p class="curriculum-content">Find Your Fit</p></div></div></div> ></div></div></div></div></div></div></div></section-->
<section id="events" class="ls" style="padding-bottom: 50px;padding-top: 50px;">
	<div class="container wide">
		<h2 class="section_header color4 text-center" style="font-size: 25px;">
			<b>Multi-Dimensional Coaching</b>
		</h2>
		<!-- <h2 class="section-title align-center">Our Programs</h2> -->
		<p class="sub-heading-para coaching-p" style="display: inline-block; color: #000; font-size: 15px">Hollistic offers Multi-Dimensional Coaching to our individual and
organisational clients, and works in partnership with other coaches and
professionals from related areas. Select from the options below to learn
more.</p>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="gallery-item" style="background-image: url(images/p5.jpg),linear-gradient(rgba(0, 0, 0, 0.32),rgba(0, 0, 0, 0));">
						<div class="program-text text-left">
							<h5 style="color: #fff; text-align: center; font-size: 20px">
								<b>
									<a href="{{url('/client-programs')}}">Client Programs</a>
								</b>
							</h5>
							<br>
								<p style="text-align: center; font-size: 15px">Private Coaching</p>
								<p style="text-align: center; font-size: 15px">Group, Organisational, and Thematic Coaching</p>
								<p style="text-align: center; font-size: 15px">Content Publications</p>
								<br>
									<center>
										<a href="{{url('/client-programs')}}" class="theme_button color2 margin_0">Read More</a>
									</center>
									<!-- <a href="#" class="btn btn-style-5">Read More</a> -->
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="gallery-item" style="background-image: url(images/p2.jpg),linear-gradient(rgba(0, 0, 0, 0),rgba(0, 0, 0, 0));">
								<div class="program-text text-left">
									<h5 style="color: #fff;text-align: center; font-size: 20px">
										<b>
											<a href="{{url('/partner-programs')}}">Partner Programs</a>
										</b>
									</h5>
									<br>
										<p style="text-align: center; font-size: 15px">Private Coaching Referrals</p>
										<p style="text-align: center; font-size: 15px">Group, Organisational, and Thematic Coaching Programs</p>
										<p style="text-align: center; font-size: 15px">Content Co-Creation and Publishing</p>
										<br>
											<!-- <li><p>Referral Sharing</p></li> -->
											<center>
												<a href="{{url('/partner-programs')}}" class="theme_button color2 margin_0">Read More</a>
											</center>
											<!-- <a href="#" class="btn btn-style-5">Read More</a> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- <section style="padding-bottom: 50px;"><div class="container"><div class="row text-center"><div class="col-sm-2"><img src="img/icons/brainstorm.png" alt=""><p>Self</p></div><div class="col-sm-3"><img src="img/icons/3d.png" alt=""><h3>Perception</h3></div><div class="col-sm-2"><img src="img/icons/networking.png" alt=""><h3>People</h3></div><div class="col-sm-3"><img src="img/icons/group.png" alt=""><h3>Communication</h3></div><div class="col-sm-2"><img src="img/icons/3d.png" alt=""><h3>World</h3></div></div></div></section> -->

@endsection

@extends('layouts.app')

@section('page', 'Schedule')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>Services</h2>
				<ol class="breadcrumb greylinks">
					<li> <a href="{{url('/')}}">
						Home
					</a> </li>
					<!-- <li> <a href="#">Program</a> </li> -->
					<li class="active"><a href="{{url('services')}}">Services </a></li>

				</ol>
			</div>
		</div>
	</div>
</section>

<section style="margin-bottom: 70px;margin-top: 50px;">
	<div class="container">
		<div class="_colwrap _centered">
			<div class="row">
				<div class="col-md-12 text-left">
					<!--<h3 class="sub-heading" style="color: #000;"><b>Registering, Ordering, Scheduling</b></h3>-->
					<p class="sub-heading-para schedule-p" style="display: inline-block;padding-bottom: 10px; color: #000;font-size: 15px;">Sign up and attend one or more of our coaching sessions or programs
						below. We suggest getting in touch with us through our H-Love
						contact channel with your coaching needs prior to signing up, so we
						may ensure that we’re a good fit for each other. Once signed up,
						you can login to your account and can control your project through
					a dashboard.</p>
					<p class="sub-heading-para schedule-p" style="display: inline-block;padding-bottom: 40px; color: #000; font-size: 15px;">Kindly note that we accept full payment in advance. Sessions and
						programs once booked cannot be cancelled or refunded, and require
					at least 24 hours’ notice for re-scheduling.</p>

					<ul style="color: #000; font-size: 15px;font-family: Verdana,Geneva,sans-serif !important;">
						<li>
						NGOs (Non-profits), Schools, Colleges, Low Income Personal</li>
						<li>
						Corporates, Startups, Standard Personal</li>
						<li>
						International</li>
					</ul>


				</div>
			</div>
		</div>
	</div>
</section>


<section class="section_padding_top_65 individual-coaching-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ">
				<h3 class="sub-heading text-center"><b>Hollistic Core Life Education (HCLE)</b></h3>
				<p class="sub-heading-para" style="color: #000; display: inline-block;padding-bottom: 20px;font-size: 18px;">Hollistic Core Life Education (HCLE) is the most diverse and comprehensive
					educational material we have created in the area of human development.
					The material comprises of five core themes, that we believe to be crucial to
					life improvement, at both the personal and professional levels.
				</p>
			</div>

		</div>
	</div>
</section>

<section>
	<div class="page-section" style="padding-top: 0px;">

		<div class="container">
			<div class="row">
				<h2 class="section_header color4 text-center" style="font-size: 25px;font-weight: bold;margin-bottom: 0;"> Themes</h2>
				<div class="col-xs-12 bottommargin_0 to_animate animated fadeInUp" data-animation="fadeInUp">
					<div class="row themes">
						<div class="col-xs-12 col-md-6">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-12">
									<div class="vertical-item text-center ds" style="height:auto;background: #fdbc22 !important;">
												<!-- <div class="item-media"> <img src="images/430x420_img1.jpg" alt="" style="height: 500px;"> </div>
												--><div class="item-content darken_gradient">
													<h3 class="entry-title curriculum-entry-title" style="margin-bottom: 33px;font-size: 21px;"> <b>Self</b> </h3>
													<p class="curriculum-content" style="color: #000;
													">One may say that the primary focus in life improvement work is
													improvement of the self, i.e. personal development. This is why it is
													imperative to pay attention to one&#39;s body, mind, heart, and soul, and
													thereby, take care of their physical, mental, emotional, and spiritual health.
													Our work on the Self helps you deeply explore and implement learnings from
												all of these areas in your life.</p>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-12">
										<div class="vertical-item  text-center ds" style="height: auto;background: #5bce4a !important;">
												<!-- <div class="item-media"> <img src="images/430x420_img5.jpg" alt="" style="height: 500px;"> </div>
												--><div class="item-content darken_gradient">
													<h3 class="entry-title curriculum-entry-title" style="margin-bottom: 36px;font-size: 21px;"> <b>People</b> </h3>
													<p class="curriculum-content" style="color: #464545;">We are happiest when we have happy and fulfilling relationships with the
														people in our lives. Through People work, learn how to deal with people and
														navigate through both easy and difficult relationships in your life better, in
														order to take your experiences with other people to higher levels.<br><br><br></p>
													</div>
												</div>
											</div>

										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-12">
												<div class="vertical-item text-center ds" style="height: auto;background: #b12525 !important;">
												<!-- <div class="item-media"> <img src="images/430x420_img3.jpg" alt="" style="height: 500px;"> </div>
												--><div class="item-content darken_gradient">
													<h3 class="entry-title curriculum-entry-title" style="font-size: 21px;margin-bottom: 34px;"> <b>Perspective</b> </h3>
													<p class="curriculum-content" style="color: #464545;">How do you look at unexpected events that occur in your life? Do you take
														them in your stride or fear that they will change your life for the worse?
														Through Perspective work, learn how to handle such occurrences better,
														making a shift in the way you respond to events which may not be in your
														control, while enabling yourself to re-focus and better manage those which
													are.</p>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-12">
											<div class="vertical-item text-center ds" style="height: auto;background: #498dea !important;">
												<!-- <div class="item-media"> <img src="images/430x420_img2.jpg" alt="" style="height: 500px;"> </div>
												--><div class="item-content darken_gradient">
													<h3 class="entry-title curriculum-entry-title" style="margin-bottom: 33px;font-size: 21px;"> <b>Communication</b> </h3>
													<p class="curriculum-content" style="color: #000;margin-bottom:32px;">The key to better relationships is improved communication within them.
														Learning to understand what to say to whom, when, and in which context,
														can be a key differentiator in the way the other person perceives and
														responds to that information. Working with Communication can help you
														achieve greater control over the quality of your interactions with the people
													in your life.</p>
												</div>
											</div>
										</div>

									</div>
								</div>
								<div class="col-xs-12 col-md-6 col-md-offset-3">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-12">
											<div class="vertical-item text-center ds" style="height: auto;background: #7d3f18 !important;">
												<!-- <div class="item-media"> <img src="images/430x420_img6.jpg" alt="" style="height: 500px;"> </div>
												--><div class="item-content darken_gradient">
													<h3 class="entry-title curriculum-entry-title" style="margin-bottom: 36px;font-size: 21px;"> <b>World</b> </h3>
													<p class="curriculum-content" style="color: #464545;margin-bottom: 34px;">Understanding how the world works through signs, vibes, and other
														information from your surroundings can be very important in decision-
														making and moving through your day and life with greater ease and success.
														Through working with the World, learn how to read these cues and live with
													greater flow and connection with your environment.</p>
												</div>
											</div>
										</div>
										<!-- <div class="col-xs-12 col-sm-6 col-md-12">
											<div class="vertical-item content-absolute text-center ds">
												<div class="item-media"> <img src="images/430x420_img4.jpg" alt=""> </div>
												<div class="item-content darken_gradient">
													<h3 class="entry-title curriculum-entry-title"> <a href="#"><b>Love &amp; Relationships</b></a> </h3>
													<p class="curriculum-content">Find Your Fit</p>
												</div>
											</div>
										</div> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section_padding_top_40 individual-coaching-section">
			<div class="container">
				<div class="row">

					<div class="col-md-12 text-left">
						<h3 class="sub-heading" style="font-size: 21px !important;"> <b>Private Coaching</b></h3>
						<p class="sub-heading-para" style="color: #000;font-size: 15px;font-family: Verdana,Geneva,sans-serif!important;">It’s easier now than ever before to seek guidance
							in any area of life. Using carefully selected elements from our five-
							module HCLE program, a professional coach will work with you to help
							transform the way you think about yourself and your life. The sessions
							are tailored to your specific needs to help your own understanding and
							management of life’s challenges. After evaluating your case over a
							short call and working with you over one or more free-flowing one-on-one sessions, our aim is to leave you in a transformed state of mind.
						Come, find your true and best self with Hollistic!</p>
					</div>
				<!-- <div class="col-md-12 text-center">
					<div class="empower-yourself-coaching">
						<div class="embed-responsive embed-responsive-3by2 custom-embed-responsive-3by2">
							<a href="http://player.vimeo.com/video/1084537" class="embed-placeholder custom-embed-placeholder">
								<img src="images/gallery/02.jpg" alt="">
							</a>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</section>

	<!-- <section class="ls page_portfolio columns_padding_0 container_padding_0 yourself-coaching-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3 class="sub-heading"> <b>Empower Yourself with coaching</b></h3>
					<p class="sub-heading-para" style="width: 60%;display: inline-block;padding-bottom: 40px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
				</div>
				<div class="col-sm-12">
					<div class="row masonry-layout columns_margin_0" data-filters=".isotope_filters">
						<div class="col-md-3">
							<div class="custom-gallery-item" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Spiritual</b></h3>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="custom-gallery-item1" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Parenting</b></h3>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="custom-gallery-item2" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Social</b></h3>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="custom-gallery-item3" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Career</b></h3>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row masonry-layout columns_margin_0" data-filters=".isotope_filters">
						<div class="col-md-3">
							<div class="custom-gallery-item5" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Intellectual</b></h3>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="custom-gallery-item6" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Emotional</b></h3>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="custom-gallery-item7" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Character</b></h3>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="custom-gallery-item8" style="height: 265px;">
								<div class="item-content">
									<a href="#">
										<h3 class="text-white-light"><b>Financial</b></h3>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->

	<section class="">
		<div class="container">
			<!-- <div class="row text-center organisational-heading">
				<div class="col-md-12">
					<h3 class="sub-heading"><b>Group / Organisational Coaching</b></h3>
					<p class="sub-heading-para">A coaching session isn’t always a
solitary exercise. With group coaching, we take everyone for a ride.
We use our core five-module program and a customised plan, to help
address all of the group’s needs. Our group sessions are intimate
conversation spaces that are focused on increasing awareness around
important issues, team-building, and taking constructive action that
benefits every member of the group. Learning about your own goals as
well as those of your team’s, along with your colleagues, friends, or
family can enhance your experience in a completely different way. Our
goal is for each person to leave the session energised with enough
learnings, ideas, tools, and techniques to apply in their groups and
organisational settings well into the future.
					</p>
				</div>
			</div> -->

			<div class="row social-coaching-section">
				<!-- <div class="col-md-6">
					<div class="">
						<img src="images/gallery/05.jpg" alt="" class="">
					</div>
				</div> -->
				<div class="col-md-12 text-left">
					<div class="">
						<h3 class="sub-heading social-coaching-custom" style="font-size: 21px !important;"><b>Group, Organisational, and Thematic Program Coaching</b></h3>
						<p class="client-p" style="color: #000;font-size: 15px;font-family: Verdana,Geneva,sans-serif !important;">A
							coaching session isn’t always a solitary exercise. With group coaching
							and our theme-based seminars/workshops, we take everyone for a
							ride. We use our five-module HCLE program and a customised plan, to
							help address all of the group’s needs. Our group sessions are
							structured as exploratory, brainstorming, experiential conversation
							spaces that are focused on organisational vision analysis, business
							model analysis, ideation and innovation, operational analysis and re-
							planning, organisational development, personal development with life
							skills training, improving communication, increasing awareness around
							important issues, team-building, and taking constructive action that
							benefits every member of the group. Learning about your own goals as
							well as those of your organisation’s and team’s, along with your
							colleagues, friends, or family, using the magical craft of coaching, can
							enhance your experience in a completely different way. Our goal is for
							each person to leave the session energised with enough learnings,
							ideas, tools, and techniques to apply in their personal lives as well as
						their groups and organisational settings well into the future.</p>
						<!-- <p>Shank tri-tip hamburger bacon pancetta tail. Strip steak andouille biltong cow tenderloin bacon brisket frankfurter sirloin tri-tip turducken beef bresaola. Bacon frankfurter ribeye kevin, tongue swine ham hock strip steak pork chop porchetta pancett.</p> -->
					</div>
				</div>
			</div>
			<div class="row sub-organisational-heading">
				<div class="col-md-12 text-left">
					<div class="">
						<h3 class="sub-heading" style="font-size: 21px !important;"> <b>Content Publications</b></h3>
						<p class="client-p" style="color: #000;font-size: 15px;font-family: Verdana,Geneva,sans-serif !important;">Our coaching and life improvement content
							comprises of a diverse mix from our five-module HCLE program.
							Browse through our extensive range of offerings, available through all
							major venues, both online and in the real world, to explore ideas from
							hundreds of themes in areas including the human mind, personal and
							organisational development, self-improvement, communication,
							dealing with people and relationships, conflict management, life skills,
							philosophy, psychology, spirituality, and much more. Browse through
							our own Content pages, our content channels on other major websites,
							or simply search by our company name, to locate our publications at
						any venue.</p>
						<!-- <p style="color: #464545;">After all,
there isn’t a better way to do justice to your own knowledge and skills
than to use it with those who could benefit as well. Become a Hollistic
Life Transformation Coach and help change the world!</p> -->
<p class="client-p" style="color: #000;font-size: 15px;font-family: Verdana,Geneva,sans-serif !important;">To begin your journey with us and further evaluate any of our Client
	Programs, reach us through our <a href="{{url('/contact')}}" style="color: #f3702b;"><b> Contact page</b></a>.</p>
</div>
</div>
				<!-- <div class="col-md-6">
					<div class="">
						<img src="images/gallery/05.jpg" alt="" class="">
					</div>
				</div> -->
			</div>
		</div>
	</section>


	{{-- Partner Program --}}




	<section style="margin-bottom: 70px;margin-top: 50px;">
		<div class="container">
			<div class="_colwrap _centered">
				<h3 class="text-center">
					<b>Coaching Partnerships</b>
				</h3>
				<div class="row">
					<div class="col-md-12">
						<a  style="color: #000;">
							<div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em" >
								<div class="row">
									<div class="col-sm-12">
										<b>
											<h4 style="font-size:21px;font-weight:bold;">Private Referrals</h4>
										</b>
										<br>
										<p style="color: #000; font-size: 15px;">We believe in helping improve lives by working in
											conjunction with others towards the greater good. Through our referral
											sharing program, we welcome referrals from as well as offer referrals
											to other coaches (including professionals from related areas*), when
											an individual or organisational client is looking for an alternate
											perspective, a broader understanding, or a specialised skillset that
											either of us can offer. Both the referring coach and referred coach
											benefit from a revenue sharing arrangement when a referral becomes
										an active client.</p>
										<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> -->
									</div>
									<!-- <div class="col-sm-6"></div> -->
								</div>
							</div>
						</a>
					</div>
					<div class="col-md-12">
						<a style="color: #000;">
							<div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em" >
								<div class="row">
									<div class="col-sm-12">
										<b>
											<h4 style="font-size:21px;font-weight:bold;">Group, Organisational, and Thematic Program Co-Management</h4>
										</b>
										<br>
										<p style="color: #000; font-size: 15px;">Coaches (including professionals from related areas*), may partner
											with us online and in-person, to jointly manage group and
											organisational projects and deliver public theme-based seminars and
											workshops in specific regions of the world. We work through alliances
											with corporations, start-ups, and socially-connected organisations,
											including government bodies and NGOs (non-profits). Areas of focus
											include organisational vision analysis, business model analysis,
											ideation and innovation, operational analysis and re-planning,
											organisational development, personal development with life skills
											training, communication, negotiation, mediation, peace-building,
											conflict resolution and transformation, therapy, and coaching.
											Together with other coaches, our group efforts can help organisations
											achieve higher levels of efficiency, while working towards the all-round
										development and well-being of the people.</p>
										<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> -->
									</div>
									<!-- <div class="col-sm-6"></div> -->
								</div>
							</div>
						</a>
					</div>
					<div class="col-md-12">
						<a style="color: #000;">
							<div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em" >
								<div class="row">
									<div class="col-sm-12">
										<b>
											<h4 style="font-size:21px;font-weight:bold;">Content Co-Development</h4>
										</b>
										<br>
										<p style="color: #000; font-size: 15px;">We believe that the life-altering impact
											that can be created through education is unparalled. We invite coaches
											(including professionals from related areas*) with an interest in the
											area of Life Improvement for both individuals and organisations to
											explore partnering with us to co-create and co-publish educational
											content in the form of books, e-books, audio podcasts, video
											interviews and discussions, online courses, and other media formats.
											Our jointly developed work, distributed through multiple online and

											real world channels, will have the ability to positively impact and
										enhance a great number of lives.</p>
										<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> -->
									</div>
									<!-- <div class="col-sm-6"></div> -->
								</div>
							</div>
						</a>
					</div>
							<!-- <div class="col-md-12"><a href="#" style="color: #000;"><div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em"><div class="row"><div class="col-sm-6"><h4>Referral Sharing</h4><p style="color: #464545;">We believe in helping improve lives by working in conjunction with others towards the greater good. Through our referral sharing program, we welcome referrals from other coaches when a client is looking for a broader perspective that we may be a good fit for, and we offer referrals to other coaches when we believe a client requires a very specific skillset that is better delivered by a specialist. Both the referring coach and receiving coach benefit from a revenue sharing arrangement when a referral becomes an active client.
</p>
<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> ></div><div class="col-sm-6"></div></div></div></a></div> -->
</div>
</div>
<p class="partner-p" style="font-family: Verdana,Geneva,sans-serif !important;">*Professionals from areas related with coaching include counsellors,
	therapists, facilitators, mediators, consultants, advisors and trainers from
both personal and organisational spheres.</p>
<p class="partner-p" style="font-family: Verdana,Geneva,sans-serif !important;">To explore partnering with us and further evaluate any of our Partner
	Programs, reach us through our <a href="{{url('/contact')}}" style="color: #e85242;">
		<b>Contact page</b></a>.
	</p>
</div>
</section>


{{-- <section style="background-color: #f1f0f0;padding-top: 25px;padding-bottom: 25px;">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<!-- <p>Schedule Session</p> -->
				<h4 style="margin-top: 0px;">Schedule Session:</h4>
				<a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a>
			</div>
		</div>
	</div>
</section>
 --}}
<!-- Modal -->
<div class="modal fade" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="scheduleModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="scheduleModal">Schedule Session</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<div class="form-group">
							<label for="formGroupExampleInput">Name</label>
							<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
						</div>
						<div class="form-group form-check">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<label class="input-group-text" for="inputGroupSelect01">Type of Organization</label>
								</div>
								<select class="custom-select" id="inputGroupSelect01">
									<option selected>Choose...</option>
									<option value="1">NgO, Schools, Colleges, Low Income Personal Rate</option>
									<option value="2">Corporate, Startups, Standard Personal Rate</option>
									<option value="3">International Rate</option>
								</select>
							</div>

						</div>
					</div>
					<div class="form-group">
						<label for="formGroupExampleInput">Rate</label>
						<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
					</div>

					<a href="/schedule" class="theme_button color2 margin_0">Schedule Session</a>
					<!--   <button type="submit" class="btn btn-primary">Submit</button> -->
				</form>
			</div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
    </div> -->
</div>
</div>
</div>
@endsection

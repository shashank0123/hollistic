@extends('layouts.app')

@section('page', 'Capital')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2>Capital</h2>
        <ol class="breadcrumb greylinks">
          <li>
            <a href="{{url('/')}}">
                            Home
                        </a>
          </li>
          <li class="active">
            <a href="{{url('capital')}}" >Capital</a>
          </li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="section_padding_top_65 section_padding_bottom_65 individual-coaching-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center">
          <b>Conscious Capital</b>
        </h3>
        <p class="sub-heading-para" style="color: #000; font-size: 18px;">Hollistic believes in the conscious investing of capital in ideas that can create
a positive and meaningful impact on a great number of lives. Our criteria for
investing include:</p>
        <div class="col-md-12">
          <table style="width: 100%;" border = "2" cellpadding = "2">
            <tbody>
              <tr>
                <td style="background: #267b1b;
    font-size: 15px;
    text-align: center;
    align-items: center;
    color: #fff;
    padding-top: 17px;
    font-weight: bold;border: 1px solid #000000;"> Project </td>
                <td style="background: #cecece;border: 1px solid #000000;"> Conscious ideas with globally scalable business models and
the potential to create positive and meaningful impact on a great
number of lives, while providing a significant return on invested capital</td>
              </tr>
              <tr>
                <td style="background: #267b1b;
    font-size: 15px;
    text-align: center;
    align-items: center;
    color: #fff;
    font-weight: bold;border: 1px solid #000000;">Team  </td>
                <td style="background: #cecece;border: 1px solid #000000;">Strong, passionate, and driven execution team</td>
              </tr>
              <tr>
                <td style="background: #267b1b;
    font-size: 15px;
    text-align: center;
    align-items: center;
    color: #fff;
    font-weight: bold;border: 1px solid #000000;">Regions  </td>
                <td style="background: #cecece;border: 1px solid #000000;">US, China (via HK/SG), and India, but also open to other
select locations </td>
              </tr>
              <tr>
                <td style="background: #267b1b;
    font-size: 15px;
    text-align: center;
    align-items: center;
    color: #fff;
    font-weight: bold;border: 1px solid #000000;"> Sectors </td>
                <td style="background: #cecece;border: 1px solid #000000;">Finance, Technology, Healthcare, and Consumer Services</td>
              </tr>
              <tr>
                <td style="background: #267b1b;
    font-size: 15px;
    text-align: center;
    align-items: center;
    color: #fff;
    font-weight: bold;border: 1px solid #000000;"> Company Type</td>
                <td style="background: #cecece;border: 1px solid #000000;">Corporations or Private Limited Companies with 2 or
more owners/partners only</td>
              </tr>
              <tr>
                <td style="background: #267b1b;
    font-size: 15px;
    text-align: center;
    align-items: center;
    color: #fff;
    font-weight: bold;border: 1px solid #000000;">Investment Size </td>
                <td style="background: #cecece;border: 1px solid #000000;">Early-stage Angel/Pre-seed/Seed startup capital in exchange for equity in the company </td>
              </tr>
              <tr>
                <td style="background: #267b1b;
    font-size: 15px;
    text-align: center;
    align-items: center;
    color: #fff;
    font-weight: bold;border: 1px solid #000000;"> Investment Horizon</td>
                <td style="background: #cecece;border: 1px solid #000000;">3 years </td>
              </tr>
            </tbody>
          </table>
          <br>
          <!--<ul class="capital">-->
          <!--  <li><span><b>Project:</b> Conscious ideas with globally scalable business models and-->
          <!--the potential to create positive and meaningful impact on a great-->
          <!--number of lives, while providing a significant return on invested capital</span></li>-->
          <!--<li><span><b>Team:</b> Strong, passionate, and driven execution team</span></li>-->
          <!--<li><span><b>Regions:</b> The two largest startup ecosystems, namely the US and India, but also open to other select locations</span></li>-->
          <!--<li><span><b>Sectors:</b> Finance, Technology, Healthcare, Consumer Services</span></li>-->
          <!--<li><span><b>Company Type:</b> Corporations or Private Limited Companies with 2 or more owners/partners only</span></li>-->
          <!--<li><span><b>Investment Size:</b> Early-stage Angel/Pre-seed/Seed startup capital in exchange for equity in the company-->
          <!--</span></li>-->
          <!--<li><span><b>Investment Horizon:</b> 3 years</span></li>-->
          <!--</ul>-->
        </div>
        <p style="font-size:18px; color:#000;">Hollistic also partners with other investors in order to evaluate investment
opportunities, syndicate investment capital, and explore co-investing
strategies.</p>
        <p style="font-size:18px; color:#000;">Entrepreneurs and investors wishing to discuss projects or ideas for
investment may reach us through our <a href="{{url('/contact')}}" style="color: #f3702b;"><b>Contact page</b></a>.
        </p>
      </div>
      <!-- <div class="col-md-3"><div class="card" style="width: 18rem;"><div class="card-body"><h5 class="card-title">Project</h5>
      <!--  <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6><p class="card-text">Conscious ideas with globally scalable business models and
the potential to create positive and meaningful impact on a great
number of lives, while providing a significant return on invested capital</p>
      <!--  <a href="#" class="card-link">Card link</a><a href="#" class="card-link">Another link</a></div></div></div> -->
      <!--  <div class="col-md-3"><div class="card" style="width: 18rem;"><div class="card-body"><h5 class="card-title">Team</h5><h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6><p class="card-text">Strong, passionate, and driven execution team</p><a href="#" class="card-link">Card link</a><a href="#" class="card-link">Another link</a></div></div></div> -->
      <!-- <div class="col-md-3"><div class="card" style="width: 18rem;"><div class="card-body"><h5 class="card-title">Regions</h5><h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6><p class="card-text">The two largest startup ecosystems, namely the US and
    India, but also open to other select locations</p><a href="#" class="card-link">Card link</a><a href="#" class="card-link">Another link</a></div></div></div> -->
      <!--  <div class="col-md-3"><div class="card" style="width: 18rem;"><div class="card-body"><h5 class="card-title">Sectors</h5><h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6><p class="card-text">Finance, Technology, Healthcare, Consumer Services</p><a href="#" class="card-link">Card link</a><a href="#" class="card-link">Another link</a></div></div></div> -->
      <!-- <div class="col-md-3"><div class="card" style="width: 18rem;"><div class="card-body"><h5 class="card-title">Company Type:</h5><h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6><p class="card-text">Corporations or Private Limited Companies with 2 or
more owners/partners only</p><a href="#" class="card-link">Card link</a><a href="#" class="card-link">Another link</a></div></div></div> -->
      <!--
                     <div class="col-md-3"><div class="card" style="width: 18rem;"><div class="card-body"><h5 class="card-title">Investment Size</h5><h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6><p class="card-text">Early-stage Angel/Pre-seed/Seed startup capital in
exchange for equity in the company</p><a href="#" class="card-link">Card link</a><a href="#" class="card-link">Another link</a></div></div></div> -->
      <!--  <div class="col-md-3"><div class="card" style="width: 18rem;"><div class="card-body"><h5 class="card-title">Investment Horizon</h5><h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6><p class="card-text">3 Years</p><a href="#" class="card-link">Card link</a><a href="#" class="card-link">Another link</a></div></div></div> -->
    </div>
  </div>
</section>
@endsection

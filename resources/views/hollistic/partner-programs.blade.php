@extends('layouts.app')

@section('page', 'Partner Programs')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65 partner-program-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>Partner Programs</h2>
				<ol class="breadcrumb greylinks">
					<li>
						<a href="{{url('/')}}">
							Home
						</a>
					</li>
					<li>
						<a href="{{url('/programs')}}">Coaching</a>
					</li>
					<li class="active">
						<a href="{{url('partner-programs')}}"> Partner Programs </a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<section style="margin-bottom: 70px;margin-top: 50px;">
	<div class="container">
		<div class="_colwrap _centered">
		    <h3 class="text-center">
          <b>Coaching Partnerships</b>
        </h3>
			<div class="row">
				<div class="col-md-12">
					<a  style="color: #000;">
						<div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em" >
							<div class="row">
								<div class="col-sm-12">
									<b>
										<h4 style="font-size:21px;font-weight:bold;">Private Referrals</h4>
									</b>
									<br>
										<p style="color: #000; font-size: 15px;">We believe in helping improve lives by working in
conjunction with others towards the greater good. Through our referral
sharing program, we welcome referrals from as well as offer referrals
to other coaches (including professionals from related areas*), when
an individual or organisational client is looking for an alternate
perspective, a broader understanding, or a specialised skillset that
either of us can offer. Both the referring coach and referred coach
benefit from a revenue sharing arrangement when a referral becomes
an active client.</p>
										<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> -->
									</div>
									<!-- <div class="col-sm-6"></div> -->
								</div>
							</div>
						</a>
					</div>
					<div class="col-md-12">
						<a style="color: #000;">
							<div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em" >
								<div class="row">
									<div class="col-sm-12">
										<b>
											<h4 style="font-size:21px;font-weight:bold;">Group, Organisational, and Thematic Program Co-Management</h4>
										</b>
										<br>
											<p style="color: #000; font-size: 15px;">Coaches (including professionals from related areas*), may partner
with us online and in-person, to jointly manage group and
organisational projects and deliver public theme-based seminars and
workshops in specific regions of the world. We work through alliances
with corporations, start-ups, and socially-connected organisations,
including government bodies and NGOs (non-profits). Areas of focus
include organisational vision analysis, business model analysis,
ideation and innovation, operational analysis and re-planning,
organisational development, personal development with life skills
training, communication, negotiation, mediation, peace-building,
conflict resolution and transformation, therapy, and coaching.
Together with other coaches, our group efforts can help organisations
achieve higher levels of efficiency, while working towards the all-round
development and well-being of the people.</p>
											<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> -->
										</div>
										<!-- <div class="col-sm-6"></div> -->
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-12">
							<a style="color: #000;">
								<div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em" >
									<div class="row">
										<div class="col-sm-12">
											<b>
												<h4 style="font-size:21px;font-weight:bold;">Content Co-Development</h4>
											</b>
											<br>
												<p style="color: #000; font-size: 15px;">We believe that the life-altering impact
that can be created through education is unparalled. We invite coaches
(including professionals from related areas*) with an interest in the
area of Life Improvement for both individuals and organisations to
explore partnering with us to co-create and co-publish educational
content in the form of books, e-books, audio podcasts, video
interviews and discussions, online courses, and other media formats.
Our jointly developed work, distributed through multiple online and

real world channels, will have the ability to positively impact and
enhance a great number of lives.</p>
												<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> -->
											</div>
											<!-- <div class="col-sm-6"></div> -->
										</div>
									</div>
								</a>
							</div>
							<!-- <div class="col-md-12"><a href="#" style="color: #000;"><div class="card hiw-hightlight-one soft-shadow page-sec-rounded pad3em"><div class="row"><div class="col-sm-6"><h4>Referral Sharing</h4><p style="color: #464545;">We believe in helping improve lives by working in conjunction with others towards the greater good. Through our referral sharing program, we welcome referrals from other coaches when a client is looking for a broader perspective that we may be a good fit for, and we offer referrals to other coaches when we believe a client requires a very specific skillset that is better delivered by a specialist. Both the referring coach and receiving coach benefit from a revenue sharing arrangement when a referral becomes an active client.
</p>
							<!-- <div class="" style="margin-top: 10px;"><a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a></div> ></div><div class="col-sm-6"></div></div></div></a></div> -->
						</div>
					</div>
					<p class="partner-p" style="font-family: Verdana,Geneva,sans-serif !important;">*Professionals from areas related with coaching include counsellors,
therapists, facilitators, mediators, consultants, advisors and trainers from
both personal and organisational spheres.</p>
					<p class="partner-p" style="font-family: Verdana,Geneva,sans-serif !important;">To explore partnering with us and further evaluate any of our Partner
Programs, reach us through our <a href="{{url('/contact')}}" style="color: #e85242;">
							<b>Contact page</b></a>.
					</p>
				</div>
			</section>
			<div class="row">
				<div class="col-sm-12"></div>
			</div>
			<section style="background-color: #f1f0f0;padding-top: 25px;padding-bottom: 25px;">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<p style="color: #464545;">Ready to proceed with a program?</p>
							<h4 style="margin-top: 0px;">Register for a Partner Account and
schedule your discussion here:</h4>
							<a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a>
						</div>
					</div>
				</div>
			</section>

@endsection

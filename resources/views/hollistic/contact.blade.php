@extends('layouts.app')

@section('page', 'Contact Us')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>Contact Us</h2>
				<ol class="breadcrumb greylinks">
					<li>
						<a href="{{url('/')}}">
							Home
						</a>
					</li>
					<!-- <li><a href="#">Pages</a></li> -->
					<li class="active">
						<a href="{{url('/contact')}}"> Contact Us </a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>


<section class="ls columns_padding_25 section_padding_top_30 section_padding_bottom_65">
	<div class="container">
		<!-- <div class="row"><div class="col-sm-12"><div class="alert alert-danger custom-alert-msg">
	  					 		Use this form only for technical queries:
							</div></div></div> -->
		<div class="row">
			<div class="col-sm-12">
				<span style="font-weight: 700;font-size: 17px;color: #e85242; font-family: Verdana,Geneva,sans-serif;">Client Queries:</span>
				<br>
					<p class="contact-p" style="font-family: Verdana,Geneva,sans-serif !important;">For any technical queries, including those related with sign-up, scheduling, or payments, please fill up the form below or email us at <a style="color: #e85242;" href="mailto:Hello@Hollistic.org">Hello@Hollistic.org</a>
and we will get back to you as soon as we can:</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 to_animate" data-animation="scaleAppear">
					<form class="contact-form columns_padding_5" method="post" action="http://webdesign-finder.com/html/diversify/">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group bottommargin_0">
									<label for="name">Full Name
										<span class="required">*</span>
									</label>
									<i class="fa fa-user highlight" aria-hidden="true"></i>
									<input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group bottommargin_0">
										<label for="phone">Phone Number
											<span class="required">*</span>
										</label>
										<i class="fa fa-phone highlight" aria-hidden="true"></i>
										<input type="text" aria-required="true" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Phone Number">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group bottommargin_0">
											<label for="email">Email address
												<span class="required">*</span>
											</label>
											<i class="fa fa-envelope highlight" aria-hidden="true"></i>
											<input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email Address">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group bottommargin_0">
												<label for="subject">Subject
													<span class="required">*</span>
												</label>
												<i class="fa fa-flag highlight" aria-hidden="true"></i>
												<input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">
												</div>
											</div>
											<div class="col-sm-12">
												<div class="contact-form-message form-group bottommargin_0">
													<label for="message">Message</label>
													<i class="fa fa-comment highlight" aria-hidden="true"></i>
													<textarea aria-required="true" rows="3" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea>
												</div>
											</div>
											<div class="col-sm-12 bottommargin_0">
												<div class="contact-form-submit">
													<button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1 margin_0">Send message</button>
													<br>

														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="row">
										<div class="">
											<!--<div class="col-md-12">-->
											<!--	<div class="media contact-section">-->
											<!--		<span style="font-weight: 700; color: #e85242;font-size: 17px;font-family: Verdana,Geneva,sans-serif !important;">Partner Queries:</span>-->
											<!--		<p style="color: #000; font-size: 15px;">For coaching partner-related queries, you may get in touch with us via email-->

											<!--			<a href="mailto:Hello@Hollistic.org"> Hello@Hollistic.org</a>.-->
											<!--		</p>-->
											<!--	</div>-->
											<!--</div>-->
											<div class="col-md-12">
												<div class="media contact-section-2">
													<span style="font-weight: 700; color: #e85242;font-size: 17px;font-family:  Verdana,Geneva,sans-serif !important;">Business Queries:</span>
													<p class="contact-p">For business-related queries, including those from entrepreneurs and
investors, please connect with us on
														<a href="http://www.linkedin.com/in/hollistic" target="_blank" style="color: #e85242;">LinkedIn</a>.
													</p>
													<!-- <div class="media-left"><i class="fa fa-linkedin fontsize_18 highlight" aria-hidden="true"></i></div> -->
													<!-- <div class="media-body"></div> -->
												</div>
											</div>
											<div class="col-md-12">
												<span style="color: #e85242; font-size: 17px;font-family: Verdana,Geneva,sans-serif;">
											<b>Coaching Queries (H-Love):</b>
												</span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="container">
											<div class="">
											    <a target="_blank" href="https://wa.me/919577119900">
												<img class="h-love-img" src="images/about/H-Love.png" style="border: 5px solid #f1ce6d;margin-left: -9px;width: 120px;">
												</a>
												</div>
												<!-- <div class="col-md-4"><img src="images/about/H-Love.png" style="height: 357px;"></div> -->
												<!-- <div class="col-md-8 text-left"><h3 class="sub-heading" style="font-size: 22px;"><b>Coaching Queries (H-Love):</b></h3><p class="sub-heading-para" style="display: inline-block;">Take the first step towards changing your life by sending your coaching-related queries to our team’s head coach, Mr. H, over WhatsApp at 9900! Please introduce yourself and include your query in the same message so that we can provide a prompt response from a special service that Hollistic offers called H-Love.
</p><p  class="sub-heading-para" style="display: inline-block;">H-Love is the Unconditional Coaching facility that we offer to anyone who becomes a part of the Hollistic family, simply by contacting us. This free service is available for you any time you may be feeling challenged and wish to talk to someone, or for any quick help or suggestion that you may need in your daily lives. Contact us through H-Love by sending us your entire query in a compact message and our coaches will get back to you as soon as we can with our unconditional best!
</p><p class="sub-heading-para" style="display: inline-block;">needs and ensure that we are the best people to help you prior to proceeding.
</p></div> -->
											</div>
										</div>
										<div class="row">
											<div class="">
												<div class="col-md-12 text-left">
													<p class="sub-heading-para contact-p" >Take the first step towards changing your life by sending your coaching-related queries to our team’s head coach, who takes on the role of Mr. H,
over WhatsApp at
														<a target="_blank" href="https://wa.me/919577119900">
															<img src="https://img.icons8.com/color/48/000000/whatsapp.png" style="width: 26px;margin-top: -4px;">+91-95-7711-9900
															</a>! Please introduce yourself and include
your query in the same message so that we can provide a prompt response
from a special service that Hollistic offers called H-Love.
														</p>
														<p class="sub-heading-para contact-p" >H-Love is the Unconditional Coaching facility that we offer to anyone who
becomes a part of the Hollistic family, simply by contacting us. This free
service is available for you any time you may be feeling challenged and wish
to talk to someone, or for any quick help or suggestion that you may need in
your daily lives. All we ask if you’re happy with our service, is to leave us

your feedback and share the love, by sharing our link with others who could
also use our help. Contact us through H-Love by sending us your entire
query in a compact message and our team will get back to you as soon as
we can with our unconditional best!</p>
														<p class="sub-heading-para contact-p" >In case we cannot answer your query here, we may offer you to sign up for
a coaching session or refer you to a more suitable source. We do suggest
that you first contact us through H-Love before signing up for a coaching
session, so we may evaluate your needs and ensure that we are the best
people to help you prior to proceeding.</p>
														<p class="sub-heading-para contact-p" >In case we cannot answer your query here, we may offer you to sign up for
a coaching session or refer you to a more suitable source. We do suggest
that you first contact us through H-Love before signing up for a coaching
session, so we may evaluate your needs and ensure that we are the best
people to help you prior to proceeding.</p>
														<p class="sub-heading-para contact-p" >Please keep in mind that as we are coaches specialised in Life Improvement,
we can assist you to sort through your own thoughts and emotions and help
you make better decisions for yourself. However, we will be unable to help
you with other types of queries. All messages exchanged though this service
are to be kept strictly confidential at both ends. We request you not to re-
post these messages on social media or forward them by email. All
communication must be non-critical in nature. In case of such situations,
please contact the police or emergency services. Finally, please note that we
reserve the right to withhold our services for any reason and we do not take
responsibility for any outcome from using this service. By proceeding to use
this service, you agree to all of the terms stated above. Click on the contact
link above to continue to H-Love!</p>
													</div>
												</div>
											</div>
											<!-- <div class="col-md-12"><p class="sub-heading-para" style="display: inline-block;">Please keep in mind that as we are coaches specialised in life improvement, we can assist you to sort through your own thoughts and emotions and help you make better decisions for yourself. However, we are not virtual assistants, so will be unable to help you with other types of queries. All messages exchanged though this service are to be kept strictly confidential at both ends. We request you not to re-post these messages on social media or forward them by email. All communication must be non-critical in nature. In case of such situations, please contact the police or emergency services. Finally, please note that we do not take any responsibility for any outcome from using this service. By proceeding to use this service, you agree to all of the terms stated above.
</p></div> -->
										</div>
									</section>
@endsection

@extends('layouts.app')

@section('page', 'Schedule')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2>Schedule</h2>
					<ol class="breadcrumb greylinks">
						<li> <a href="{{url('/')}}">
							Home
						</a> </li>
						<!-- <li> <a href="#">Program</a> </li> -->
						<li class="active"><a href="{{url('schedule')}}">Schedule </a></li>

					</ol>
				</div>
			</div>
		</div>
	</section>

	<section style="margin-bottom: 70px;margin-top: 50px;">
		<div class="container">
			<div class="_colwrap _centered">
				<div class="row">
					<div class="col-md-12 text-left">
						<!--<h3 class="sub-heading" style="color: #000;"><b>Registering, Ordering, Scheduling</b></h3>-->
						<p class="sub-heading-para schedule-p" style="display: inline-block;padding-bottom: 10px; color: #000;font-size: 15px;">Sign up and attend one or more of our coaching sessions or programs
							below. We suggest getting in touch with us through our H-Love
							contact channel with your coaching needs prior to signing up, so we
							may ensure that we’re a good fit for each other. Once signed up,
							you can login to your account and can control your project through
							a dashboard.</p>
														<p class="sub-heading-para schedule-p" style="display: inline-block;padding-bottom: 40px; color: #000; font-size: 15px;">Kindly note that we accept full payment in advance. Sessions and
							programs once booked cannot be cancelled or refunded, and require
							at least 24 hours’ notice for re-scheduling.</p>

							<ul style="color: #000; font-size: 15px;font-family: Verdana,Geneva,sans-serif !important;">
								<li>
							NGOs (Non-profits), Schools, Colleges, Low Income Personal</li>
							<li>
							Corporates, Startups, Standard Personal</li>
							<li>
							International</li>
							</ul>


					</div>
				</div>
			</div>
		</div>
	</section>
			

<section style="background-color: #f1f0f0;padding-top: 25px;padding-bottom: 25px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<!-- <p>Schedule Session</p> -->
					<h4 style="margin-top: 0px;">Schedule Session:</h4>
					<a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#registerModal" style="color: #fff;">Apply Now</a>
				</div>
			</div>
		</div>
	</section>

<!-- Modal -->
<div class="modal fade" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="scheduleModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="scheduleModal">Schedule Session</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form>
  <div class="form-group">
    <div class="form-group">
    <label for="formGroupExampleInput">Name</label>
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
  </div>
  <div class="form-group form-check">
    <div class="input-group mb-3">
  <div class="input-group-prepend">
    <label class="input-group-text" for="inputGroupSelect01">Type of Organization</label>
  </div>
  <select class="custom-select" id="inputGroupSelect01">
    <option selected>Choose...</option>
    <option value="1">NgO, Schools, Colleges, Low Income Personal Rate</option>
    <option value="2">Corporate, Startups, Standard Personal Rate</option>
    <option value="3">International Rate</option>
  </select>
</div>

  </div>
  </div>
 <div class="form-group">
    <label for="formGroupExampleInput">Rate</label>
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
  </div>

  <a href="/schedule" class="theme_button color2 margin_0">Schedule Session</a>
<!--   <button type="submit" class="btn btn-primary">Submit</button> -->
</form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
@endsection

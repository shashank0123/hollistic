<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
    <title> {{ config('app.name', 'Hollistic') }} : A Global Life Improvement Ecosystem | @yield('page')</title>

    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{url('/')}}/assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('/')}}/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('/')}}/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/assets/images/favicon-16x16.png">
    <link rel="manifest" href="{{url('/')}}/assets/images/site.webmanifest">
    <link rel="mask-icon" href="{{url('/')}}/assets/images/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicons/favicon.png"> -->
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7CLora:400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="{{url('/')}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/assets/css/animations.css">
    <link rel="stylesheet" href="{{url('/')}}/assets/css/fonts.css">
    <!-- <link rel="stylesheet" href="assets/css/style.css"> -->
    <link rel="stylesheet" href="{{url('/')}}/assets/css/main.css" class="color-switcher-link">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/css/responsive.css">
    <link rel="stylesheet" href="{{url('/')}}/assets/css/shop.css" class="color-switcher-link">
    <script src="{{url('/')}}/assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="{{url('/')}}/assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/css/slick.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/css/custom.css" />
    <link href="{{url('/')}}/assets/css/lightbox.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <!-- <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css"/> -->
    <!--[if lt IE 9]>
		<script src="assets/js/vendor/html5shiv.min.js"></script>
		<script src="assets/js/vendor/respond.min.js"></script>
		<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->
    <style media="screen">
        .d-none {
            display: none;
        }

        .text-red {
            color: #ff0000;
        }

        .invalid-feedback {
          color: #ff0000;
        }
    </style>
</head>
<body>
  <!--[if lt IE 9]>
    <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
  <![endif]-->
    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
			<i class="rt-icon2-cross2"></i>
		</span>
        </button>
        <div class="widget widget_search">
            <form method="get" class="searchform search-form form-inline" action="http://webdesign-finder.com/html/diversify/">
                <div class="form-group bottommargin_0">
                    <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input"> </div>
                <button type="submit" class="theme_button no_bg_button">Search</button>
            </form>
        </div>
    </div>
    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
        <div class="fw-messages-wrap ls with_padding">
            <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
            <!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->
        </div>
    </div>
    <!-- eof .modal -->

    <div id="canvas">
      <div id="box_wrapper" class="">
        @include('contents.header')
        <main class="">
            @yield('content')
        </main>
        @include('contents.footer')
      </div>
    </div>

    <!-- model -->
    @include('contents.model')


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/assets/js/compressed.js"></script>
    <script src="{{url('/')}}/assets/js/selectize.min.js"></script>
    <script src="{{url('/')}}/assets/js/main.js"></script>
    <script src="{{url('/')}}/assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/slick.min.js"></script>
    <script src="{{url('/')}}/assets/js/lightbox.js"></script>
    <script src="{{url('/')}}/assets/js/hammer.js"></script>
    <script src="{{url('/')}}/assets/js/jquery.hammer.js"></script>
    <script type="text/javascript">
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true
        })

        $('#lightbox').hammer().on("swipe", function(event) {
            if (event.gesture.direction === 4) {
                console.log('swipe');
                $('#lightbox a.lb-prev').trigger('click');
            } else if (event.gesture.direction === 2) {
                console.log('swipe');
                $('#lightbox a.lb-next').trigger('click');
            }
        });
        $(function() {
            var cururl = window.location.pathname;
            var curpage = cururl.substr(cururl.lastIndexOf('/') + 1);
            var hash = window.location.hash.substr(1);
            if ((curpage == "" || curpage == "/" || curpage == "admin") && hash == "") {
                //$("nav .navbar-nav > li:first-child").addClass("active");
            } else {
                $(".mainmenu li").each(function() {
                    $(this).removeClass("active");
                });
                if (hash != "")
                    $(".mainmenu li a[href*='" + hash + "']").parents("li").addClass("active");
                else
                    $(".mainmenu li a[href*='" + curpage + "']").parents("li").addClass("active");
            }
        });
        const items = document.querySelectorAll(".accordion a");

        function toggleAccordion() {
            this.classList.toggle('active');
            this.nextElementSibling.classList.toggle('active');
        }

        items.forEach(item => item.addEventListener('click', toggleAccordion));
        $('#view-more').click(function(event) {
            event.preventDefault();
            var pin = '1234' // Will be fetched from backend.
            var upin = $('#gpin').val();

            if (pin == upin) {
                $('#gallery-accessor').hide();
                $('#private-gallery').show();

            } else {
                $('#gpin').css('border-color', 'red');
                $('#gpin-success').hide();
                $('#gpin-message').show();
            }

        });
    </script>
</body>
</html>

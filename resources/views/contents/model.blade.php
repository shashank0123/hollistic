<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document" style=" width: 100%;height: 100%;margin-top: 0px;">
				<div class="modal-content" style="height: auto;min-height: 100%;border-radius: 0;">
					<div class="modal-header" style="border-bottom: unset;padding-top: 10px;padding-bottom: 0px;">
					    <!-- <h5 class="modal-title" id="exampleModalLabel">Hollistic</h5> -->
					    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					        <span aria-hidden="true">&times;</span>
					    </button>
				    </div>
				    <div class="modal-body" style="padding-top: 0px !important;">
				    	<div class="container-fluid">
				    		<div class="row">
				    			<div class="col-md-12 text-center" style="margin-bottom: 30px;">
				    				<h1 style="margin-bottom: 10px;">Get Your Winning Edge</h1>
				    				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    				tempor incididunt ut labore et dolore magna aliqua.</p>
				    			</div>
				    		</div>
				    	<form class="row">
							<div class="form-group col-md-6">
							    <!-- <label for="formGroupExampleInput">Example label</label> -->
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="First Name" required>
							</div>

						    <div class="form-group col-md-6">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Last Name" required>
						    </div>

						    <div class="form-group col-md-6">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Phone Number" required>
							</div>

						    <div class="form-group col-md-6">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Email" required>
						    </div>

						    <div class="form-group col-md-12">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Country" required>
							</div>

						    <div class="form-group col-md-12">
							    <input type="text" style="border:0;border-bottom: 1px solid #ccc !important;border-radius: 0px !important" class="form-control" id="formGroupExampleInput" placeholder="Occupation" required>
						    </div>

						    <div class="col-md-12 text-center">
								<a href="#" class="theme_button color2 margin_0" style="width: 235px;margin-top: 20px !important">Submit</a>
						    </div>

						    <div class="col-md-12 text-center">
						    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						    	consequat.</p>
						    </div>
						</form>
						</div>
				    </div>
				   <!--  <div class="modal-footer" style="border-top: unset;">
					    <button type="button" class="btn btn-secondary modal-close-btn" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary">Save changes</button>
				    </div> -->
				</div>
			</div>
		</div>

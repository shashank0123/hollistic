
            <footer class="page_footer template_footer ds ms parallax overlay_color section_padding_top_110 section_padding_bottom_100 columns_padding_25">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="subscribe" class="widget widget_mailchimp with_padding cs main_color2 parallax overlay_color topmargin_0">
                                <h3 class="widget-title">Our Newsletter</h3>
                                <form class="signup" action="subscribe.store" method="get">
                                    <div class="form-group-wrap">
                                        <div class="form-group">
                                            <label for="mailchimp" class="sr-only">Enter your email here</label>
                                            <input name="email" type="email" autocomplete="off" id="mailchimp" class="mailchimp_email form-control text-center" placeholder="Your Email Address">
                                        </div>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                        <button type="submit" class="theme_button color2">Subscribe now</button>
                                    </div>
                                    <div class="response"></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                            <div class="widget widget_text widget_about">
                                <a href="{{url('/')}}" class="logo logo_with_text bottommargin_10">
                                    <img src="images/logo5.png" alt="">
                                    <span class="logo_text">
                            Hollistic<sup style="font-size:11px;top: -1.5em;">&reg;</sup>
									<small style="color: #6993cd;border-color: #6993cd; font-weight: 700;margin-left: -7%;">LIFE IMPROVEMENT</small>
								</span>
                                </a>
                                <p>Any and all information displayed here is copyright of Hollistic<sup style="font-size: 11px;">®</sup> unless another source or copyright-holder is cited. If any information has been addressed or sent to any person or organisation, it may be of a confidential nature and intended solely for the addressee(s).
                                    <a href="#" style="color: #acadb8;" data-toggle="modal" data-target="#hollisticModal"> Read More...</a>
                                </p>
                                <p class="topmargin_25">
                                    <a class="social-icon border-icon rounded-icon socicon-youtube" target="_blank" href="https://www.youtube.com/channel/UCJzsP1_r3YoNVNdf06n4kXw" title="Youtube"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-blogger" target="_blank" href="https://www.blogger.com/u/2/blogger.g#welcome" title="Twitter"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-vimeo" target="_blank" href="https://vimeo.com/hollistic" title="vimeo" title="vimeo"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-instagram" target="_blank" href="https://www.facebook.com/hollisticmedia" title="instagram"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-facebook" target="_blank" href="https://www.facebook.com/hollisticmedia" title="Linkedin"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-pinterest" target="_blank" href="https://in.pinterest.com/hollisticmedia/" title="Youtube"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-tumblr" target="_blank" href="https://www.tumblr.com/blog/hollisticmedia" title="Tumblr"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-snapchat" target="_blank" href="https://www.snapchat.com/add/hollisticmedia" title="snapchat"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-reddit" target="_blank" href="https://www.reddit.com/user/hollisticmedia" title="reddit"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-quora" target="_blank" href="https://www.quora.com/profile/Hollistic-Media" title="quorq"></a>
                                    <a class="social-icon border-icon rounded-icon socicon-flickr" target="_blank" href="https://www.flickr.com/photos/170872704@N04/" title="flickr"></a>
                                </p>
                            </div>
                        </div>
                        <!-- <div class="col-md-3 col-sm-6 col-xs-12 text-center"><div class="widget widget_text"><h3 class="widget-title">New Here?</h3><ul class="list-unstyled greylinks"><li class="media"><a href="partner-program.php"> Partner Programs </a></li><li class="media"><a href="client-programs.php"> Client Programs </a></li><li class="media"><a href="venture-program.php">  Venture Programs </a></li></ul></div></div> -->
                        <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                            <div class="widget widget_text">
                                <h3 class="widget-title">Quick Links</h3>
                                <ul class="list-unstyled greylinks">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <li class="media">
                                                <a href="{{url('/')}}"> Home </a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('about')}}"> About Us </a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('products')}}">Products</a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('ventures')}}"> Ventures </a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('capital')}}"> Capital </a>
                                            </li>
                                        </div>
                                        <div class="col-xs-6">
                                            <li class="media">
                                                <a href="{{url('content')}}"> Content </a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('/services')}}"> Services </a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('/merchandise')}}"> Merchandise </a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('/faq')}}"> Help</a>
                                            </li>
                                            <li class="media">
                                                <a href="{{url('/contact')}}"> Contact Us </a>
                                            </li>
                                           
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="col-md-4 col-sm-6 col-xs-12 text-center"><div class="widget widget_text"><h3 class="widget-title">Our Contacts</h3><ul class="list-unstyled greylinks"><li class="media"><i class="fa fa-map-marker highlight rightpadding_5" aria-hidden="true"></i>  73 Harvey Forest Suite, Singapore </li><li class="media"><a href="tel:+919577119900"><i class="fa fa-phone highlight rightpadding_5" aria-hidden="true"></i>
										+91-95-7711-9900 </a></li><li class="media"><i class="fa fa-pencil highlight rightpadding_5" aria-hidden="true"></i><a href="mailto:H-Love@Hollistic.org" > H-Love@Hollistic.org</a></li><li class="media"><i class="fa fa-clock-o highlight rightpadding_5" aria-hidden="true"></i> Mon-Fri: 9:00-19:00, Sat: 10:00-17:00 </li></ul></div></div> -->
                        <!-- <div class="col-md-3 col-sm-6 col-xs-12 hidden-xs hidden-sm text-center"><div class="widget widget_recent_posts"><h3 class="widget-title">Recent Blogs</h3><ul class="list-unstyled greylinks"><li><p><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</a></p><div class="entry-meta inline-content greylinks"><span><i class="fa fa-calendar highlight3 rightpadding_5" aria-hidden="true"></i><a href="#"><time datetime="2017-10-03T08:50:40+00:00">
										17 jan, 2018</time></a></span><span><i class="fa fa-user highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">Admin</a></span><span class="categories-links"><i class="fa fa-tags highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">lgbt</a></span></div></li><li><p><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></p><div class="entry-meta inline-content greylinks"><span><i class="fa fa-calendar highlight3 rightpadding_5" aria-hidden="true"></i><a href="#"><time datetime="2017-10-03T08:50:40+00:00">
										17 jan, 2018</time></a></span><span><i class="fa fa-user highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">Admin</a></span><span class="categories-links"><i class="fa fa-tags highlight3 rightpadding_5" aria-hidden="true"></i><a href="#">Serivces</a></span></div></li></ul></div></div> -->
                    </div>
                </div>
            </footer>
            <section class="ds ms parallax page_copyright overlay_color section_padding_top_30 section_padding_bottom_30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p style="color: #fff;font-size: 15px;"> Copyright &copy; 2015-2019
                                <a href="{{url('/')}}" style="color: #fff" target="_blank">Hollistic.org</a>. All Rights Reserved.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- eof #box_wrapper -->
    </div>
    <div class="modal fade" id="hollisticModal" tabindex="-1" role="dialog" aria-labelledby="hollisticModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hollistic</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Any and all information displayed here is copyright of Hollistic® unless another source or copyright-holder is cited. If any information has been addressed or sent to any person or organisation, it may be of a confidential nature and intended solely for the addressee(s). Any disclosure, copying, or distribution of the contents of the information in any form is strictly prohibited and is unlawful. The information provided herein by Hollistic does not constitute legal, professional or commercial advice, whatsoever. While every care has been taken to ensure that the content is useful and accurate, Hollistic gives no warranty, guarantee, undertaking in this respect, and does not accept any legal liability or responsibility for the content or the accuracy of the information so provided, or, for any loss or damage caused arising directly or indirectly in connection with reliance on the use of such information.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary modal-close-btn" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- eof #canvas -->

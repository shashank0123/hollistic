@extends('layouts.app')

@section('page', 'Login')

@section('content')
<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2>User Login</h2>
					<ol class="breadcrumb greylinks">
						<li> <a href="{{url('/')}}">
							Home
						</a> </li>
						<!-- <li> <a href="#">Pages</a> </li> -->
						<li class="active"><a href="{{url('/login')}}"> Login </a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>

	<section id="contact" class="ls section_padding_top_100" style="padding-bottom: 100px;">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 bottommargin_0 to_animate animated fadeInUp" data-animation="fadeInUp">
					<div class="ds bg_teaser with_padding big-padding" style="background-image: url(&quot;images/help-form.jpg&quot;);"> <img src="images/help-form.jpg" alt="">
						<div class="row columns_padding_30">
							<div class="col-xs-12 col-sm-9 col-md-7 col-lg-6 col-sm-offset-3 col-md-offset-5 col-lg-offset-6">
								<h2 class="section_header color3">Login Here.</h2>
								<form class="columns_padding_10" method="post" action="{{url('/login')}}">
                  @csrf
									<div class="row">
                    <div class="col-sm-12">
  										<div class="form-group bottommargin_0">
  											<label for="email">Email address</label>
  											<!-- <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email Address" required> -->
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
  										</div>
  									</div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
  										<div class="form-group bottommargin_0">
  											<label for="name">Password</label>
  											<!-- <input type="password" aria-required="true" size="30" value="" name="password" id="name" class="form-control" placeholder="Password" required> -->
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
  										</div>
  									</div>
                  </div>

									<div class="col-sm-12" style="margin-top: 0px;margin-bottom: 0px;">
										<div class="checkbox">
											<label>
												<!-- <input type="checkbox"> <small>Remember Me </small> -->
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> <small>Remember Me </small>
											</label>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="contact-form-submit">
											<button type="submit" style="width: 100%;" id="" name="contact_submit" class="theme_button color3 min_width_button margin_0">Login</button>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="text-center">
											<a href="#">Forgot Your Password?</a>
                      @if (Route::has('password.request'))
                          <!-- <a  href="{{ route('password.request') }}">
                              {{ __('Forgot Your Password?') }}
                          </a> -->
                      @endif
										</div>
									</div>
									<div class="col-sm-12 bottommargin_0">
										<div class="loginSignUpSeparator customloginSignUpSeparator"><span class="textInSeparator"><b>or</b></span></div>
									</div>
									<div class="col-sm-12 bottommargin_0">
										<div class="contact-form-submit topmargin_10">
											<a type="submit" href="{{url('register')}}" style="width: 100%;" id="contact_form_submit" name="contact_submit" class="theme_button color3 min_width_button margin_0">Sign Up</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- validation -->
	<script type="text/javascript" src="{{asset('assets/js/validation.js')}}"></script>
@endsection

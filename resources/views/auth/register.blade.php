@extends('layouts.app')

@section('page', 'Register')

@section('content')

<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2>User registration</h2>
        <ol class="breadcrumb greylinks">
          <li> <a href="{{url('/')}}">
            Home
          </a> </li>
          <!-- <li> <a href="#">Pages</a> </li> -->
          <li class="active"><a href="{{url('/register')}}"> registration </a></li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section id="contact" class="ls section_padding_top_100" style="padding-bottom: 100px;">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 bottommargin_0 to_animate animated fadeInUp" data-animation="fadeInUp">
        <div class="ds bg_teaser with_padding big-padding" style="background-image: url(&quot;images/help-form.jpg&quot;);"> <img src="images/help-form.jpg" alt="">
          <div class="row columns_padding_30">
            <div class="col-xs-12 col-sm-9 col-md-7 col-lg-6 col-sm-offset-3 col-md-offset-5 col-lg-offset-6">
              <h2 class="section_header color3">Register Here.</h2>
              <form class="columns_padding_10" method="post" action="{{ route('register') }}">
                  @csrf
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group bottommargin_0">
                      <label for="first_name">First Name <span class="required">*</span></label>
                      <!-- <input type="text" aria-required="true" size="30" value="" name="fname" id="fname" class="form-control" placeholder="First Name*" pattern="^[a-zA-Z\s]*$" required> -->
                      <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="First Name*" pattern="^[a-zA-Z\s]*$" required autocomplete="first_name" autofocus>

                      @error('first_name')
                          <span class="invalid-feedback" role="alert">
                              {{ $message }}
                          </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group bottommargin_0">
                      <label for="last_name">Last Name <span class="required">*</span></label>
                      <!-- <input type="text" aria-required="true" size="30" value="" name="lname" id="lname" class="form-control" placeholder="Last Name*" pattern="^[a-zA-Z\s]*$" required> -->
                      <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name*" pattern="^[a-zA-Z\s]*$" required autocomplete="last_name" autofocus>

                      @error('last_name')
                          <span class="invalid-feedback" role="alert">
                              {{ $message }}
                          </span>
                      @enderror
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group bottommargin_0">
                      <label for="email">Email address<span class="required">*</span></label>
                      <!-- <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email Address*" pattern="^.+@[^\.].*\.[a-z]{2,}$" required> -->
                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address*" pattern="^.+@[^\.].*\.[a-z]{2,}$" required autocomplete="email">

                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              {{ $message }}
                          </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group bottommargin_0">
                      <label for="phone">Phone No.<span class="required">*</span></label>
                      <!-- <input type="text" aria-required="true" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Phone No*" pattern="^\d+$" maxlength="10" required> -->
                      <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone No*" pattern="^\d+$" maxlength="10" required autocomplete="phone">

                      @error('phone')
                          <span class="invalid-feedback" role="alert">
                              {{ $message }}
                          </span>
                      @enderror
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group bottommargin_0">
                      <label for="name">Password<span class="required">*</span></label>
                      <!-- <input type="password" aria-required="true" size="30" value="" name="password" id="password" class="form-control" placeholder="Password" pattern="^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$" required > -->
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" pattern="^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$" required autocomplete="new-password">

                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              {{ $message }}
                          </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group bottommargin_0">
                      <label for="cpassword">Confirm Password<span class="required">*</span></label>
                      <!-- <input type="password" aria-required="true" size="30" value="" name="cpassword" id="cpassword" class="form-control" placeholder="Confirm Password" required> -->
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
                    </div>
                  </div>
                </div>

                <div class="col-sm-12" style="margin-top: 0px;margin-bottom: 0px;">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" required checked> <small>I agree with the website <a href="#" target="_blank"> user agreement</a> and the <a href="#" target="_blank">privacy policy</a>.</small>
                    </label>
                  </div>
                </div>

                <div class="col-sm-12 bottommargin_0">
                  <div class="contact-form-submit">
                    <button type="submit"  style="width: 100%;" id="registration_submit" name="contact_submit" class="theme_button color3 min_width_button margin_0 btn-submit">SignUp</button>
                  </div>
                </div>
                <div class="col-sm-12 bottommargin_0">
                  <div class="loginSignUpSeparator"><span class="textInSeparator"><b>or</b></span></div>
                </div>
                <div class="col-sm-12 bottommargin_0">
                  <div class="contact-form-submit topmargin_10">
                    <a type="submit" href="{{url('/login')}}" style="width: 100%;" id="contact_form_submit" name="contact_submit" class="theme_button color3 min_width_button margin_0 ">LogIn</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- validation -->
<script type="text/javascript" src="{{asset('assets/js/validation.js')}}"></script>

@endsection

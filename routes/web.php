<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index');
Route::get('/about', 'SiteController@getAbout');
Route::get('/programs', 'SiteController@getPrograms');
Route::get('/client-programs', 'SiteController@getClientPrograms');  
Route::get('/partner-programs', 'SiteController@getPartnerPrograms');    
Route::get('/ventures', 'SiteController@getVentures');
Route::get('/capital', 'SiteController@getCapital');
Route::get('/content','SiteController@getContent');
Route::get('/services', 'SiteController@getServices'); 
Route::get('/products', 'SiteController@getProducts');
Route::get('/merchandise', 'SiteController@getMerchandise');
Route::get('/faq', 'SiteController@getFaq');
Route::get('/contact', 'SiteController@getContact');
Route::get('/cart', 'SiteController@getCart');

Route::resource('/subscribe', 'SubscribeController');


//Authenticate
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function() {

Route::get('/home', 'HomeController@indexAdmin')->name('home');
Route::post('/ajaxData', 'Admin\Sitecontroller@getCities');

//For Programs
Route::get('/programs-list', 'Admin\ProgramController@getProgramsList');
Route::get('/add-program', 'Admin\ProgramController@addProgram');
Route::post('/add-program-success', 'Admin\ProgramController@createProgram');
Route::get('/edit-program/{id}', 'Admin\ProgramController@editProgram');
Route::post('/edit-program/{id}', 'Admin\ProgramController@updateProgram');
Route::get('/programs-list/{id}', 'Admin\ProgramController@deleteProgram');

Route::get('/searched-programs', 'Admin\ProgramController@getSearchedProgram');

//For Marchantise Product
Route::get('/products-list', 'Admin\MerchandiseController@getProductsList');
Route::get('/add-product', 'Admin\MerchandiseController@addProduct');
Route::post('/add-product-success', 'Admin\MerchandiseController@createProduct');
Route::get('/edit-product/{id}', 'Admin\MerchandiseController@editProduct');
Route::post('/edit-product/{id}', 'Admin\MerchandiseController@updateProduct');
Route::get('/products-list/{id}', 'Admin\MerchandiseController@deleteProduct');

Route::get('/searched-products', 'Admin\MerchandiseController@getSearchedProduct');

//For Product Category
Route::get('/categories-list', 'Admin\CategoryController@getCategoriesList');
Route::get('/add-category', 'Admin\CategoryController@addCategory');
Route::post('/add-category-success', 'Admin\CategoryController@createCategory');
Route::get('/edit-category/{id}', 'Admin\CategoryController@editCategory');
Route::post('/edit-category/{id}', 'Admin\CategoryController@updateCategory');
Route::get('/categories-list/{id}', 'Admin\CategoryController@deleteCategory');

Route::get('/searched-categories', 'Admin\CategoryController@getSearchedCategory');

//For Seo
Route::get('/seos-list', 'Admin\SeoController@getSeosList');
Route::get('/add-seo', 'Admin\SeoController@addSeo');
Route::post('/add-seo-success', 'Admin\SeoController@createSeo');
Route::get('/edit-seo/{id}', 'Admin\SeoController@editSeo');
Route::post('/edit-seo/{id}', 'Admin\SeoController@updateSeo');
Route::get('/seos-list/{id}', 'Admin\SeoController@deleteSeo');

Route::get('/searched-seos', 'Admin\SeoController@getSearchedSeo');


//For Banner Images
Route::get('/images-list', 'Admin\BannerController@getImagesList');
Route::get('/add-image', 'Admin\BannerController@addImage');
Route::post('/add-image-success', 'Admin\BannerController@createImage');
Route::get('/edit-image/{id}', 'Admin\BannerController@editImage');
Route::post('/edit-image/{id}', 'Admin\BannerController@updateImage');
Route::get('/images-list/{id}', 'Admin\BannerController@deleteImage');

Route::get('/searched-images', 'Admin\BannerController@getSearchedImage');

//For FAQS
Route::get('/faqs-list', 'Admin\FaqController@getFaqsList');
Route::get('/add-faq', 'Admin\FaqController@addFaq');
Route::post('/add-faq-success', 'Admin\FaqController@createFaq');
Route::get('/edit-faq/{id}', 'Admin\FaqController@editFaq');
Route::post('/edit-faq/{id}', 'Admin\FaqController@updateFaq');
Route::get('/faqs-list/{id}', 'Admin\FaqController@deleteFaq');

Route::get('/searched-faqs', 'Admin\FaqController@getSearchedFaq');

//For Testimonials
Route::get('/testimonials-list', 'Admin\TestimonialController@getTestimonialsList');
Route::get('/add-testimonial', 'Admin\TestimonialController@addTestimonial');
Route::post('/add-testimonial-success', 'Admin\TestimonialController@createTestimonial');
Route::get('/edit-testimonial/{id}', 'Admin\TestimonialController@editTestimonial');
Route::post('/edit-testimonial/{id}', 'Admin\TestimonialController@updateTestimonial');
Route::get('/testimonials-list/{id}', 'Admin\TestimonialController@deleteTestimonial');

Route::get('/searched-testimonials', 'Admin\TestimonialController@getSearchedTestimonial');


//For Team Members
Route::get('/members-list', 'Admin\OurTeamController@getMembersList');
Route::get('/add-member', 'Admin\OurTeamController@addMember');
Route::post('/add-member-success', 'Admin\OurTeamController@createMember');
Route::get('/edit-member/{id}', 'Admin\OurTeamController@editMember');
Route::post('/edit-member/{id}', 'Admin\OurTeamController@updateMember');
Route::get('/members-list/{id}', 'Admin\OurTeamController@deleteMember');

Route::get('/searched-member', 'Admin\OurTeamController@getSearchedMember');


//Other Pages
Route::get('/newsletters-list','Admin\OtherPagesController@getNewslettersList');
Route::get('/searched-newsletters', 'Admin\OtherPagesController@getSearchedNewsletter');
Route::get('/newsletters-list/{id}','Admin\OtherPagesController@deleteNewsletter');

Route::get('/contacts-list','Admin\OtherPagesController@getContactsList');
Route::get('/searched-contacts', 'Admin\OtherPagesController@getSearchedContact');
Route::get('/contatcs-list/{id}','Admin\OtherPagesController@deleteContact');


Route::get('/pages-list', 'Admin\CmsController@getAll');
Route::get('/edit-content/{id}', 'Admin\CmsController@editContent');
Route::post('/edit-content/{id}', 'Admin\CmsController@updateContent');

Route::get('/sendsms', 'Admin\OtherPagesController@showsendsms');
Route::post('/send-sms', 'Admin\OtherPagesController@sendsms');

});

// Route::post('/send-sms', 'Admin\OtherPagesController@sendsms');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/my-programs', 'HomeController@MyPrograms')->name('my-programs');
Route::get('/settings', 'HomeController@Settings')->name('settings');
Route::get('/program-documents', 'HomeController@ProgramDocuments')->name('program-documents');

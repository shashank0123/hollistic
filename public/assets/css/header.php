<?php session_start(); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->


<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
	<title>Hollistic: A Global Life Improvement Ecosystem | <?= $page ?></title>

	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/favicons/favicon.png">
	<!-- <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon.png"> -->
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7CLora:400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animations.css">
	<link rel="stylesheet" href="css/fonts.css">
	<!-- <link rel="stylesheet" href="css/style.css"> -->
	<link rel="stylesheet" href="css/main.css" class="color-switcher-link">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	<link rel="stylesheet" href="css/shop.css" class="color-switcher-link">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/custom.css"/>
	<link href="css/lightbox.css" rel="stylesheet">

<!-- <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/> -->
	<!--[if lt IE 9]>
		<script src="js/vendor/html5shiv.min.js"></script>
		<script src="js/vendor/respond.min.js"></script>
		<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->
</head>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
	<div class="preloader">
		<div class="preloader_image"></div>
	</div>
	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">
			<i class="rt-icon2-cross2"></i>
		</span>
	</button>
		<div class="widget widget_search">
			<form method="get" class="searchform search-form form-inline" action="http://webdesign-finder.com/html/diversify/">
				<div class="form-group bottommargin_0"> <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input"> </div> <button type="submit" class="theme_button no_bg_button">Search</button> </form>
		</div>
	</div>
	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls with_padding">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->
		</div>
	</div>
	<!-- eof .modal -->
	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">
			<!-- template sections -->
			<section style="background-color: #000;" class="page_topline ds table_section table_section_lg section_padding_top_15 section_padding_bottom_15 columns_margin_0">
				<div class="container-fluid responsive-header-section">
					<div class="row">
						<div class="col-lg-6 text-center text-lg-left hidden-xs">
							<div class="inline-content big-spacing">
								<div class="page_social">
								    <a class="social-icon socicon-youtube" target="_blank" href="https://www.youtube.com/channel/UCJzsP1_r3YoNVNdf06n4kXw" title="Youtube"></a>
								    <a class="social-icon socicon-blogger" target="_blank" href="https://www.blogger.com/u/2/blogger.g#welcome" title="blogger"></a>
								    <a class="social-icon socicon-vimeo" target="_blank" href="https://vimeo.com/hollistic" title="vimeo"></a>
								    <a class="social-icon socicon-instagram" target="_blank" href="https://www.instagram.com/hollisticmedia/" title="instagram"></a>
								    <a class="social-icon socicon-facebook" target="_blank" href="https://www.facebook.com/hollisticmedia" title="Facebook"></a>
								    <a class="social-icon socicon-pinterest" target="_blank" href="https://in.pinterest.com/hollisticmedia/" title="pinterest"></a>
								    <a class="social-icon socicon-twitter" target="_blank" href="https://twitter.com/hollisticmedia" title="Twitter"></a>
								    <a class="social-icon socicon-tumblr" target="_blank" href="https://www.tumblr.com/blog/hollisticmedia" title="tumblr"></a>
								    <a class="social-icon socicon-snapchat" target="_blank" href="https://www.snapchat.com/add/hollisticmedia" title="snapchat"></a>
								    <a class="social-icon socicon-reddit" target="_blank" href="https://www.reddit.com/user/hollisticmedia" title="reddit"></a>
								    <a class="social-icon socicon-quora" target="_blank" href="https://www.quora.com/profile/Hollistic-Media" title="quora"></a>
								    <!--<a class="social-icon socicon-buzzfeed" href="#" title="buzzfeed"></a>-->
								    <a class="social-icon socicon-flickr" href="https://www.flickr.com/photos/170872704@N04/" title="flickr"></a>



								</div>
								<!-- <span class="xs-block">
									<i class="fa fa-phone" aria-hidden="true"></i>
									+91-95-7711-9900
								</span> -->
					</div>
						</div>
						<div class="col-lg-6 text-center text-lg-right">
							<div id="topline-animation-wrap">
								<div id="" class="inline-content big-spacing"> <span class="hidden-xs">
							<!-- <i class="fa fa-map-marker highlight3 rightpadding_5" aria-hidden="true"></i>
							73 Harvey Forest Suite, Singapore -->
						</span>
						<!-- <span class="greylinks xs-block">
							<i class="fa fa-phone highlight3 rightpadding_5" aria-hidden="true"></i>
							<a href="tel:+919577119900">+91-95-7711-9900</a>
						</span> -->
						<!-- <span class="greylinks xs-block">
							<i class="fa fa-pencil highlight3 rightpadding_5" aria-hidden="true"></i>
							<a href="mailto:H-Love@Hollistic.org">H-Love@Hollistic.org</a>
						</span> -->
						<div class="xs-block">
							<ul class="inline-list menu greylinks">
								<!-- <li class="dropdown">
									<a id="search-show" href="index.php">
										<i class="fa fa-search" style="font-size: 18px;"></i>
									</a>
								</li> -->
								<li>
									<?php if(isset($_SESSION['loggedin'])) : ?>
										<a href="account.php" style="text-color: #fff;font-weight: bold;font-size: 14px;">
											<i class="fa fa-user" aria-hidden="true"  style="font-size: 16px;"></i> ACCOUNT
										</a>
									<?php else : ?>
										<a href="login.php" style="text-color: #fff;font-weight: bold;font-size: 14px;">
										<i class="fa fa-user" aria-hidden="true"  style="font-size: 16px;"></i> LOGIN
									</a>
									<?php endif; ?>
								</li>
								<!-- <li>
									<a href="shop-cart.php">
										<i class="fa fa-shopping-cart" aria-hidden="true"  style="font-size: 18px;"></i>
									</a>
								</li> -->
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<header class="page_header header_white toggler_xs_right columns_margin_0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 display_table">
				<div class="header_left_logo display_table_cell">
					<a href="index.php" class="logo logo_with_text">
						<img src="images/logo1.jpg" alt="">
						<span class="logo_text">
							<span>Hollistic<sup style="font-size: 11px;top: -1.5em;">®</sup></span>
							<small style="color: #8B0000;
    border-color: #8B0000; font-weight: bold; font-weight:700; margin-left:-6%;">Life improvement</small>
						</span>
					</a>
				</div>
				<div class="header_mainmenu display_table_cell text-center">
					<!-- main nav start -->
					<nav class="mainmenu_wrapper">
						<ul class="mainmenu nav sf-menu">
							<li class="responsive-social-icons">
								<a class="social-icon socicon-youtube" style="display: inline-block;padding: 0px 0;" href="#" title="youtube" target="blank"></a>
								<a class="social-icon socicon-blogger" style="display: inline-block;padding: 0px 0;" href="https://www.blogger.com/u/2/blogger.g#welcome" target="blank"></a>
								<a class="social-icon socicon-vimeo" style="display: inline-block;padding: 0px 0;" href="https://vimeo.com/hollistic" title="Google Plus" target="blank"></a>
								<a class="social-icon socicon-instagram" style="display: inline-block;padding: 0px 0;" href="https://www.instagram.com/hollisticmedia/" title="Linkedin" target="blank"></a>
								<a class="social-icon socicon-facebook" style="display: inline-block;padding: 0px 0;" href="https://www.facebook.com/hollisticmedia" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-pinterest" style="display: inline-block;padding: 0px 0;" href="https://in.pinterest.com/hollisticmedia/" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-twitter" style="display: inline-block;padding: 0px 0;" href="https://twitter.com/hollisticmedia" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-tumblr" style="display: inline-block;padding: 0px 0;" href="https://www.tumblr.com/blog/hollisticmedia" title="Youtube" target="blank"></a>
								<a class="social-icon socicon-snapchat" style="display: inline-block;padding: 0px 0;" href="https://www.snapchat.com/add/hollisticmedia" title="snapchat" target="blank"></a>
								<a class="social-icon socicon-reddit" style="display: inline-block;padding: 0px 0;" href="https://www.reddit.com/user/hollisticmedia" title="reddit" target="blank"></a>
								<a class="social-icon socicon-quora" style="display: inline-block;padding: 0px 0;" href="#" title="quora" target="blank"></a>

							</li><br>
							<li class="active">
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="about.php">About Us</a>
							</li>
							<!-- eof pages -->
							<li>
								<a href="programs.php">Coaching</a>
								<ul>
									<li>
										<a href="client-programs.php">Client Programs</a>
									</li>
									<li>
										<a href="partner-program.php">Partner Programs</a>
									</li>

								</ul>
							</li>
							<li>
										<a href="ventures.php">Ventures</a>
									</li>
									<li>
								<a href="capital.php">Capital</a>
							</li>

							<li>
								<a href="content.php">Content</a>
							</li>
							<li>
								<a href="schedule.php">Schedule</a>
							</li>

							<li>
								<a href="merchandise.php">Merchandise</a>
							</li>
							<li>
								<a href="faq.php">Help</a>
							</li>
							<li>
								<a href="contact.php">Contact Us</a>
							</li>
							<!-- <li style="margin-bottom: 15px;margin-top: 7px;">
								<a href="#" class="theme_button color2 margin_0 connect-withus-btn" data-toggle="modal" data-target="#registerModal">Connect With Us</a>
							</li> -->
							
						</ul>
					</nav>

					<span class="toggle_menu"><span>
					</span>
				</span>
			</div>
			<!-- <div class="header_right_buttons display_table_cell text-right hidden-xs">
				<a href="#" class="theme_button color2 margin_0" data-toggle="modal" data-target="#contentModal">Connect With Us</a>
			</div> -->
		</div>
	</div>
</div>
</header>
<div class="serach-bar-responsive">
	<div id="topline-show" class="widget widget_search">
		<form method="get" class="searchform" action="http://webdesign-finder.com/html/diversify/">
			<div class="form-group-wrap">
				<div class="form-group serach-input-responsive">
					<label class="sr-only" for="topline-widget-search">Search for:</label>
					<input id="topline-widget-search" type="text" value="" name="search" class="form-control" placeholder="Enter Keyword...">
					<a href="index.php" id="search-close">
						<i class="fa fa-close close-icon-btn"></i>
					</a>
				</div>
				<button type="submit" class="theme_button no_bg_button color1">ok</button>
			</div>
		</form>
	</div>
</div>

<?php
	include('modal.php');
	?>
<?php
	include('header.php');
	?>

	<section class="page_breadcrumbs ds background_cover section_padding_top_65 section_padding_bottom_65">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2>Contact</h2>
							<ol class="breadcrumb greylinks">
								<li> <a href="index.php">
							Home
						</a> </li>
								<!-- <li> <a href="#">Pages</a> </li> -->
								<li class="active">Contact</li>
							</ol>
						</div>
					</div>
				</div>
			</section>
			<section class="ls columns_padding_25 section_padding_top_100 section_padding_bottom_110">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h4>Contact Form</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 to_animate" data-animation="scaleAppear">
							<form class="contact-form columns_padding_5" method="post" action="http://webdesign-finder.com/html/diversify/">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group bottommargin_0"> <label for="name">Full Name <span class="required">*</span></label> <i class="fa fa-user highlight" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name">											</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group bottommargin_0"> <label for="phone">Phone Number<span class="required">*</span></label> <i class="fa fa-phone highlight" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Phone Number">											</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group bottommargin_0"> <label for="email">Email address<span class="required">*</span></label> <i class="fa fa-envelope highlight" aria-hidden="true"></i> <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email Address">											</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group bottommargin_0"> <label for="subject">Subject<span class="required">*</span></label> <i class="fa fa-flag highlight" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">											</div>
									</div>
									<div class="col-sm-12">
										<div class="contact-form-message form-group bottommargin_0"> <label for="message">Message</label> <i class="fa fa-comment highlight" aria-hidden="true"></i> <textarea aria-required="true" rows="3" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea> </div>
									</div>
									<div class="col-sm-12 bottommargin_0">
										<div class="contact-form-submit topmargin_30"> 
											<button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1 margin_0">Send message</button> 
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="col-md-4 to_animate" data-animation="scaleAppear">
							<ul class="list1 no-bullets no-top-border no-bottom-border">
								<li>
									<div class="media">
										<div class="media-left"> <i class="rt-icon2-shop highlight fontsize_18"></i> </div>
										<div class="media-body">
											<h6 class="media-heading grey">Coaching Related Queries:</h6> Lorem ipsum dolor sit amet consectetur adipisicing elit. </div>
									</div>
								</li>
								<li>
									<div class="media">
										<div class="media-left"> <i class="rt-icon2-phone5 highlight fontsize_18"></i> </div>
										<div class="media-body">
											<h6 class="media-heading grey">Phone:</h6> +91-9920562563 </div>
									</div>
								</li>
								<!-- <li>
									<div class="media">
										<div class="media-left"> <i class="rt-icon2-stack4 highlight fontsize_18"></i> </div>
										<div class="media-body">
											<h6 class="media-heading grey">Fax:</h6> 8(800) 123-12345 </div>
									</div>
								</li> -->
								<li>
									<div class="media">
										<div class="media-left"> <i class="rt-icon2-mail highlight fontsize_18"></i> </div>
										<div class="media-body greylinks">
											<h6 class="media-heading grey">Email:</h6> <a href="#">Business@Hollistic.org.</a> </div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>

	<?php
	include('footer.php');
	?>
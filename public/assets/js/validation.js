$(":input").keyup(function(event) {
    // remove backend error
    $('.invalid-feedback').html('');
    // validate regex if exist
    if (this.pattern) {
        var fieldId = this.id;
        var value = this.value;
        var pattern = this.pattern;
        var pattern = new RegExp(this.pattern);
        var re = pattern.test(this.value);
        // var re = value.match(pattern);
        if (!re) {
            $('#' + fieldId + '_error').removeClass('d-none');
            if (document.getElementById(fieldId + '_error')) {
                $('#' + fieldId + '_error').removeClass('d-none');
                $('#' + fieldId + '_error').html('This value is not valid');
                $('.btn-submit').prop('disabled', true);
            } else {
                $('#' + fieldId).after('<small id="' + fieldId + '_error" class="text-red">This value is not valid</small>');
                $('.btn-submit').prop('disabled', true);
            }
        } else {
            $('#' + fieldId + '_error').addClass('d-none');
            $('.btn-submit').prop('disabled', false)
        }
    }
});

function dateCheck(form) {
    // password and Confirm password matching
    // $('#password, #cpassword').on('keyup', function () {
    if ($('#password').val() == $('#cpassword').val()) {
        $('#btn-submit').attr('disabled', false);
        $('#message').html('').css('color', 'green');
        return true;
    } else {
        $('#btn-submit').attr('disabled', true);
        $('#message').html('Password mismatch').css('color', 'red');
        form.preventDefault();
        return false;
    }
    // });
}
$('#registration_submit').on('submit', function(e) {
    e.preventDefault(e);

    event.preventDefault();

    var formEl = $(this);
    var submitButton = $('input[type=submit]', formEl);

    $.ajax({
        type: 'POST',
        // url: formEl.prop('action'),
        url: 'controllers/register.php',
        accept: {
            javascript: 'application/javascript'
        },
        data: formEl.serialize(),
        beforeSend: function() {
            submitButton.prop('disabled', 'disabled');
        }
    }).done(function(data) {
        submitButton.prop('disabled', false);
    });

});
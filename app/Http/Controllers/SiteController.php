<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;

class SiteController extends Controller
{
    public function index(){
    	return view('hollistic.index');
    } 

    public function getAbout(){
    	return view('hollistic.about');
    }

    public function getPrograms(){
    	return view('hollistic.programs');
    }

    public function getClientPrograms(){
    	return view('hollistic.client-programs');
    }

    public function getPartnerPrograms(){
    	return view('hollistic.partner-programs');
    }

    public function getVentures(){
    	return view('hollistic.ventures');
    }

    public function getCapital(){
    	return view('hollistic.capital');
    }

    public function getContent(){
    	return view('hollistic.content');
    }

    public function getServices(){
    	return view('hollistic.services');
    }

    public function getProducts(){
    	return view('hollistic.products');
    }

    public function getMerchandise(){
    	return view('hollistic.merchandise');
    }

    public function getFaq(){
    	$faqs = Faq::where('status','Active')->get();
    	return view('hollistic.faq',compact('faqs'));
    }

    public function getContact(){
    	return view('hollistic.contact');
    }

    public function getCart(){
        return view('hollistic.cart');
    }
}

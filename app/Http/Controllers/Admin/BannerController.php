<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    public function getImagesList(){
    $images = Banner::orderBy('created_at','DESC')->get();
    return view('admin.images.allimages',compact('images'));
  }

  public function addImage(){
    return view('admin.images.add-image');

  }

  public function createImage(Request $request){
    $image = new Banner;

        // for image_url
        if ($request->hasfile('image_url')) {
           $file=$request->file('image_url');
           $file->move(public_path(). '/banner-images/', time().$file->getClientOriginalName());
           $image->image_url=time().$file->getClientOriginalName();           
        }
       
        $image->title = $request->title;
        $image->text = $request->text;
        $image->status = $request->status;
        $image->save();
       
        return redirect()->back()->with('message','Data Inserted Successfully');

  }

  public function editImage($id){
    $image = Banner::find($id);
    return view('admin.images.edit-image',compact('image'));

  }

  public function updateImage(Request $request,$id){

    $image = Banner::find($id);
    
     // for image_url
        if ($request->hasfile('image_url')) {
           $file=$request->file('image_url');
           $file->move(public_path(). '/banner-images/', $file->getClientOriginalName());
           $image->image_url=$file->getClientOriginalName();           
        }

        $image->title = $request->title;
        $image->text = $request->text;
        $image->status = $request->status;
        $image->update();

        return redirect()->back()->with('message','Details Updated Successfully');

  }

  public function deleteImage($id){
    $image = Banner::find($id);
        $image->delete();

        return redirect()->back()->with('message','Record Deleted Successfully');
  }

  public function getSearchedImage(Request $request){
    $key = $request->keyword;
        $images = Banner::where('status','LIKE','%' .$key. '%')->orWhere('image_category',$key)->get();
        return view('admin.images.allimages',compact('images'));
  }
}

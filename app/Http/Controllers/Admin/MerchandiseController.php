<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MarchantisProduct;

class MerchandiseController extends Controller
{
    public function getProductsList(){
		$products = MarchantisProduct::orderBy('created_at','DESC')->get();
		return view('admin.merchandise.allproducts',compact('products'));
	}

	public function addProduct(){
		return view('admin.merchandise.add-product');
	}

	public function createProduct(Request $request){
		$product = new MarchantisProduct;

        // for image_url
        if ($request->hasfile('image_url')) {
           $file=$request->file('image_url');
           $file->move(public_path(). '/products-img/', time().$file->getClientOriginalName());
           $product->image_url=time().$file->getClientOriginalName();           
        }
        $product->title = $request->title;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->duration = $request->duration;
        $product->price_per_person = $request->price_per_person;        
        $product->slug = strtolower(str_replace(' ', '-', $request->title));
        $product->status = $request->status;
        $product->save();
       
        return redirect()->back()->with('message','Data Inserted Successfully');

	}

	public function editProduct($id){
		$product = MarchantisProduct::find($id);
		return view('admin.merchandise.edit-product',compact('product'));

	}

	public function updateProduct(Request $request,$id){

		$product = MarchantisProduct::find($id);
		
		 // for image_url
        if ($request->hasfile('image_url')) {
           $file=$request->file('image_url');
           $file->move(public_path(). '/products-img/', $file->getClientOriginalName());
           $product->image_url=$file->getClientOriginalName();           
        }
        $product->title = $request->title;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->duration = $request->duration;        
        $product->slug = strtolower(str_replace(' ', '-', $request->title));
        $product->price_per_person = $request->price_per_person;  
        $product->status = $request->status;
        $product->update();

        return redirect()->back()->with('message','Details Updated Successfully');

	}

	public function deleteProduct($id){
		$product = MarchantisProduct::find($id);
        $product->delete();

        return redirect()->back()->with('message','Record Deleted Successfully');
	}

    public function getSearchedProduct(Request $request){
        $product = $request->keyword;

        $products = MarchantisProduct::where('title','LIKE','%' .$product. '%')->orWhere('short_description','LIKE','%' .$product. '%')->orWhere('description','LIKE','%' .$product. '%')->orWhere('status','LIKE','%' .$product. '%')->orWhere('created_at','LIKE','%' .$product. '%')->get();
        
        return view('admin.merchandise.allproducts',compact('products'));
    }
}

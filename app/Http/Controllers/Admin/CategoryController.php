<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Catgory;

class CategoryController extends Controller
{
    //Category
	public function getCategoriesList(){
		$categories = Catgory::orderBy('created_at','DESC')->get();
		return view('admin.merchandise.allcategories',compact('categories'));
	}

	public function addCategory(){
		return view('admin.merchandise.add-category');
	}

	public function createCategory(Request $request){
		$category = new Catgory;
		$type = "";

		if($request->base == 'Product'){
		$type = "merchandise";
		}
		else{
		$type = "program";			
		}

        $category->category_name = $request->category_name;
        $category->category_type = $type;
        $category->base = $request->base;
        $category->slug = strtolower(str_replace(' ', '-', $request->category_name));
        $category->status = $request->status;
       
        $category->save();
       
        return redirect()->back()->with('message','Data Inserted Successfully');
	}

	public function editCategory($id){
		$category = Catgory::find($id);
		return view('admin.merchandise.edit-category',compact('category'));
	}

	public function updateCategory(Request $request,$id){

		$category = Catgory::find($id);
		if($request->base == 'Product'){
		$type = "merchandise";
		}
		else{
		$type = "program";			
		}

		$category->category_name = $request->category_name;
        $category->category_type = $type;
        $category->base = $request->base;
        $category->slug = strtolower(str_replace(' ', '-', $request->category_name));
        $category->status = $request->status;        
        $category->update();

        return redirect()->back()->with('message','Details Updated Successfully');
	}

	public function deleteCategory($id){
		$category = Catgory::find($id);
        $category->delete();

        return redirect()->back()->with('message','Record Deleted Successfully');
	}
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Input;
use Input;
use App\Programs;

class ProgramController extends Controller
{
    public function getProgramsList(){
		$programs = Programs::orderBy('created_at','DESC')->get();
		return view('admin.programs.allprograms',compact('programs'));
	}

	public function addProgram(){
		return view('admin.programs.add-program');
	}

	public function createProgram(Request $request){
		$program = new Programs;

        // for image_url
        if ($request->hasfile('image_url')) {
           $file=$request->file('image_url');
           $file->move(public_path(). '/programs-img/', time().$file->getClientOriginalName());
           $program->image_url=time().$file->getClientOriginalName();           
        }
        $program->title = $request->title;
        $program->short_description = $request->short_description;
        $program->description = $request->description;
        $program->duration = $request->duration;
        $program->price_per_person = $request->price_per_person;        
        $program->slug = strtolower(str_replace(' ', '-', $request->title));
        $program->status = $request->status;
        $program->save();
       
        return redirect()->back()->with('message','Data Inserted Successfully');

	}

	public function editProgram($id){
		$program = Programs::find($id);
		return view('admin.programs.edit-program',compact('program'));

	}

	public function updateProgram(Request $request,$id){

		$program = Programs::find($id);
		
		 // for image_url
        if ($request->hasfile('image_url')) {
           $file=$request->file('image_url');
           $file->move(public_path(). '/programs-img/', $file->getClientOriginalName());
           $program->image_url=$file->getClientOriginalName();           
        }
        $program->title = $request->title;
        $program->short_description = $request->short_description;
        $program->description = $request->description;
        $program->duration = $request->duration;        
        $program->slug = strtolower(str_replace(' ', '-', $request->title));
        $program->price_per_person = $request->price_per_person;  
        $program->status = $request->status;
        $program->update();

        return redirect()->back()->with('message','Details Updated Successfully');

	}

	public function deleteProgram($id){
		$program = Programs::find($id);
        $program->delete();

        return redirect()->back()->with('message','Record Deleted Successfully');
	}

    public function getSearchedProgram(Request $request){
        $program = $request->keyword;

        $programs = Programs::where('title','LIKE','%' .$program. '%')->orWhere('short_description','LIKE','%' .$program. '%')->orWhere('description','LIKE','%' .$program. '%')->orWhere('status','LIKE','%' .$program. '%')->orWhere('created_at','LIKE','%' .$program. '%')->get();
        
        return view('admin.programs.allprograms',compact('programs'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Seo;

class SeoController extends Controller
{
    public function getSeosList(){
		$seos = Seo::orderBy('created_at','DESC')->get();
		return view('admin.seo.allseos',compact('seos'));
	}

	public function addSeo(){
		return view('admin.seo.add-seo');
	}

	public function createSeo(Request $request){
		$seo = new Seo;

        $seo->title = $request->title;
        $seo->page_name = $request->page_name;
        $seo->keywords = $request->keywords;
        $seo->tags = $request->tags;
        $seo->meta_desc = $request->meta_desc;
        $seo->save();
       
        return redirect()->back()->with('message','Data Inserted Successfully');

	}

	public function editSeo($id){
		$seo = Seo::find($id);
		return view('admin.seo.edit-seo',compact('seo'));

	}

	public function updateSeo(Request $request,$id){

		$seo = Seo::find($id);
		
        $seo->title = $request->title;
        $seo->page_name = $request->page_name;
        $seo->keywords = $request->keywords;
        $seo->tags = $request->tags;
        $seo->meta_desc = $request->meta_desc;
        $seo->update();

        return redirect()->back()->with('message','Details Updated Successfully');

	}

	public function deleteSeo($id){
		$seo = Seo::find($id);
        $seo->delete();

        return redirect()->back()->with('message','Record Deleted Successfully');
	}

    public function getSearchedSeo(Request $request){
        $seo = $request->keyword;

        $seos = Seo::where('title','LIKE','%' .$seo. '%')->orWhere('short_description','LIKE','%' .$seo. '%')->orWhere('description','LIKE','%' .$seo. '%')->orWhere('status','LIKE','%' .$seo. '%')->orWhere('created_at','LIKE','%' .$seo. '%')->get();
        
        return view('admin.seo.allseos',compact('seos'));
    }
}
